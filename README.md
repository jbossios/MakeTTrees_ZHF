# Setup

## First step: setup correct folder structure and clone this repository

Create your working directory and inside create two folders named *source* and *build*

Inside *source* clone this repository.

Once done, move the setup scripts:

```
cd source/MakeTTrees_ZHF/scripts
mv *.sh ../../../
cd ../../../
```

## Get xAODAnaHelpers (xAH)

```
cd source
git clone https://github.com/UCATLAS/xAODAnaHelpers
```

## Get TruthJetReconstructionAlg (to run over HIGG5D2)

```
cd source
git clone ssh://git@gitlab.cern.ch:7999/atlas-trigger/jet/truthjetreconstructionalg.git
```

# Run

## Run locally (for testing)

### Setup

```
source LocalSetup.sh
```

### Take a look at *TestRun.py* and run it:

```
cd source/MakeTTrees_ZHF/scripts
python TestRun.py
```

## Run on the grid

### Setup

Before sourcing *GridSetup.sh*, source *LocalSetup.sh* to create the *CMakeLists.txt* file

Erase everything from *build/* created by *LocalSetup.sh* and then do:

```
source GridSetup.sh
```

Take a look at runGrid.py and run it:

```
python source/MakeTTrees_ZHF/scripts/runGrid.py
```

# Tips

If encounter with error "no module named python"
Erase everything from build/ and source the needed shell script
