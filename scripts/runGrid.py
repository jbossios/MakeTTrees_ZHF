#!/usr/bin/python
import os, sys
from time import strftime

test = False

timestamp = strftime("_%d%m%y")
if not test:
  if not os.path.exists("gridOutput"):
    os.system("mkdir gridOutput")
  if not os.path.exists("gridOutput/gridJobs"):
    os.system("mkdir gridOutput/gridJobs")

version = 'v11'

systematics = False

# Replicate to a given DDM endpoint
destSE = ""
#destSE = "CERN-PROD_LOCALGROUPDISK"
#destSE = "CA-VICTORIA-WESTGRID-T2_LOCALGROUPDISK"
#destSE = "CA-SFU-T2_LOCALGROUPDISK"
#destSE = "TRIUMF-LCG2_LOCALGROUPDISK" # use this one
#destSE = "CA-WATERLOO-T2_LOCALGROUPDISK" # use this one too

# Exclude site
excludeSite = ""

# PLEASE NOTE: all the samples will be sent using the config defined above
# Each data-taking period and MC16 campaign have dedicated configs
samples = { # should match to the keys of dataSamples.py or mcSamples.py
#DATA
#  "Data15_STDM3_p4433",
#  "Data16_STDM3_p4433",
#  "Data17_STDM3_p4433",
#  "Data18_STDM3_p4433",
#Z(->ee) + jets Sherpa 2.2.11
#  'MC16a_Zee_Sherpa_2211_STDM3_p4434',
#  'MC16d_Zee_Sherpa_2211_STDM3_p4434',
#  'MC16e_Zee_Sherpa_2211_STDM3_p4434',
#Z(->ee) + jets MadGraph
#  "MC16a_Zee_MG_STDM3_p4434",
#  "MC16d_Zee_MG_STDM3_p4434",
#  "MC16e_Zee_MG_STDM3_p4434",
#Z(->ee) + jets MG FxFx
#  "MC16a_Zee_MG_FxFx_STDM3_p4434",
#  "MC16d_Zee_MG_FxFx_STDM3_p4434",
#  "MC16e_Zee_MG_FxFx_STDM3_p4434",
#Z(->mumu) + jets Sherpa 2.2.11
#  'MC16a_Zmumu_Sherpa_2211_STDM3_p4434',
#  'MC16d_Zmumu_Sherpa_2211_STDM3_p4434',
#  'MC16e_Zmumu_Sherpa_2211_STDM3_p4434',
#Z(->mumu) + jets MadGraph
#  "MC16a_Zmumu_MG_STDM3_p4434",
#  "MC16d_Zmumu_MG_STDM3_p4434",
#  "MC16e_Zmumu_MG_STDM3_p4434",
#Z(->mumu) + jets MG FxFx
#  "MC16a_Zmumu_MG_FxFx_STDM3_p4434",
#  "MC16d_Zmumu_MG_FxFx_STDM3_p4434",
#  "MC16e_Zmumu_MG_FxFx_STDM3_p4434",
#Z(->tautau) + jets
#  "MC16a_Ztautau_Sherpa_STDM3_p4432",
#  "MC16d_Ztautau_Sherpa_STDM3_p4432",
#  "MC16e_Ztautau_Sherpa_STDM3_p4432",
#Z(->tautau) + jets Sherpa 2.2.11
#  "MC16a_Ztautau_Sherpa_2211_STDM3_p4434",
#  "MC16d_Ztautau_Sherpa_2211_STDM3_p4434",
#  "MC16e_Ztautau_Sherpa_2211_STDM3_p4434",
#ttbar nonallhad
#  "MC16a_ttbar_nonallhad_PPy8_STDM3_p4432",
#  "MC16d_ttbar_nonallhad_PPy8_STDM3_p4432",
#  "MC16e_ttbar_nonallhad_PPy8_STDM3_p4432",
#ttbar dilepton
#  "MC16a_ttbar_dilepton_PPy8_STDM3_p4434",
#  "MC16d_ttbar_dilepton_PPy8_STDM3_p4434",
#  "MC16e_ttbar_dilepton_PPy8_STDM3_p4434",
#SingleTop
#  "MC16a_SingleTop_PPy8_STDM3_p4432",
#  "MC16d_SingleTop_PPy8_STDM3_p4432",
#  "MC16e_SingleTop_PPy8_STDM3_p4432",
#W(->enu) + jets
#  "MC16a_Wenu_Sherpa_STDM3_p4432",
#  "MC16d_Wenu_Sherpa_STDM3_p4432",
#  "MC16e_Wenu_Sherpa_STDM3_p4432",
#W(->munu) + jets
#  "MC16a_Wmunu_Sherpa_STDM3_p4432",
#  "MC16d_Wmunu_Sherpa_STDM3_p4432",
#  "MC16e_Wmunu_Sherpa_STDM3_p4432",
#W(->taunu) + jets
#  "MC16a_Wtaunu_Sherpa_STDM3_p4432",
#  "MC16d_Wtaunu_Sherpa_STDM3_p4432",
#  "MC16e_Wtaunu_Sherpa_STDM3_p4432",
#Diboson
#  "MC16a_Diboson_Sherpa_STDM3_p4432",
#  "MC16d_Diboson_Sherpa_STDM3_p4432",
#  "MC16e_Diboson_Sherpa_STDM3_p4432",
#VHbb
#  "MC16a_VHbb_PPy8_STDM3_p4432",
#  "MC16d_VHbb_PPy8_STDM3_p4432",
#  "MC16e_VHbb_PPy8_STDM3_p4432",
#VHcc
#  "MC16a_VHcc_PPy8_STDM3_p4432",
#  "MC16d_VHcc_PPy8_STDM3_p4432",
#  "MC16e_VHcc_PPy8_STDM3_p4432",
# Zmumu DAOD_PHYS (only for DAOD_PHYS studies)
#  "MC16a_Zmumu_364109_DAOD_PHYS_p4087",
#PHYS
#  "MC16a_Zmumu_DAOD_PHYS_Ref1_p4166",
#  "MC16a_Zmumu_DAOD_PHYS_Ref2_p4166",
}

excludeAllExcept_DSIDs = [ # example 364104 will run over DSID=364104 only
]

excludeAllExcept_periods = [ # example: periodD will run over periodD only
]

##################################################################################
# DO NOT MODIFY
##################################################################################

#### Driver option ####
runType = 'grid' # CERN grid

## Set this only for group production submissions ##
production_name = ""

from mcSamples import *
from dataSamples import *

def getDataPeriod(sampleName):
  return sampleName[13:20]

def getMCDSID(sampleName):
  if 'valid' not in sampleName:
    return sampleName[11:17]
  else:
    return sampleName[7:13]

# Create a dictionary with all the requested samples
SamplesDict = dict()
for key in samples: # Loop over provided keys
  if "Data" in key:
    if key in dataSamples:
      for sample in dataSamples[key]: # add each dataset name to the dict
        dataPeriod = getDataPeriod(sample)
        if len(excludeAllExcept_periods) != 0:
          if dataPeriod not in excludeAllExcept_periods: continue # skip data period
        SamplesDict[key+"_"+dataPeriod] = sample
    else: print "ERROR: key "+key+" not found in dataSamples.py"
  else: # MC
    if key in mcSamples:
      for sample in mcSamples[key]: # add each dataset name to the dict
        dsid = getMCDSID(sample)
        if len(excludeAllExcept_DSIDs) != 0:
          if dsid not in excludeAllExcept_DSIDs: continue # skip DSID
	SamplesDict[key+"_"+dsid] = sample
    else: print "ERROR: key "+key+" not found in mcSamples.py"

for sampleName, sample in SamplesDict.iteritems():

  # Set config name and extraTag
  if 'Data' in sampleName:
    year = sampleName.split('_')[0].replace('Data', '')
    config_name = 'source/MakeTTrees_ZHF/data/config_Tree_20{}Data_ZHFJETS_AntiKt4EMPFlow.py'.format(year)
    extraTag = "_{}".format(version)  # extra output tag for all files
  else:  # MC
    mc16_campaign = sampleName.split('_')[0]
    if systematics:
      if 'HIGG5D2' in sampleName:
        config_name = 'source/MakeTTrees_ZHF/data/config_Tree_{}_ZHFJETS_AntiKt4EMPFlow_Systematics_HIGG5D2.py'.format(mc16_campaign)
        extraTag = "_{}_sys".format(version)  # extra output tag for all files
      else:  # STDM3
        config_name = 'source/MakeTTrees_ZHF/data/config_Tree_{}_ZHFJETS_AntiKt4EMPFlow_Systematics.py'.format(mc16_campaign)
        extraTag = "_{}_sys".format(version)  # extra output tag for all files
    else:  # nominal
      if 'Zee' in sampleName or 'Zmumu' in sampleName:
        config_name = 'source/MakeTTrees_ZHF/data/config_Tree_{}_ZHFJETS_AntiKt4EMPFlow_NominalOnlySignal.py'.format(mc16_campaign)
        extraTag = "_{}_nom".format(version)  # extra output tag for all files
      else:
        if 'HIGG5D2' in sampleName:
          config_name = 'source/MakeTTrees_ZHF/data/config_Tree_{}_ZHFJETS_AntiKt4EMPFlow_NominalOnlyBkgs_HIGG5D2.py'.format(mc16_campaign)
          extraTag = "_{}_nom".format(version)  # extra output tag for all files
	else:  # STDM3
          config_name = 'source/MakeTTrees_ZHF/data/config_Tree_{}_ZHFJETS_AntiKt4EMPFlow_NominalOnlyBkgs.py'.format(mc16_campaign)
          extraTag = "_{}_nom".format(version)  # extra output tag for all files

  output_tag = sampleName + extraTag + timestamp
  submit_dir = "gridOutput/gridJobs/submitDir_"+output_tag

  ## Configure submission driver ##
  driverCommand = ''
  if runType == 'grid':
    #driverCommand  = 'prun --optSubmitFlags="--forceStaged"'
    driverCommand  = 'prun --optSubmitFlags="--noEmail" '
    if destSE != "": driverCommand += ' --optGridDestSE='+destSE
    if excludeSite != "": driverCommand += ' --optSubmitFlags="--excludedSite='+excludeSite+'"'
    #driverCommand += ' --optGridMemory=4096' # Temporary
    if 'Systematics' in config_name: driverCommand += ' --optGridNFilesPerJob=1' # run one file per job when running with systematics
    driverCommand += ' --optGridOutputSampleName='
    #driverCommand = 'prun --optGridMaxNFilesPerJob=2 --optSubmitFlags="--forceStaged" --optGridOutputSampleName='
    #driverCommand = 'prun --optSubmitFlags="--skipScout --excludedSite=ANALY_CERN_SHORT,ANALY_BNL_SHORT" --optGridOutputSampleName='
    if len(production_name) > 0:
      #driverCommand = ' prun --optSubmitFlags="--memory=5120 --official --skipScout" --optGridOutputSampleName='
      driverCommand = ' prun --optSubmitFlags="--official --forceStaged" --optGridOutputSampleName='
      #driverCommand = ' prun --optGridMaxNFilesPerJob=2 --optSubmitFlags="--official --forceStaged" --optGridOutputSampleName='
      #driverCommand = ' prun --optSubmitFlags="--official" --optGridOutputSampleName='
      driverCommand += 'group.'+production_name
    else:
      driverCommand += 'user.%nickname%'
    driverCommand += '.%in:name[2]%.'+output_tag
  elif runType == 'local':
    driverCommand = ' direct'

  command = './source/xAODAnaHelpers/scripts/xAH_run.py'
  if runType == 'grid':
    command += ' --inputRucio '

  if 'sampleLists' in sample:
    command += ' --inputList'
  command += ' --files '+sample
  command += ' --config '+config_name
  command += ' --force --submitDir '+submit_dir
  command += ' '+driverCommand

  print command
  if not test: os.system(command)
