dataSamples = dict()
dataSamples["Data15_STDM3_p4433"] = [
  "data15_13TeV.periodD.physics_Main.PhysCont.DAOD_STDM3.grp23_v01_p4433",
  "data15_13TeV.periodE.physics_Main.PhysCont.DAOD_STDM3.grp23_v01_p4433",
  "data15_13TeV.periodF.physics_Main.PhysCont.DAOD_STDM3.grp23_v01_p4433",
  "data15_13TeV.periodG.physics_Main.PhysCont.DAOD_STDM3.grp23_v01_p4433",
  "data15_13TeV.periodH.physics_Main.PhysCont.DAOD_STDM3.grp23_v01_p4433",
  "data15_13TeV.periodJ.physics_Main.PhysCont.DAOD_STDM3.grp23_v01_p4433",
]
dataSamples["Data16_STDM3_p4433"] = [
  "data16_13TeV.periodA.physics_Main.PhysCont.DAOD_STDM3.grp23_v01_p4433",
  "data16_13TeV.periodB.physics_Main.PhysCont.DAOD_STDM3.grp23_v01_p4433",
  "data16_13TeV.periodC.physics_Main.PhysCont.DAOD_STDM3.grp23_v01_p4433",
  "data16_13TeV.periodD.physics_Main.PhysCont.DAOD_STDM3.grp23_v01_p4433",
  "data16_13TeV.periodE.physics_Main.PhysCont.DAOD_STDM3.grp23_v01_p4433",
  "data16_13TeV.periodF.physics_Main.PhysCont.DAOD_STDM3.grp23_v01_p4433",
  "data16_13TeV.periodG.physics_Main.PhysCont.DAOD_STDM3.grp23_v01_p4433", 
  "data16_13TeV.periodI.physics_Main.PhysCont.DAOD_STDM3.grp23_v01_p4433",
  "data16_13TeV.periodK.physics_Main.PhysCont.DAOD_STDM3.grp23_v01_p4433",
  "data16_13TeV.periodL.physics_Main.PhysCont.DAOD_STDM3.grp23_v01_p4433",
]
dataSamples["Data17_STDM3_p4433"] = [
  "data17_13TeV.periodB.physics_Main.PhysCont.DAOD_STDM3.grp23_v01_p4433",
  "data17_13TeV.periodC.physics_Main.PhysCont.DAOD_STDM3.grp23_v01_p4433",
  "data17_13TeV.periodD.physics_Main.PhysCont.DAOD_STDM3.grp23_v01_p4433",
  "data17_13TeV.periodE.physics_Main.PhysCont.DAOD_STDM3.grp23_v01_p4433",
  "data17_13TeV.periodF.physics_Main.PhysCont.DAOD_STDM3.grp23_v01_p4433",
  "data17_13TeV.periodH.physics_Main.PhysCont.DAOD_STDM3.grp23_v01_p4433",
  "data17_13TeV.periodI.physics_Main.PhysCont.DAOD_STDM3.grp23_v01_p4433",
  "data17_13TeV.periodK.physics_Main.PhysCont.DAOD_STDM3.grp23_v01_p4433",
]
dataSamples["Data18_STDM3_p4433"] = [
  "data18_13TeV.periodB.physics_Main.PhysCont.DAOD_STDM3.grp23_v01_p4433",
  "data18_13TeV.periodC.physics_Main.PhysCont.DAOD_STDM3.grp23_v01_p4433",
  "data18_13TeV.periodD.physics_Main.PhysCont.DAOD_STDM3.grp23_v01_p4433",
  "data18_13TeV.periodF.physics_Main.PhysCont.DAOD_STDM3.grp23_v01_p4433",
  "data18_13TeV.periodI.physics_Main.PhysCont.DAOD_STDM3.grp23_v01_p4433",
  "data18_13TeV.periodK.physics_Main.PhysCont.DAOD_STDM3.grp23_v01_p4433",
  "data18_13TeV.periodL.physics_Main.PhysCont.DAOD_STDM3.grp23_v01_p4433",
  "data18_13TeV.periodM.physics_Main.PhysCont.DAOD_STDM3.grp23_v01_p4433",
  "data18_13TeV.periodO.physics_Main.PhysCont.DAOD_STDM3.grp23_v01_p4433",
  "data18_13TeV.periodQ.physics_Main.PhysCont.DAOD_STDM3.grp23_v01_p4433",
]
# old
dataSamples["Data15_STDM3_p4251"] = [
  "data15_13TeV.periodD.physics_Main.PhysCont.DAOD_STDM3.grp23_v01_p4251",
  "data15_13TeV.periodE.physics_Main.PhysCont.DAOD_STDM3.grp23_v01_p4251",
  "data15_13TeV.periodF.physics_Main.PhysCont.DAOD_STDM3.grp23_v01_p4251",
  "data15_13TeV.periodG.physics_Main.PhysCont.DAOD_STDM3.grp23_v01_p4251",
  "data15_13TeV.periodH.physics_Main.PhysCont.DAOD_STDM3.grp23_v01_p4251",
  "data15_13TeV.periodJ.physics_Main.PhysCont.DAOD_STDM3.grp23_v01_p4251",
]
dataSamples["Data16_STDM3_p4251"] = [
  "data16_13TeV.periodA.physics_Main.PhysCont.DAOD_STDM3.grp23_v01_p4251",
  "data16_13TeV.periodB.physics_Main.PhysCont.DAOD_STDM3.grp23_v01_p4251",
  "data16_13TeV.periodC.physics_Main.PhysCont.DAOD_STDM3.grp23_v01_p4251",
  "data16_13TeV.periodD.physics_Main.PhysCont.DAOD_STDM3.grp23_v01_p4251",
  "data16_13TeV.periodE.physics_Main.PhysCont.DAOD_STDM3.grp23_v01_p4251",
  "data16_13TeV.periodF.physics_Main.PhysCont.DAOD_STDM3.grp23_v01_p4251",
  "data16_13TeV.periodG.physics_Main.PhysCont.DAOD_STDM3.grp23_v01_p4251", 
  "data16_13TeV.periodI.physics_Main.PhysCont.DAOD_STDM3.grp23_v01_p4251",
  "data16_13TeV.periodK.physics_Main.PhysCont.DAOD_STDM3.grp23_v01_p4251",
  "data16_13TeV.periodL.physics_Main.PhysCont.DAOD_STDM3.grp23_v01_p4251",
]
dataSamples["Data17_STDM3_p4251"] = [
  "data17_13TeV.periodB.physics_Main.PhysCont.DAOD_STDM3.grp23_v01_p4251",
  "data17_13TeV.periodC.physics_Main.PhysCont.DAOD_STDM3.grp23_v01_p4251",
  "data17_13TeV.periodD.physics_Main.PhysCont.DAOD_STDM3.grp23_v01_p4251",
  "data17_13TeV.periodE.physics_Main.PhysCont.DAOD_STDM3.grp23_v01_p4251",
  "data17_13TeV.periodF.physics_Main.PhysCont.DAOD_STDM3.grp23_v01_p4251",
  "data17_13TeV.periodH.physics_Main.PhysCont.DAOD_STDM3.grp23_v01_p4251",
  "data17_13TeV.periodI.physics_Main.PhysCont.DAOD_STDM3.grp23_v01_p4251",
  "data17_13TeV.periodK.physics_Main.PhysCont.DAOD_STDM3.grp23_v01_p4251",
]
dataSamples["Data18_STDM3_p4251"] = [
  "data18_13TeV.periodB.physics_Main.PhysCont.DAOD_STDM3.grp23_v01_p4251",
  "data18_13TeV.periodC.physics_Main.PhysCont.DAOD_STDM3.grp23_v01_p4251",
  "data18_13TeV.periodD.physics_Main.PhysCont.DAOD_STDM3.grp23_v01_p4251",
  "data18_13TeV.periodF.physics_Main.PhysCont.DAOD_STDM3.grp23_v01_p4251",
  "data18_13TeV.periodI.physics_Main.PhysCont.DAOD_STDM3.grp23_v01_p4251",
  "data18_13TeV.periodK.physics_Main.PhysCont.DAOD_STDM3.grp23_v01_p4251",
  "data18_13TeV.periodL.physics_Main.PhysCont.DAOD_STDM3.grp23_v01_p4251",
  "data18_13TeV.periodM.physics_Main.PhysCont.DAOD_STDM3.grp23_v01_p4251",
  "data18_13TeV.periodO.physics_Main.PhysCont.DAOD_STDM3.grp23_v01_p4251",
  "data18_13TeV.periodQ.physics_Main.PhysCont.DAOD_STDM3.grp23_v01_p4251",
]
# old derivations w/o new b-tagging shallow copy containers
dataSamples["Data15_STDM3_p3760"] = [
  "data15_13TeV.periodH.physics_Main.PhysCont.DAOD_STDM3.grp23_v01_p3760",
  "data15_13TeV.periodE.physics_Main.PhysCont.DAOD_STDM3.grp23_v01_p3760",
  "data15_13TeV.periodF.physics_Main.PhysCont.DAOD_STDM3.grp23_v01_p3760",
  "data15_13TeV.periodG.physics_Main.PhysCont.DAOD_STDM3.grp23_v01_p3760",
  "data15_13TeV.periodJ.physics_Main.PhysCont.DAOD_STDM3.grp23_v01_p3760",
  "data15_13TeV.periodD.physics_Main.PhysCont.DAOD_STDM3.grp23_v01_p3760",
]
dataSamples["Data16_STDM3_p3760"] = [
  "data16_13TeV.periodE.physics_Main.PhysCont.DAOD_STDM3.grp23_v01_p3760",
  "data16_13TeV.periodK.physics_Main.PhysCont.DAOD_STDM3.grp23_v01_p3760",
  "data16_13TeV.periodC.physics_Main.PhysCont.DAOD_STDM3.grp23_v01_p3760",
  "data16_13TeV.periodI.physics_Main.PhysCont.DAOD_STDM3.grp23_v01_p3760",
  "data16_13TeV.periodF.physics_Main.PhysCont.DAOD_STDM3.grp23_v01_p3760",
  "data16_13TeV.periodL.physics_Main.PhysCont.DAOD_STDM3.grp23_v01_p3760",
  "data16_13TeV.periodA.physics_Main.PhysCont.DAOD_STDM3.grp23_v01_p3760",
  "data16_13TeV.periodD.physics_Main.PhysCont.DAOD_STDM3.grp23_v01_p3760",
  "data16_13TeV.periodB.physics_Main.PhysCont.DAOD_STDM3.grp23_v01_p3760",
  "data16_13TeV.periodG.physics_Main.PhysCont.DAOD_STDM3.grp23_v01_p3760", 
]
dataSamples["Data17_STDM3_p3760"] = [
  "data17_13TeV.periodB.physics_Main.PhysCont.DAOD_STDM3.grp23_v01_p3760",
  "data17_13TeV.periodF.physics_Main.PhysCont.DAOD_STDM3.grp23_v01_p3760",
  "data17_13TeV.periodC.physics_Main.PhysCont.DAOD_STDM3.grp23_v01_p3760",
  "data17_13TeV.periodE.physics_Main.PhysCont.DAOD_STDM3.grp23_v01_p3760",
  "data17_13TeV.periodD.physics_Main.PhysCont.DAOD_STDM3.grp23_v01_p3760",
  "data17_13TeV.periodI.physics_Main.PhysCont.DAOD_STDM3.grp23_v01_p3760",
  "data17_13TeV.periodH.physics_Main.PhysCont.DAOD_STDM3.grp23_v01_p3760",
  "data17_13TeV.periodK.physics_Main.PhysCont.DAOD_STDM3.grp23_v01_p3760",
]
dataSamples["Data18_STDM3_p3760"] = [
  "data18_13TeV.periodD.physics_Main.PhysCont.DAOD_STDM3.grp23_v01_p3760",
  "data18_13TeV.periodL.physics_Main.PhysCont.DAOD_STDM3.grp23_v01_p3760",
  "data18_13TeV.periodB.physics_Main.PhysCont.DAOD_STDM3.grp23_v01_p3760",
  "data18_13TeV.periodM.physics_Main.PhysCont.DAOD_STDM3.grp23_v01_p3760",
  "data18_13TeV.periodK.physics_Main.PhysCont.DAOD_STDM3.grp23_v01_p3760",
  "data18_13TeV.periodI.physics_Main.PhysCont.DAOD_STDM3.grp23_v01_p3760",
  "data18_13TeV.periodC.physics_Main.PhysCont.DAOD_STDM3.grp23_v01_p3760",
  "data18_13TeV.periodF.physics_Main.PhysCont.DAOD_STDM3.grp23_v01_p3760",
  "data18_13TeV.periodO.physics_Main.PhysCont.DAOD_STDM3.grp23_v01_p3760",
  "data18_13TeV.periodQ.physics_Main.PhysCont.DAOD_STDM3.grp23_v01_p3760",
]
