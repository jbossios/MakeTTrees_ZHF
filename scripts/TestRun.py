import os,sys

#sample = "/eos/atlas/atlascerngroupdisk/phys-sm/ZHF_FullRun2/TestDxAODs/MC16a_Zmumu/STDM3/p3918/mc16_13TeV/DAOD_STDM3.18619926._000022.pool.root.1"     # MC16a Zmumu
#sample = "/eos/atlas/atlascerngroupdisk/phys-sm/ZHF_FullRun2/TestDxAODs/MC16a_Zee/STDM3_MC16a_p3918/mc16_13TeV/DAOD_STDM3.18622475._000021.pool.root.1" # MC16a Zee
#sample = "/eos/user/j/jbossios/SM/DxAODs/Top/mc16_13TeV/DAOD_STDM3.13992845._000211.pool.root.1"                                                 # Top
#sample = "/eos/atlas/atlascerngroupdisk/phys-sm/ZHF_FullRun2/TestDxAODs/data15/STDM3/p3760/data15_13TeV/DAOD_STDM3.17004399._000032.pool.root.1" # data15 STDM3 p3760
#sample = "/eos/atlas/atlascerngroupdisk/phys-sm/ZHF_FullRun2/TestDxAODs/data15/STDM3/p3984/data15_13TeV/DAOD_STDM3.19402976._000051.pool.root.1" # data15 STDM3 p3984
#sample = "/eos/atlas/atlascerngroupdisk/phys-sm/ZHF_FullRun2/TestDxAODs/STDM3/data18/data18_13TeV/DAOD_STDM3.17003866._000095.pool.root.1"       # data18
#sample = "/eos/atlas/atlascerngroupdisk/phys-sm/ZHF_FullRun2/TestDxAODs/MC16a_Diboson/STDM3/p3916/WlvZqq/mc16_13TeV/DAOD_STDM3.18620129._000011.pool.root.1" # MC16a diboson p3916
#sample = "/eos/atlas/atlascerngroupdisk/phys-sm/ZHF_FullRun2/TestDxAODs/MC16a_Zmumu/STDM3/p3970/mc16_13TeV/DAOD_STDM3.19273088._000032.pool.root.1" # MC16a Zmumu p3970
#sample = "/eos/atlas/atlascerngroupdisk/phys-sm/ZHF_FullRun2/TestDxAODs/MC16e_Ztautau/DAOD_STDM3.19273073._000066.pool.root.1" # MC16e Ztautau
#sample = "/eos/atlas/atlascerngroupdisk/phys-sm/ZHF_FullRun2/TestDxAODs/MC16e_Zmumu/DAOD_STDM3.19274002._000045.pool.root.1"   # MC16e Zmumu
#sample = "/eos/atlas/atlascerngroupdisk/phys-sm/ZHF_FullRun2/TestDxAODs/MC16e_Zee/DAOD_STDM3.19274350._000047.pool.root.1"     # MC16e Zee
#sample = "/eos/atlas/atlascerngroupdisk/phys-sm/ZHF_FullRun2/TestDxAODs/MC16d_Ztautau/mc16_13TeV/DAOD_STDM3.21193851._000013.pool.root.1" # MC16d Ztautau

# Test Zmumu DAOD_PHYS samples
#sample = '/afs/cern.ch/user/j/jbossios/work/public/SM/WZGroup/Z+c_13TeV/GIT_xAH/git_xAH/Inputs/valid1.361107.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zmumu.deriv.DAOD_PHYS.e5112_s3227_r11825_p4166/DAOD_PHYS.21615217._000001.pool.root.1'
#sample = "/afs/cern.ch/user/j/jbossios/work/public/SM/WZGroup/Z+c_13TeV/GIT_xAH/git_xAH/Inputs/MC16e_Zmumu_Sherpa_361405_PHYS/valid1/DAOD_PHYS.21615236._000001.pool.root.1"
#sample = '/afs/cern.ch/user/j/jbossios/work/public/SM/WZGroup/Z+c_13TeV/GIT_xAH/git_xAH/Inputs/MC16a_Ztautau_Powheg_361108_PHYS/valid1.361108.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Ztautau.deriv.DAOD_PHYS.e5112_s3227_r11825_p4166/DAOD_PHYS.21615221._000001.pool.root.1'
#sample = '/afs/cern.ch/user/j/jbossios/work/public/SM/WZGroup/Z+c_13TeV/GIT_xAH/git_xAH/Inputs/MC16e_Zee_Powheg_361106_PHYS/valid1.361106.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zee.deriv.DAOD_PHYS.e5112_s3227_r11825_p4166/DAOD_PHYS.21568547._000007.pool.root.1'

#sample = "/eos/atlas/atlascerngroupdisk/phys-sm/ZHF_FullRun2/TestDxAODs/MC16d_Zmumu/STDM3/mc16_13TeV/DAOD_STDM3.19275045._000001.pool.root.1" # MC16d Zmumu

#sample = "/eos/atlas/user/j/jbossios/SM/ZHFRun2/DAOD_TestFiles/MC16a_Zmumu/mc16_13TeV/DAOD_STDM3.19274245._000127.pool.root.1" # Test file for coding days
#sample = "/eos/atlas/atlascerngroupdisk/phys-sm/Wjets_FullRun2/McGill/DAODtestFile/Wenu_Sherpa_MC16a_p3972/mc16_13TeV/DAOD_STDM4.19241749._000069.pool.root.1" # Test file for coding days
#sample = "/eos/atlas/atlascerngroupdisk/phys-sm/Wjets_FullRun2/McGill/DAODtestFile/Wenu_Sherpa_MC16a/mc16_13TeV/DAOD_STDM4.19242232._000115.pool.root.1"
#sample = "/eos/user/g/gcallea/zbb/mc16_13TeV.364100.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV0_70_CVetoBVeto.deriv.DAOD_STDM3.e5271_s3126_r9364_p4097/DAOD_STDM3.STDM3_MC16.pool.root.1"
# Event by event comparisons
#sample = '/eos/atlas/atlascerngroupdisk/phys-sm/ZHF_FullRun2/TestDxAODs/MC16a_Zmumu/STDM3/p4252/mc16_13TeV/DAOD_STDM3.22513068._000040.pool.root.1' # just testing dRjet
#sample = '/eos/atlas/atlascerngroupdisk/phys-sm/ZHF_FullRun2/TestDxAODs/data15/STDM3/p3984/data15_13TeV/DAOD_STDM3.19402976._000051.pool.root.1' # data15
#sample = '/eos/atlas/atlascerngroupdisk/phys-sm/ZHF_FullRun2/TestDxAODs/MC16a_Zmumu/STDM3/p3970/mc16_13TeV/DAOD_STDM3.19273088._000032.pool.root.1' # MC16a Zmumu
#sample = '/eos/user/j/jbossios/SM/WZgroup/ZHF/DAOD_TestFiles/MC16a_Zee/mc16_13TeV/DAOD_STDM3.22512126._000035.pool.root.1' # MC16a Zee 364118
#sample = '/eos/user/j/jbossios/SM/WZgroup/ZHF/DAOD_TestFiles/MC16a_ttbar/mc16_13TeV/DAOD_STDM3.19685332._000598.pool.root.1' # MC16a ttbar
###sample = '/eos/atlas/user/j/jbossios/SM/ZHFRun2/DAOD_TestFiles/MC16a_ttbar/mc16_13TeV/DAOD_STDM3.19685320._000894.pool.root.1' # MC16a ttbar [deleted]
#sample = '/eos/atlas/user/j/jbossios/SM/ZHFRun2/DAOD_TestFiles/MC16a_Diboson/mc16_13TeV/DAOD_STDM3.21193971._000087.pool.root.1' # MC16a Diboson
#sample = '/eos/atlas/user/j/jbossios/SM/ZHFRun2/DAOD_TestFiles/MC16a_SingleTop/mc16_13TeV/DAOD_STDM3.21194121._000029.pool.root.1' # MC16a SingleTop
# DAOD_PHYS
#sample = "/eos/atlas/user/j/jbossios/SM/ZHFRun2/DAOD_PHYS_testFiles/mc16_13TeV/DAOD_PHYS.20609937._000047.pool.root.1"
#sample = "/eos/user/j/jbossios/SM/WZgroup/ZHF/DAOD_PHYS_testFiles_R21/Sherpa_Zmumu_364111_p4164/mc16_13TeV/DAOD_PHYS.22858361._000077.pool.root.1"

# Test samples with latest ptags
#sample = '/eos/atlas/atlascerngroupdisk/phys-sm/ZHF_FullRun2/TestDxAODs/data15/STDM3/p4251/data15_13TeV/DAOD_STDM3.22506485._000035.pool.root.1'    # data15
#sample = '/eos/atlas/atlascerngroupdisk/phys-sm/ZHF_FullRun2/TestDxAODs/MC16a_Zmumu/STDM3/p4252/mc16_13TeV/DAOD_STDM3.22513068._000040.pool.root.1' # MC16a Zmumu 364104
#sample = '/eos/atlas/atlascerngroupdisk/phys-sm/ZHF_FullRun2/TestDxAODs/MC16a_Zmumu/STDM3/p4252_364105/mc16_13TeV/DAOD_STDM3.22513691._000040.pool.root.1' # MC16a Zmumu 364105 (rich on truth b-jets)
#sample = '/eos/atlas/atlascerngroupdisk/phys-sm/ZHF_FullRun2/TestDxAODs/MC16a_Zee/STDM3_p4252/mc16_13TeV/DAOD_STDM3.22513272._000040.pool.root.1'   # MC16a Zee
#sample = '/eos/atlas/atlascerngroupdisk/phys-sm/ZHF_FullRun2/TestDxAODs/MC16a_Zee_MG/mc16_13TeV/DAOD_STDM3.24617478._000001.pool.root.1' # MC16a Zee MG
#sample = '/eos/user/j/jbossios/SM/WZgroup/ZHF/DAOD_TestFiles/MC16a_VH/HIGG5D2/mc16_13TeV/DAOD_HIGG5D2.22958819._000004.pool.root.1' # VHcc HIGG5D2
sample = '/eos/atlas/atlascerngroupdisk/phys-sm/ZHF_FullRun2/TestDxAODs/MC16a_Zmumu_Sh_2211/mc16_13TeV/DAOD_STDM4.25793393._000003.pool.root.1'

# Configs for Resolved analysis
#config = "config_Tree_2015Data_ZHFJETS_AntiKt4EMPFlow"
#config = "config_Tree_2016Data_ZHFJETS_AntiKt4EMPFlow"
#config = "config_Tree_2017Data_ZHFJETS_AntiKt4EMPFlow"
#config = "config_Tree_2018Data_ZHFJETS_AntiKt4EMPFlow"
config = "config_Tree_MC16a_ZHFJETS_AntiKt4EMPFlow_NominalOnlySignal_STDM4"
#config = "config_Tree_MC16a_ZHFJETS_AntiKt4EMPFlow_NominalOnlySignal"
#config = "config_Tree_MC16a_ZHFJETS_AntiKt4EMPFlow_NominalOnlyBkgs"
#config = "config_Tree_MC16a_ZHFJETS_AntiKt4EMPFlow_Systematics"
#config = "config_Tree_MC16a_ZHFJETS_AntiKt4EMPFlow_NominalOnlySignal_PHYS"
#config = "config_Tree_MC16a_ZHFJETS_AntiKt4EMPFlow_NominalOnlyBkgs_HIGG5D2"

# Configs for Boosted analysis
#config = "config_Tree_MC16a_ZHFJETS_Boosted_AntiKt10LCTopoTrimmedPtFrac5SmallR20"
#config = "config_Tree_MC16a_ZHFJETS_AntiKt4EMPFlow"

################################################
## DO NOT MODIFY
################################################

# choose appropiate config
CONFIG  = config
CONFIG += ".py"

# run xAH_run.py
command  = "python ../../../source/xAODAnaHelpers/scripts/xAH_run.py --config ../data/"
command += CONFIG
command += " --files "
command += sample
command += " --force direct"
print(command)
os.system(command)
