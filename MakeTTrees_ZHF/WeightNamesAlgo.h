/** @file WeightNamesAlgo.h
 *  @brief Get event weight names
 *  @author Jona Bossio (jbossios@cern.ch)
 */

#ifndef MakeTTrees_Wjets_WeightNamesAlgo_H
#define MakeTTrees_Wjets_WeightNamesAlgo_H

// EDM include(s):
#include "PMGTools/PMGTruthWeightTool.h"

// algorithm wrapper
#include "xAODAnaHelpers/Algorithm.h"

// Infrastructure include(s):
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODRootAccess/TStore.h"

// ROOT
#include "TNamed.h"

#include <sstream>

#include "AsgTools/AnaToolHandle.h"

// NOTE:
// Variables that don't get filled at submission time should be
// protected from being send from the submission node to the worker
// node (done by the //!)

class WeightNamesAlgo : public xAH::Algorithm
{

  public:

    // Use tool only once
    bool m_toolUsed = false; //!
      
  private:

    // tools
    asg::AnaToolHandle<PMGTools::IPMGTruthWeightTool> m_weightTool {"PMGTools::PMGTruthWeightTool/PMGTruthWeightTool"   , this}; //!

    TNamed * m_names; //!

  public:

    // Constructor
    WeightNamesAlgo ();

    /** @brief Setup the job (inherits from Algorithm)*/
    virtual EL::StatusCode setupJob (EL::Job& job);
    /** @brief Execute the file (inherits from Algorithm)*/
    virtual EL::StatusCode fileExecute ();
    /** @brief Initialize the output histograms before any input file is attached (inherits from Algorithm)*/
    virtual EL::StatusCode histInitialize ();
    /** @brief Change to the next input file (inherits from Algorithm)*/
    virtual EL::StatusCode changeInput (bool firstFile);
    /** @brief Initialize the input file (inherits from Algorithm)*/
    virtual EL::StatusCode initialize ();
    /** @brief Execute each event of the input file (inherits from Algorithm)*/
    virtual EL::StatusCode execute ();
    /** @brief End the event execution (inherits from Algorithm)*/
    virtual EL::StatusCode postExecute ();
    /** @brief Finalize the input file (inherits from Algorithm)*/
    virtual EL::StatusCode finalize ();
    /** @brief Finalize the histograms after all files (inherits from Algorithm)*/
    virtual EL::StatusCode histFinalize ();

    // these are the functions not inherited from Algorithm
    /** @brief Configure the variables once before running*/
    virtual EL::StatusCode configure ();

    /** @brief Used to distribute the algorithm to the workers*/
    ClassDef(WeightNamesAlgo, 1);
};

#endif
