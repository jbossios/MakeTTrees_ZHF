/** @file ZHFHelpTreeBase.h
 *  @brief Extension of xAH's HelpTreeBase
 *  @author Jona Bossio (jbossios@cern.ch)
 */

#ifndef MakeTTrees_ZHF_ZHFHelpTreeBase_H
#define MakeTTrees_ZHF_ZHFHelpTreeBase_H

// EDM include(s):
#include "xAODTruth/TruthParticleContainer.h"
#include "xAODJet/JetContainer.h"
#include "xAODMuon/MuonContainer.h"
#include "xAODEgamma/ElectronContainer.h"

// algorithm wrapper
#include "xAODAnaHelpers/HelpTreeBase.h"

// Infrastructure include(s):
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODRootAccess/TStore.h"

// ROOT include(s):
#include "TH1D.h"
#include "TFile.h"
#include "TLorentzVector.h"

#include <sstream>

#include "AsgTools/AnaToolHandle.h"

// NOTE:
// Variables that don't get filled at submission time should be
// protected from being send from the submission node to the worker
// node (done by the //!)

class ZHFHelpTreeBase : public HelpTreeBase
{
  // put your configuration variables here as public variables.
  // that way they can be set directly from CINT and python.

  private:

    bool                              m_addJetLooseBad         = false;
    bool                              m_addPassTruthSelections = false;
    bool                              m_addPassRecoSelections  = false;
    bool                              m_addJetTruthInfo        = false;
    std::vector<std::string>          m_ContinuousBTaggers;
    //std::vector<double>               m_jet_MV2c10_BTagWeight;
    std::vector<int>                  m_jet_MV2c10_BTagQuantile;
    std::vector< std::vector<float> > m_jet_MV2c10_BTagSF;
    //std::vector< std::vector<float> > m_jet_MV2c10_BTagIneffSF;
    //std::vector<double>               m_jet_DL1_BTagWeight;
    std::vector<int>                  m_jet_DL1_BTagQuantile;
    std::vector< std::vector<float> > m_jet_DL1_BTagSF;
    //std::vector< std::vector<float> > m_jet_DL1_BTagIneffSF;
    //std::vector<double>               m_jet_DL1r_BTagWeight;
    std::vector<int>                  m_jet_DL1r_BTagQuantile;
    std::vector< std::vector<float> > m_jet_DL1r_BTagSF;
    //std::vector< std::vector<float> > m_jet_DL1r_BTagIneffSF;
    std::vector<int>                  m_DFCommonJets_eventClean_LooseBad;
    std::vector<int>                  m_passTruthSelections;
    std::vector<int>                  m_passRecoSelections;
    std::vector<int>                  m_jet_HadronConeExclTruthLabelID;
    std::vector<int>                  m_truthJet_HadronConeExclTruthLabelID;
    std::vector<int>                  m_jet_PartonTruthLabelID;
    std::vector<int>                  m_truthJet_PartonTruthLabelID;
    double                            m_truthMET_NonInt_sumet;
    double                            m_truthMET_NonInt_phi;
    double                            m_truthMET_Int_sumet;
    double                            m_truthMET_Int_phi;
    double                            m_truthMET_IntOut_sumet;
    double                            m_truthMET_IntOut_phi;
    double                            m_truthMET_IntMuons_sumet;
    double                            m_truthMET_IntMuons_phi;
    float                             m_metFinalTrk;

  public:

    // Constructor
    ZHFHelpTreeBase(xAOD::TEvent *event, TTree* tree, TFile* file, const float units = 1e3, bool debug = false, xAOD::TStore* store = nullptr );
    // Destructor
    ~ZHFHelpTreeBase();

    void AddJetsUser(const std::string& detailStr = "", const std::string& jetName = "jet");
    void AddEventUser(const std::string& detailStr = "");
    void AddTruthMET();
    void AddMETUser(const std::string& detailStr = "", const std::string& metName = "met");

    void FillJetsUser(const xAOD::Jet* jet, const std::string& jetName = "jet");
    void FillEventUser(const xAOD::EventInfo*);
    void FillTruthMET(const xAOD::MissingETContainer*);
    void FillMETUser(const xAOD::MissingETContainer*, const std::string& metName = "met");

    void ClearJetsUser(const std::string& jetName = "jet");
    void ClearEventUser();
    void ClearMETUser(const std::string& metName = "met");

    /** @brief Used to distribute the algorithm to the workers*/
    ClassDef(ZHFHelpTreeBase, 1);
};

#endif
