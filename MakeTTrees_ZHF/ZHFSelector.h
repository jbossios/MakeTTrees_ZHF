/** @file ZHFSelector.h
 *  @brief An event is not discarded if it passes the reco or truth ZHF selections
 *  @author Jona Bossio (jbossios@cern.ch)
 */

#ifndef MakeTTrees_ZHF_ZHFSelector_H
#define MakeTTrees_ZHF_ZHFSelector_H

// EDM include(s):
#include "xAODEventInfo/EventInfo.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "xAODJet/JetContainer.h"
#include "xAODMuon/MuonContainer.h"
#include "xAODEgamma/ElectronContainer.h"

// algorithm wrapper
#include "xAODAnaHelpers/Algorithm.h"

// Infrastructure include(s):
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODRootAccess/TStore.h"

// ROOT include(s):
#include "TH1D.h"
#include "TFile.h"
#include "TLorentzVector.h"

#include <sstream>

#include "AsgTools/AnaToolHandle.h"

// NOTE:
// Variables that don't get filled at submission time should be
// protected from being send from the submission node to the worker
// node (done by the //!)

class ZHFSelector : public xAH::Algorithm
{
  // put your configuration variables here as public variables.
  // that way they can be set directly from CINT and python.
  public:

    // configuration
    /** input container name */
    std::string m_inContainerName_TruthElectrons;
    std::string m_inContainerName_TruthMuons;
    std::string m_inContainerName_RecoElectrons;
    std::string m_inContainerName_RecoMuons;
    std::string m_inputAlgoSystNames  = "";
    std::string m_outputAlgoSystNames = "ZBosonSelection_Syst";
    float m_RecoZmass_min      = -1.; // no cut by default
    float m_RecoZmass_max      = -1.; // no cut by default
    float m_MeVtoGeV           = 0.001;
    bool  m_makeTruthSelection = true; // make truth selections by default in MC

  private:

    // cutflow
    TH1D* m_cutflowHist = nullptr;  //!
    TH1D* m_cutflowHistW = nullptr; //!
    int   m_cutflow_bin;            //!
    int   m_numEventPass;           //!
    int   m_weightNumEventPass;     //!

  public:

    // Constructor
    ZHFSelector ();

    /** @brief Setup the job (inherits from Algorithm)*/
    virtual EL::StatusCode setupJob (EL::Job& job);
    /** @brief Execute the file (inherits from Algorithm)*/
    virtual EL::StatusCode fileExecute ();
    /** @brief Initialize the output histograms before any input file is attached (inherits from Algorithm)*/
    virtual EL::StatusCode histInitialize ();
    /** @brief Change to the next input file (inherits from Algorithm)*/
    virtual EL::StatusCode changeInput (bool firstFile);
    /** @brief Initialize the input file (inherits from Algorithm)*/
    virtual EL::StatusCode initialize ();
    /** @brief Execute each event of the input file (inherits from Algorithm)*/
    virtual EL::StatusCode execute ();
    /** @brief End the event execution (inherits from Algorithm)*/
    virtual EL::StatusCode postExecute ();
    /** @brief Finalize the input file (inherits from Algorithm)*/
    virtual EL::StatusCode finalize ();
    /** @brief Finalize the histograms after all files (inherits from Algorithm)*/
    virtual EL::StatusCode histFinalize ();

  // these are the functions not inherited from Algorithm
    /** @brief Configure the variables once before running*/
    virtual EL::StatusCode configure ();

    // Other functions (not from Algorithm)
    bool executeTruthSelection( const xAOD::TruthParticleContainer* inMuons, const xAOD::TruthParticleContainer* inElectrons, const xAOD::EventInfo* eventInfo);
    bool executeRecoSelection( const xAOD::MuonContainer* inMuons, const xAOD::ElectronContainer* inElectrons, const xAOD::EventInfo* eventInfo, bool decorate = true);

    /** @brief Used to distribute the algorithm to the workers*/
    ClassDef(ZHFSelector, 1);
};

#endif
