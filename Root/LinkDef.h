#include <MakeTTrees_ZHF/ZHFSelector.h>
#include <MakeTTrees_ZHF/ZHFHelpTreeBase.h>
#include <MakeTTrees_ZHF/ZHFTreeAlgo.h>
#include <map>
#include <TString.h>

#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclass;


#endif

#ifdef __CINT__
#pragma link C++ class ZHFSelector+;
#pragma link C++ class ZHFHelpTreeBase+;
#pragma link C++ class ZHFTreeAlgo+;
#pragma link C++ class WeightNamesAlgo+;

#endif
