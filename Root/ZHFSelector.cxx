/*******************************************************************
 *
 * Algorithm to select events passing reco or truth ZHF selections
 *
 * Jona Bossio (jbossioscern.ch)
 *
 ******************************************************************/

// EL includes:
#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>

// EDM includes:
#include "xAODCore/ShallowCopy.h"
#include "AthContainers/ConstDataVector.h"
#include "AthContainers/DataVector.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "xAODJet/JetContainer.h"
#include "xAODMuon/MuonContainer.h"
#include "xAODEgamma/ElectronContainer.h"
#include "xAODEventInfo/EventInfo.h"

// package include:
#include "MakeTTrees_ZHF/ZHFSelector.h"

// xAODAnaHelpers includes
#include "xAODAnaHelpers/HelperClasses.h"
#include <xAODAnaHelpers/HelperFunctions.h>

using namespace std;
using namespace xAH;

// this is needed to distribute the algorithm to the workers
ClassImp(ZHFSelector)

ZHFSelector :: ZHFSelector () :
  Algorithm("ZHFSelector")
{
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0.  Note that you should only put
  // the most basic initialization here, since this method will be
  // called on both the submission and the worker node.  Most of your
  // initialization code will go into histInitialize() and
  // initialize().

}

EL::StatusCode  ZHFSelector :: configure (){

  return EL::StatusCode::SUCCESS;
}


EL::StatusCode ZHFSelector :: setupJob (EL::Job& job)
{
  // Here you put code that sets up the job on the submission object
  // so that it is ready to work with your algorithm, e.g. you can
  // request the D3PDReader service or add output files.  Any code you
  // put here could instead also go into the submission script.  The
  // sole advantage of putting it here is that it gets automatically
  // activated/deactivated when you add/remove the algorithm from your
  // job, which may or may not be of value to you.
  job.useXAOD();
  xAOD::Init( "" ).ignore(); // call before opening first file

  return EL::StatusCode::SUCCESS;
}



EL::StatusCode ZHFSelector :: histInitialize ()
{
  // Here you do everything that needs to be done at the very
  // beginning on each worker node, e.g. create histograms and output
  // trees.  This method gets called before any input files are
  // connected.
  ANA_MSG_DEBUG( "Calling histInitialize");
  ANA_CHECK( xAH::Algorithm::algInitialize())

  return EL::StatusCode::SUCCESS;
}



EL::StatusCode ZHFSelector :: fileExecute ()
{
  // Here you do everything that needs to be done exactly once for every
  // single file, e.g. collect a list of all lumi-blocks processed
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode ZHFSelector :: changeInput (bool firstFile)
{
  // Here you do everything you need to do when we change input files,
  // e.g. resetting branch addresses on trees.  If you are using
  // D3PDReader or a similar service this method is not needed.
  (void)firstFile; //surpress unused param warning
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode ZHFSelector :: initialize ()
{
  // Here you do everything that you need to do after the first input
  // file has been connected and before the first event is processed,
  // e.g. create additional histograms based on which variables are
  // available in the input files.  You can also create all of your
  // histograms and trees in here, but be aware that this method
  // doesn't get called if no events are processed.  So any objects
  // you create here won't be available in the output if you have no
  // input events.

  ANA_MSG_DEBUG( " Calling initialize");

  m_event = wk()->xaodEvent();
  m_store = wk()->xaodStore();

  // Protection
  if ( m_inContainerName_RecoElectrons.empty() ){
    ANA_MSG_FATAL( "m_inContainerName_RecoElectrons is empty!" );
    return EL::StatusCode::FAILURE;
  } else if ( m_inContainerName_RecoMuons.empty() ){
    ANA_MSG_FATAL( "m_inContainerName_RecoMuons is empty!" );
  }
  ANA_MSG_DEBUG("Input reco electron container: " << m_inContainerName_RecoElectrons);
  ANA_MSG_DEBUG("Input reco muon container: " << m_inContainerName_RecoMuons);

  if ( isMC() && m_makeTruthSelection ) {

    // Protections
    if ( m_inContainerName_TruthElectrons.empty() ){
      ANA_MSG_FATAL( "m_inContainerName_TruthElectrons is empty!");
      return EL::StatusCode::FAILURE;
    } else if ( m_inContainerName_TruthMuons.empty() ){
      ANA_MSG_FATAL( "m_inContainerName_TruthMuons is empty!");
      return EL::StatusCode::FAILURE;
    }
    ANA_MSG_DEBUG("Input truth electron container: " << m_inContainerName_TruthElectrons);
    ANA_MSG_DEBUG("Input truth muon container: " << m_inContainerName_TruthMuons);
  
  }

  // Cutflows
  
  // Get file where the histograms are stored
  TFile *file = wk()->getOutputFile("cutflow");

  // Retrieve the event cutflows
  m_cutflowHist  = (TH1D*)file->Get("cutflow");
  m_cutflowHistW = (TH1D*)file->Get("cutflow_weighted");
  m_cutflow_bin  = m_cutflowHist->GetXaxis()->FindBin(m_name.c_str());
  m_cutflowHistW->GetXaxis()->FindBin(m_name.c_str());

  m_numEventPass        = 0;
  m_weightNumEventPass  = 0;

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode ZHFSelector :: execute ()
{
  // Here you do everything that needs to be done on every single
  // events, e.g. read input variables, apply cuts, and fill
  // histograms and trees.  This is where most of your actual analysis
  // code will go.
  
  ANA_MSG_DEBUG( " Calling execute");

  // Retrieve eventInfo
  const xAOD::EventInfo* eventInfo(nullptr);
  ANA_CHECK( HelperFunctions::retrieve(eventInfo, m_eventInfoContainerName, m_event, m_store, msg()) );

  // MC event weight
  float mcEvtWeight(1.0);
  if(eventInfo->eventType( xAOD::EventInfo::IS_SIMULATION ) ){
    static SG::AuxElement::Accessor< float > mcEvtWeightAcc("mcEventWeight");
    if ( ! mcEvtWeightAcc.isAvailable( *eventInfo ) ) {
      ANA_MSG_ERROR( "mcEventWeight is not available as decoration! Aborting" );
      return EL::StatusCode::FAILURE;
    }
    mcEvtWeight = mcEvtWeightAcc( *eventInfo );
  }
  
  ANA_MSG_DEBUG( "Applying ZHF Reco Selection..." );

  // Did any collection pass the cuts?

  bool eventPassReco(false);
  bool count(true); // count for the 1st collection in the container - could be better as
                    // should only count for the nominal (but that's how is implemented in xAH)
  const xAOD::MuonContainer*     inRecoMuons(nullptr);
  const xAOD::ElectronContainer* inRecoElectrons(nullptr);
  auto vecOutContainerNames = std::make_unique< std::vector< std::string > >();

  // if input comes from xAOD, or just running one collection,
  // then get the one collection and be done with it
  //
  if ( m_inputAlgoSystNames.empty() ) {

    // Get reco muons and electrons
    if(m_store->contains<ConstDataVector<xAOD::MuonContainer> >(m_inContainerName_RecoMuons)){
      ANA_CHECK( HelperFunctions::retrieve(inRecoMuons, m_inContainerName_RecoMuons, m_event, m_store, msg()) );
    } else {
      ANA_MSG_ERROR( "m_inContainerName_RecoMuons not found!" );
    }
    if(m_store->contains<ConstDataVector<xAOD::ElectronContainer> >(m_inContainerName_RecoElectrons)){
      ANA_CHECK( HelperFunctions::retrieve(inRecoElectrons, m_inContainerName_RecoElectrons, m_event, m_store, msg()) );
    } else {
      ANA_MSG_ERROR( "m_inContainerName_RecoElectrons not found!" );
    }

    eventPassReco = executeRecoSelection( inRecoMuons, inRecoElectrons, eventInfo );

  } else { // get the list of systematics to run over

    // get vector of string giving the syst names of the upstream algo from TStore (rememeber: 1st element is a blank string: nominal case!)
    std::vector< std::string >* systNames(nullptr);
    ANA_CHECK( HelperFunctions::retrieve(systNames, m_inputAlgoSystNames, 0, m_store, msg()) );

    // prepare a vector of the names of CDV containers for usage by downstream algos
    // must be a pointer to be recorded in TStore
    ANA_MSG_DEBUG( " input list of syst size: " << static_cast<int>(systNames->size()) );

    // loop over systematic sets
    bool eventPassThisSystReco(false);
    for ( auto systName : *systNames ) {

      ANA_MSG_DEBUG( " syst name: " << systName);
      ANA_MSG_DEBUG( "  input container name: " << m_inContainerName_RecoMuons+systName );
      ANA_MSG_DEBUG( "  input container name: " << m_inContainerName_RecoElectrons+systName );

      // Get reco electrons and muons
      if(m_store->contains<ConstDataVector<xAOD::MuonContainer> >(m_inContainerName_RecoMuons + systName)){
        ANA_CHECK( HelperFunctions::retrieve(inRecoMuons, m_inContainerName_RecoMuons + systName, m_event, m_store, msg()) );
      }
      if(m_store->contains<ConstDataVector<xAOD::ElectronContainer> >(m_inContainerName_RecoElectrons + systName)){
        ANA_CHECK( HelperFunctions::retrieve(inRecoElectrons, m_inContainerName_RecoElectrons + systName, m_event, m_store, msg()) );
      }

      // find out if the event passes the event selection
      eventPassThisSystReco = executeRecoSelection( inRecoMuons, inRecoElectrons, eventInfo, count);
      if(count) count = false; // only count for 1 collection

      if ( eventPassThisSystReco ) {
        // save the string of syst set under question if event is passing the selection
        vecOutContainerNames->push_back( systName );
      }

      // if for at least one syst set the event passes selection, this will remain true!
      eventPassReco = ( eventPassReco || eventPassThisSystReco );

    } // close loop over syst sets
 
  }

  bool eventPassTruth(true);
  if ( isMC() && m_makeTruthSelection ) {
  
    ANA_MSG_DEBUG( "Applying ZHF Truth Selection..." ); 

    // Did any collection pass the cuts?

    const xAOD::TruthParticleContainer* inTruthMuons(nullptr);
    const xAOD::TruthParticleContainer* inTruthElectrons(nullptr);

    // Get truth muons and electrons
    if(m_store->contains<ConstDataVector<xAOD::TruthParticleContainer> >(m_inContainerName_TruthMuons)){
      ANA_CHECK( HelperFunctions::retrieve(inTruthMuons, m_inContainerName_TruthMuons, m_event, m_store, msg()) );
    } else {
      ANA_MSG_ERROR( "m_inContainerName_TruthMuons not found!" );
      return EL::StatusCode::FAILURE;
    }
    if(m_store->contains<ConstDataVector<xAOD::TruthParticleContainer> >(m_inContainerName_TruthElectrons)){
      ANA_CHECK( HelperFunctions::retrieve(inTruthElectrons, m_inContainerName_TruthElectrons, m_event, m_store, msg()) );
    } else {
      ANA_MSG_ERROR( "m_inContainerName_TruthElectrons not found!" );
      return EL::StatusCode::FAILURE;
    }
    
    // find out if the event passes the selections
    eventPassTruth = executeTruthSelection( inTruthMuons, inTruthElectrons, eventInfo);
        
  }

  bool eventPass = false;
  if(isMC() && m_makeTruthSelection) eventPass = eventPassReco || eventPassTruth;
  else                               eventPass = eventPassReco; // data or MC (backgrounds or systematics) w/o truth

  if(eventPass) m_cutflowHist->Fill( m_cutflow_bin, 1 );

  if ( !m_inputAlgoSystNames.empty() ) {
    ANA_MSG_DEBUG(" output list of syst size: " << static_cast<int>(vecOutContainerNames->size()) );
    // record in TStore the list of systematics names that should be considered down stream
    ANA_CHECK( m_store->record( std::move(vecOutContainerNames), m_outputAlgoSystNames));
  }

  // look what we have in TStore
  if(msgLvl(MSG::VERBOSE)) m_store->print();

  if( !eventPass ) {
    wk()->skipEvent();
    return EL::StatusCode::SUCCESS;
  }
  
  if(isMC()) ANA_MSG_DEBUG( "This event passes reco or truth selections" );
  else ANA_MSG_DEBUG( "This event passes reco selections" ); // data
 
  m_numEventPass++;
  m_weightNumEventPass += mcEvtWeight;

  ANA_MSG_DEBUG( "Left ZHF Selection..." );

  return EL::StatusCode::SUCCESS;
}


bool ZHFSelector :: executeRecoSelection ( const xAOD::MuonContainer* inMuons, const xAOD::ElectronContainer* inElectrons, const xAOD::EventInfo* eventInfo, bool decorate)
{

  ANA_MSG_DEBUG( "In  executeRecoSelection..." );

  bool passSel = true;

  int nMuons     = inMuons->size();
  int nElectrons = inElectrons->size();
  int nLeptons   = nMuons + nElectrons;

  // Ask for exactly two muons
  if(nLeptons!=2){
    ANA_MSG_DEBUG( "Selection not passed (nRecoLeptons!=2)" );
    passSel = false;
  } else { // exactly two leptons
    // request them to have opposite charge
    int nNegative = 0;
    int nPositive = 0;
    std::vector<TLorentzVector> selectedLeptons;
    for ( auto mu_itr : *inMuons ) { // loop over muons
      TLorentzVector lep;
      lep.SetPtEtaPhiE(mu_itr->pt()*m_MeVtoGeV, mu_itr->eta(), mu_itr->phi(), mu_itr->e()*m_MeVtoGeV);
      selectedLeptons.push_back(lep);
      if(mu_itr->charge()==-1) nNegative++;
      else if(mu_itr->charge()==1) nPositive++;
    }
    for ( auto el_itr : *inElectrons ) { // loop over electrons
      TLorentzVector lep;
      lep.SetPtEtaPhiE(el_itr->pt()*m_MeVtoGeV, el_itr->eta(), el_itr->phi(), el_itr->e()*m_MeVtoGeV);
      selectedLeptons.push_back(lep);
      if(el_itr->charge()==-1) nNegative++;
      else if(el_itr->charge()==1) nPositive++;
    }
    ANA_MSG_DEBUG( "nNegativeLeptons: " << nNegative );
    ANA_MSG_DEBUG( "nPositiveLeptons: " << nPositive );
    if(nNegative!=1 && nPositive!=1) {
      ANA_MSG_DEBUG( "Selection not passed (the two reco leptons have the same charge)" );
      passSel = false;
    }

    if(passSel){
      // apply Z mass cut
      float Zmass = (selectedLeptons.at(0)+selectedLeptons.at(1)).M();
      ANA_MSG_DEBUG( "Zmass: " << Zmass );
      if(m_RecoZmass_min!=-1 && Zmass < m_RecoZmass_min) passSel = false;
      if(m_RecoZmass_max!=-1 && Zmass > m_RecoZmass_max) passSel = false;
      if(!passSel) ANA_MSG_DEBUG( "Selection not passed (Zmass cut not satisfied)" );
    }
    selectedLeptons.clear();
  }

  if(passSel) ANA_MSG_DEBUG( "ZHF Reco selection passed" );

  // Decorate EventInfo container with reco selection decision
  if(decorate){
    static SG::AuxElement::Decorator<int> passRecoSelDecor("PassRecoSelections");
    passRecoSelDecor(*eventInfo) = passSel;
  }

  return passSel;
}


bool ZHFSelector :: executeTruthSelection ( const xAOD::TruthParticleContainer* inMuons, const xAOD::TruthParticleContainer* inElectrons, const xAOD::EventInfo* eventInfo)
{

  ANA_MSG_DEBUG( "In  executeTruthSelection..." );

  bool passSel = true;
  
  static SG::AuxElement::Decorator< int > passTruthSelDecor("PassTruthSelections");

  int nMuons     = inMuons->size();
  int nElectrons = inElectrons->size();
  int nLeptons   = nMuons + nElectrons;
 
  // Require exactly two leptons
  if(nLeptons!=2) { // not exactly two leptons
    ANA_MSG_DEBUG( "Selection not passed (nTruthLeptons!=2)" );
    passSel = false;
  } else { // exactly two leptons
    // request muons/electrons to have opposite charge
    int nNegative = 0;
    int nPositive = 0;
    std::vector<TLorentzVector> selectedLeptons;
    for ( auto lep_itr : *inMuons ) { // loop over muons
      TLorentzVector lep;
      lep.SetPtEtaPhiE(lep_itr->pt()*m_MeVtoGeV, lep_itr->eta(), lep_itr->phi(), lep_itr->e()*m_MeVtoGeV);
      selectedLeptons.push_back(lep);
      if(lep_itr->charge()==-1) nNegative++;
      else if(lep_itr->charge()==1) nPositive++;
    }
    for ( auto lep_itr : *inElectrons ) { // loop over electrons
      TLorentzVector lep;
      lep.SetPtEtaPhiE(lep_itr->pt()*m_MeVtoGeV, lep_itr->eta(), lep_itr->phi(), lep_itr->e()*m_MeVtoGeV);
      selectedLeptons.push_back(lep);
      if(lep_itr->charge()==-1) nNegative++;
      else if(lep_itr->charge()==1) nPositive++;
    }
    ANA_MSG_DEBUG( "nNegativeLeptons: " << nNegative );
    ANA_MSG_DEBUG( "nPositiveLeptons: " << nPositive );
    if(nNegative!=1 && nPositive!=1) {
      ANA_MSG_DEBUG( "Selection not passed (the two truth leptons have the same charge)" );
      passSel = false;
    }
  }

  if(passSel) ANA_MSG_DEBUG( "ZHF Truth selection passed" );

  // Decorate EventInfo container with truth selection decision
  passTruthSelDecor(*eventInfo) = passSel;

  return passSel;
}



EL::StatusCode ZHFSelector :: postExecute ()
{
  ANA_MSG_VERBOSE("postExecute()");
  // Here you do everything that needs to be done after the main event
  // processing.  This is typically very rare, particularly in user
  // code.  It is mainly used in implementing the NTupleSvc.
  return EL::StatusCode::SUCCESS;
}


EL::StatusCode ZHFSelector :: finalize ()
{
  ANA_MSG_DEBUG("finalize()");
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk.  This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.  This is different from histFinalize() in that it only
  // gets called on worker nodes that processed input events.

  ANA_MSG_DEBUG( "Filling cutflow");
  m_cutflowHist ->SetBinContent( m_cutflow_bin, m_numEventPass        );
  m_cutflowHistW->SetBinContent( m_cutflow_bin, m_weightNumEventPass  );

  ANA_MSG_DEBUG("Done finalize");

  return EL::StatusCode::SUCCESS;
}


EL::StatusCode ZHFSelector :: histFinalize ()
{
  ANA_MSG_DEBUG("histFinalize()");
  // This method is the mirror image of histInitialize(), meaning it
  // gets called after the last event has been processed on the worker
  // node and allows you to finish up any objects you created in
  // histInitialize() before they are written to disk.  This is
  // actually fairly rare, since this happens separately for each
  // worker node.  Most of the time you want to do your
  // post-processing on the submission node after all your histogram
  // outputs have been merged.  This is different from finalize() in
  // that it gets called on all worker nodes regardless of whether
  // they processed input events.


  ANA_CHECK( xAH::Algorithm::algFinalize());
  return EL::StatusCode::SUCCESS;
}
