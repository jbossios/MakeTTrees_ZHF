/*******************************************************************
 *
 * Algorithm to select events passing reco or truth ZHF selections
 *
 * Jona Bossio (jbossioscern.ch)
 *
 ******************************************************************/

// EL includes:
#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>

// EDM includes:
#include "xAODCore/ShallowCopy.h"
#include "AthContainers/ConstDataVector.h"
#include "AthContainers/DataVector.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "xAODJet/JetContainer.h"
#include "xAODMuon/MuonContainer.h"
#include "xAODEgamma/ElectronContainer.h"
#include "xAODEventInfo/EventInfo.h"

// package include:
#include "MakeTTrees_ZHF/ZHFHelpTreeBase.h"

// xAODAnaHelpers includes
#include "xAODAnaHelpers/HelperClasses.h"
#include <xAODAnaHelpers/HelperFunctions.h>

using namespace std;
using namespace xAH;

// this is needed to distribute the algorithm to the workers
ClassImp(ZHFHelpTreeBase)

ZHFHelpTreeBase :: ZHFHelpTreeBase(xAOD::TEvent *event, TTree* tree, TFile* file, const float units, bool debug, xAOD::TStore* store) :
  HelpTreeBase(event, tree, file, units, debug, store)
{
  Info("ZHFHelpTreeBase", "Creating output TTree  %s", tree->GetName());
}

ZHFHelpTreeBase :: ~ZHFHelpTreeBase()
{
}

// Jets
void ZHFHelpTreeBase::AddJetsUser(const std::string& detailStr, const std::string& jetName)
{
  // BTagging
  if(jetName == "jet" || jetName == "trackjet"){ // only for reco jets
    if(detailStr.find("BTagging_MV2c10_Continuous")!= std::string::npos) m_ContinuousBTaggers.push_back("MV2c10");
    if(detailStr.find("BTagging_DL1_Continuous")!= std::string::npos) m_ContinuousBTaggers.push_back("DL1");
    if(detailStr.find("BTagging_DL1r_Continuous")!= std::string::npos) m_ContinuousBTaggers.push_back("DL1r");
    for(unsigned int itag=0;itag<m_ContinuousBTaggers.size();++itag){
      //std::string branchWeightName   = jetName + "_BTagWeight_" + m_ContinuousBTaggers.at(itag);
      std::string branchQuantileName = jetName + "_BTagQuantile_" + m_ContinuousBTaggers.at(itag);
      std::string branchSFName       = jetName + "_BTagSF_" + m_ContinuousBTaggers.at(itag);
      //std::string branchIneffSFName  = jetName + "_BTagInefficiencySF_" + m_ContinuousBTaggers.at(itag);
      if(m_ContinuousBTaggers.at(itag)=="MV2c10"){
        //m_tree->Branch(branchWeightName.c_str(), &m_jet_MV2c10_BTagWeight);
        m_tree->Branch(branchQuantileName.c_str(), &m_jet_MV2c10_BTagQuantile);
      } else if (m_ContinuousBTaggers.at(itag)=="DL1"){
        //m_tree->Branch(branchWeightName.c_str(), &m_jet_DL1_BTagWeight);
        m_tree->Branch(branchQuantileName.c_str(), &m_jet_DL1_BTagQuantile);
      } else if (m_ContinuousBTaggers.at(itag)=="DL1r"){
        //m_tree->Branch(branchWeightName.c_str(), &m_jet_DL1r_BTagWeight);
        m_tree->Branch(branchQuantileName.c_str(), &m_jet_DL1r_BTagQuantile);
      }
      if(m_isMC){
        if(m_ContinuousBTaggers.at(itag)=="MV2c10"){
          m_tree->Branch(branchSFName.c_str(), &m_jet_MV2c10_BTagSF);
          //m_tree->Branch(branchIneffSFName.c_str(), &m_jet_MV2c10_BTagIneffSF);
	} else if(m_ContinuousBTaggers.at(itag)=="DL1"){
          m_tree->Branch(branchSFName.c_str(), &m_jet_DL1_BTagSF);
          //m_tree->Branch(branchIneffSFName.c_str(), &m_jet_DL1_BTagIneffSF);
	} else if(m_ContinuousBTaggers.at(itag)=="DL1r"){
          m_tree->Branch(branchSFName.c_str(), &m_jet_DL1r_BTagSF);
          //m_tree->Branch(branchIneffSFName.c_str(), &m_jet_DL1r_BTagIneffSF);
	}
      }
    }
  }

  // HadronConeExclTruthLabelID and PartonTruthLabelID
  if(m_isMC && detailStr.find("Truth")!= std::string::npos){ // only in MC
    m_addJetTruthInfo = true;
    std::string branchName = jetName + "_HadronConeExclTruthLabelID";
    if(jetName == "jet" || jetName == "trackjet" || jetName == "fatjet"){ // reco jet
      m_tree->Branch(branchName.c_str(), &m_jet_HadronConeExclTruthLabelID);
    } else if(jetName == "truthJet" || jetName == "truth_fatjet" || jetName == "truth_trackjet"){ // truth jet
      m_tree->Branch(branchName.c_str(), &m_truthJet_HadronConeExclTruthLabelID);
    }
    branchName = jetName + "_PartonTruthLabelID";
    if(jetName == "jet" || jetName == "trackjet" || jetName == "fatjet"){ // reco jet
      m_tree->Branch(branchName.c_str(), &m_jet_PartonTruthLabelID);
    } else if(jetName == "truthJet" || jetName == "truth_fatjet" || jetName == "truth_trackjet"){ // truth jet
      m_tree->Branch(branchName.c_str(), &m_truthJet_PartonTruthLabelID);
    }
  }
}

void ZHFHelpTreeBase::FillJetsUser( const xAOD::Jet* jet, const std::string& jetName) {
  // BTagging
  if(jetName == "jet" || jetName == "trackjet"){ // only for reco jets
    for(unsigned int itag=0;itag<m_ContinuousBTaggers.size();++itag){
      /*double tagWeight;
      bool status = jet->getAttribute<double>( "BTag_Weight_"+m_ContinuousBTaggers.at(itag)+"_Continuous", tagWeight );
      if(status){
        if(m_ContinuousBTaggers.at(itag)=="MV2c10")    m_jet_MV2c10_BTagWeight.push_back(tagWeight);
	else if(m_ContinuousBTaggers.at(itag)=="DL1")  m_jet_DL1_BTagWeight.push_back(tagWeight);
	else if(m_ContinuousBTaggers.at(itag)=="DL1r") m_jet_DL1r_BTagWeight.push_back(tagWeight);
      }else{
        if(m_ContinuousBTaggers.at(itag)=="MV2c10")    m_jet_MV2c10_BTagWeight.push_back(-999);
	else if(m_ContinuousBTaggers.at(itag)=="DL1")  m_jet_DL1_BTagWeight.push_back(-999);
	else if(m_ContinuousBTaggers.at(itag)=="DL1r") m_jet_DL1r_BTagWeight.push_back(-999);
      }*/
      int quantile;
      bool status = jet->getAttribute<int>( "BTag_Quantile_"+m_ContinuousBTaggers.at(itag)+"_Continuous", quantile );
      if(status){
        if(m_ContinuousBTaggers.at(itag)=="MV2c10")    m_jet_MV2c10_BTagQuantile.push_back(quantile);
	else if(m_ContinuousBTaggers.at(itag)=="DL1")  m_jet_DL1_BTagQuantile.push_back(quantile);
	else if(m_ContinuousBTaggers.at(itag)=="DL1r") m_jet_DL1r_BTagQuantile.push_back(quantile);
      }else{
        if(m_ContinuousBTaggers.at(itag)=="MV2c10")    m_jet_MV2c10_BTagQuantile.push_back(-999);
	else if(m_ContinuousBTaggers.at(itag)=="DL1")  m_jet_DL1_BTagQuantile.push_back(-999);
	else if(m_ContinuousBTaggers.at(itag)=="DL1r") m_jet_DL1r_BTagQuantile.push_back(-999);
      }
      if(m_isMC){
        std::vector<float> SF;
        std::vector<float> junk(1,-999);
        status = jet->getAttribute< std::vector<float> >( "BTag_SF_"+m_ContinuousBTaggers.at(itag)+"_Continuous", SF );
        if(status){
          if(m_ContinuousBTaggers.at(itag)=="MV2c10")    m_jet_MV2c10_BTagSF.push_back(SF);
	  else if(m_ContinuousBTaggers.at(itag)=="DL1")  m_jet_DL1_BTagSF.push_back(SF);
	  else if(m_ContinuousBTaggers.at(itag)=="DL1r") m_jet_DL1r_BTagSF.push_back(SF);
        }else{
          if(m_ContinuousBTaggers.at(itag)=="MV2c10")    m_jet_MV2c10_BTagSF.push_back(junk);
	  else if(m_ContinuousBTaggers.at(itag)=="DL1")  m_jet_DL1_BTagSF.push_back(junk);
	  else if(m_ContinuousBTaggers.at(itag)=="DL1r") m_jet_DL1r_BTagSF.push_back(junk);
        }
        /*std::vector<float> ineffSF;
        status = jet->getAttribute< std::vector<float> >( "BTag_InefficiencySF_"+m_ContinuousBTaggers.at(itag)+"_Continuous", ineffSF );
        if(status){
          if(m_ContinuousBTaggers.at(itag)=="MV2c10")    m_jet_MV2c10_BTagIneffSF.push_back(ineffSF);
	  else if(m_ContinuousBTaggers.at(itag)=="DL1")  m_jet_DL1_BTagIneffSF.push_back(ineffSF);
	  else if(m_ContinuousBTaggers.at(itag)=="DL1r") m_jet_DL1r_BTagIneffSF.push_back(ineffSF);
        }else{
          if(m_ContinuousBTaggers.at(itag)=="MV2c10")    m_jet_MV2c10_BTagIneffSF.push_back(junk);
	  else if(m_ContinuousBTaggers.at(itag)=="DL1")  m_jet_DL1_BTagIneffSF.push_back(junk);
	  else if(m_ContinuousBTaggers.at(itag)=="DL1r") m_jet_DL1r_BTagIneffSF.push_back(junk);
        }*/
      }
    }
  }
  // HadronConeExclTruthLabelID and PartonTruthLabelID
  if(m_isMC && m_addJetTruthInfo) {
    int HadronConeExclTruthLabelID;
    bool status = jet->getAttribute<int>("HadronConeExclTruthLabelID", HadronConeExclTruthLabelID);
    if(jetName == "jet" || jetName == "trackjet" || jetName == "fatjet"){
      if(status){ m_jet_HadronConeExclTruthLabelID.push_back(HadronConeExclTruthLabelID); }
      else{ m_jet_HadronConeExclTruthLabelID.push_back(-999); }
    } else if(jetName == "truthJet"){
      if(status){ m_truthJet_HadronConeExclTruthLabelID.push_back(HadronConeExclTruthLabelID); }
      else{ m_truthJet_HadronConeExclTruthLabelID.push_back(-999); }
    }
    int PartonTruthLabelID;
    status = jet->getAttribute<int>("PartonTruthLabelID", PartonTruthLabelID);
    if(jetName == "jet" || jetName == "trackjet" || jetName == "fatjet"){ // reco jet
      if(status){ m_jet_PartonTruthLabelID.push_back(PartonTruthLabelID); }
      else{ m_jet_PartonTruthLabelID.push_back(-999); }
    } else if(jetName == "truthJet" || jetName == "truth_fatjet" || jetName == "truth_trackjet"){ // truth jet
      if(status){ m_truthJet_PartonTruthLabelID.push_back(PartonTruthLabelID); }
      else{ m_truthJet_PartonTruthLabelID.push_back(-999); }
    }
  }
}

void ZHFHelpTreeBase::ClearJetsUser(const std::string& jetName) {
  if(jetName == "jet" || jetName == "trackjet"){ // only for reco jets
    //m_jet_MV2c10_BTagWeight.clear();
    m_jet_MV2c10_BTagQuantile.clear();
    //m_jet_DL1_BTagWeight.clear();
    m_jet_DL1_BTagQuantile.clear();
    //m_jet_DL1r_BTagWeight.clear();
    m_jet_DL1r_BTagQuantile.clear();
    if(m_isMC){
      m_jet_MV2c10_BTagSF.clear();
      //m_jet_MV2c10_BTagIneffSF.clear();
      m_jet_DL1_BTagSF.clear();
      //m_jet_DL1_BTagIneffSF.clear();
      m_jet_DL1r_BTagSF.clear();
      //m_jet_DL1r_BTagIneffSF.clear();
    }
  }
  if(m_isMC && m_addJetTruthInfo){
    if(jetName == "jet" || jetName == "trackjet" || jetName == "fatjet"){
      m_jet_HadronConeExclTruthLabelID.clear();
      m_jet_PartonTruthLabelID.clear();
    } else if(jetName == "truthJet" || jetName == "truth_fatjet" || jetName == "truth_trackjet"){
      m_truthJet_HadronConeExclTruthLabelID.clear();
      m_truthJet_PartonTruthLabelID.clear();
    }
  }
}

// Event info
void ZHFHelpTreeBase::AddEventUser(const std::string& detailStr) {
  (void)detailStr;
  if(m_isMC){
    Info("AddEventUser","Adding event information");
    if(detailStr.find("JetCleaning")!= std::string::npos){
      m_addJetLooseBad       = true;
      std::string branchName = "DFCommonJets_eventClean_LooseBad";
      m_tree->Branch(branchName.c_str(), &m_DFCommonJets_eventClean_LooseBad);
    }
    if(detailStr.find("PassTruthSel")!= std::string::npos){
      m_addPassTruthSelections = true;
      std::string branchName = "PassTruthSelections";
      m_tree->Branch(branchName.c_str(), &m_passTruthSelections);
    }
    if(detailStr.find("PassRecoSel")!= std::string::npos){
      m_addPassRecoSelections = true;
      std::string branchName = "PassRecoSelections";
      m_tree->Branch(branchName.c_str(), &m_passRecoSelections);
    }
  }
}

void ZHFHelpTreeBase::FillEventUser(const xAOD::EventInfo* eventInfo) {
  if(m_isMC){
    // Jet cleaning event flag
    if(m_addJetLooseBad){
      if(eventInfo->isAvailable<char>("DFCommonJets_eventClean_LooseBad")) {
        int JetCleaningFlag = eventInfo->auxdataConst<char>("DFCommonJets_eventClean_LooseBad");
        m_DFCommonJets_eventClean_LooseBad.push_back(JetCleaningFlag);
      } else {
        m_DFCommonJets_eventClean_LooseBad.push_back(-999);
      }
    }
    // Pass truth selections flag
    if(m_addPassTruthSelections){
      if(eventInfo->isAvailable<int>("PassTruthSelections")) {
        int TruthSelFlag = eventInfo->auxdataConst<int>("PassTruthSelections");
        m_passTruthSelections.push_back(TruthSelFlag);
      } else {
        m_passTruthSelections.push_back(0);
      }
    }
    // Pass reco selections flag
    if(m_addPassRecoSelections){
      if(eventInfo->isAvailable<int>("PassRecoSelections")) {
        int RecoSelFlag = eventInfo->auxdataConst<int>("PassRecoSelections");
        m_passRecoSelections.push_back(RecoSelFlag);
      } else {
        m_passRecoSelections.push_back(0);
      }
    }
  }
}

void ZHFHelpTreeBase::ClearEventUser() {
  if(m_isMC){
    if(m_addJetLooseBad)         m_DFCommonJets_eventClean_LooseBad.clear();
    if(m_addPassTruthSelections) m_passTruthSelections.clear();
    if(m_addPassRecoSelections)  m_passRecoSelections.clear();
  }
}


// Truth MET
void ZHFHelpTreeBase::AddTruthMET() {
  if(m_isMC){
    Info("AddTruthMET","Adding truth MET");
    std::string branchName = "truthMET_NonInt_sumet";
    m_tree->Branch(branchName.c_str(), &m_truthMET_NonInt_sumet);
    branchName = "truthMET_NonInt_phi";
    m_tree->Branch(branchName.c_str(), &m_truthMET_NonInt_phi);
    branchName = "truthMET_Int_sumet";
    m_tree->Branch(branchName.c_str(), &m_truthMET_Int_sumet);
    branchName = "truthMET_Int_phi";
    m_tree->Branch(branchName.c_str(), &m_truthMET_Int_phi);
    branchName = "truthMET_IntOut_sumet";
    m_tree->Branch(branchName.c_str(), &m_truthMET_IntOut_sumet);
    branchName = "truthMET_IntOut_phi";
    m_tree->Branch(branchName.c_str(), &m_truthMET_IntOut_phi);
    branchName = "truthMET_IntMuons_sumet";
    m_tree->Branch(branchName.c_str(), &m_truthMET_IntMuons_sumet);
    branchName = "truthMET_IntMuons_phi";
    m_tree->Branch(branchName.c_str(), &m_truthMET_IntMuons_phi);
  }
}

void ZHFHelpTreeBase::FillTruthMET(const xAOD::MissingETContainer* met) {
  if(m_isMC){
    const xAOD::MissingET* truthNonInt = *met->find("NonInt"); //  all stable, non-interacting particles including neutrinos, SUSY LSPs, Kaluza-Klein particles etc.
    m_truthMET_NonInt_sumet = truthNonInt->sumet() / m_units;
    m_truthMET_NonInt_phi   = truthNonInt->phi();
    const xAOD::MissingET* truthInt = *met->find("Int"); // all stable, interacting particles within detector acceptance (|eta|<5) excluding muons (approximate calo term)
    m_truthMET_Int_sumet = truthInt->sumet() / m_units;
    m_truthMET_Int_phi   = truthInt->phi();
    const xAOD::MissingET* truthIntOut = *met->find("IntOut"); // all stable, interacting particles outside detector acceptance (|eta|>5)
    m_truthMET_IntOut_sumet = truthIntOut->sumet() / m_units;
    m_truthMET_IntOut_phi   = truthIntOut->phi();
    const xAOD::MissingET* truthIntMuons = *met->find("IntMuons"); // all final state muons in |eta|<5 and with pt>6 GeV
    m_truthMET_IntMuons_sumet = truthIntMuons->sumet() / m_units;
    m_truthMET_IntMuons_phi   = truthIntMuons->phi();
  }
}


// MET
void ZHFHelpTreeBase::AddMETUser(const std::string& detailStr, const std::string& metName)
{
  (void)detailStr;
  std::string branchName = metName + "FinalTrk";
  m_tree->Branch(branchName.c_str(), &m_metFinalTrk);
}

void ZHFHelpTreeBase::FillMETUser( const xAOD::MissingETContainer* met, const std::string& metName) {
  (void)metName;
  const xAOD::MissingET* final_trk = *met->find("FinalTrk"); // "FinalTrk" uses the track-based soft terms)
  m_metFinalTrk                    = final_trk->met() / m_units;
}

void ZHFHelpTreeBase::ClearMETUser(const std::string& metName) {
  (void)metName;
  m_metFinalTrk = -999;
}
