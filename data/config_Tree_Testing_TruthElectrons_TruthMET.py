import ROOT
from MakeTTrees_ZHF import CommonDefs as ZHF_config
from xAODAnaHelpers import Config     as xAH_config

c = xAH_config()

jetAlgo = "AntiKt4EMTopo"
jetType = jetAlgo.replace("AntiKt4","")

c.algorithm("BasicEventSelection", { 
  "m_name"                           : "BasicSelection",
  "m_msgLevel"                       : ZHF_config.msgLevel,
  "m_applyGRLCut"                    : False,
  "m_derivationName"                 : ZHF_config.derivationName,
  "m_useMetaData"                    : True,
  "m_storePassHLT"                   : False,
  "m_storeTrigDecisions"             : False,
  "m_applyTriggerCut"                : False,
  "m_checkDuplicatesMC"              : True,
  "m_applyJetCleaningEventFlag"      : ZHF_config.applyJetCleaningEventFlagMC,
  "m_applyEventCleaningCut"          : ZHF_config.applyEventCleaningCut,
  "m_PVNTrack"                       : ZHF_config.minPVnTrack,
  "m_applyPrimaryVertexCut"          : ZHF_config.applyPrimaryVertexCut,
  "m_vertexContainerName"            : ZHF_config.vertexContainerName,
  "m_doPUreweighting"                : True,
  "m_lumiCalcFileNames"              : ','.join(ZHF_config.iLumis["MC16a"]),
  "m_PRWFileNames"                   : ','.join(ZHF_config.PRWs["MC16a"]),
  } )

c.algorithm("ElectronCalibrator", {
  "m_name"                           : "ElectronCalibrator",
  "m_sort"                           : True,
  "m_esModel"                        : "es2018_R21_v0",
  "m_inContainerName"                : ZHF_config.ELCalib_inContainer,
  "m_outContainerName"               : ZHF_config.ELCalib_outContainer,
  } )

c.algorithm("ElectronSelector", {
  "m_name"                           : "ElectronBaseSelector",
  "m_msgLevel"                       : ZHF_config.msgLevel,
  "m_pT_min"                         : ZHF_config.ELBaseSel_pT_min,
  "m_eta_max"                        : ZHF_config.ELBaseSel_eta_max,
  "m_z0sintheta_max"                 : ZHF_config.ELBaseSel_z0sintheta_max,
  "m_d0sig_max"                      : ZHF_config.ELBaseSel_d0sig_max,
  "m_IsoWPList"                      : ZHF_config.ELBaseSel_IsoWPList,
  "m_inContainerName"                : ZHF_config.ELBaseSel_inContainer,
  "m_outContainerName"               : ZHF_config.ELBaseSel_outContainer,
  "m_readIDFlagsFromDerivation"      : ZHF_config.ELBaseSel_readIDFlags,
  "m_doLHPIDcut"                     : ZHF_config.ELBaseSel_doLHPIDcut,
  "m_LHOperatingPoint"               : ZHF_config.ELBaseSel_LHWP,
  "m_createSelectedContainer"        : True,
  "m_decorateSelectedObjects"        : True,
  } )

c.algorithm("JetCalibrator", {
  "m_name"                           : "JetCalibrator",
  "m_calibConfigFullSim"             : ZHF_config.EMcalibConfigFullSim,
  "m_calibSequence"                  : ZHF_config.calibSequenceMC,
  "m_sort"                           : True,
  "m_doCleaning"                     : ZHF_config.doCleaning,
  "m_jetCleanCutLevel"               : ZHF_config.jetCleanCutLevel,
  "m_saveAllCleanDecisions"          : False,
  "m_inContainerName"                : jetAlgo+"Jets",
  "m_outContainerName"               : ZHF_config.JETCalib_outContainer,
  "m_jetAlgo"                        : jetAlgo,
  "m_redoJVT"                        : ZHF_config.redoJVT,
  "m_calculatefJVT"                  : ZHF_config.calculatefJVT,
  "m_addGhostMuonsToJets"            : ZHF_config.addGhostMuonsToJets,
  } )

c.algorithm("JetSelector", {
  "m_name"                           : "JetBaseSelector",
  "m_msgLevel"                       : ZHF_config.msgLevel,
  "m_pT_min"                         : ZHF_config.JETBaseSel_pT_min,
  "m_inContainerName"                : ZHF_config.JETBaseSel_inContainer,
  "m_outContainerName"               : ZHF_config.JETBaseSel_outContainer,
  "m_cleanJets"                      : ZHF_config.JETBaseSel_cleanJets,
  "m_cleanEvent"                     : ZHF_config.JETBaseSel_cleanEvent,
  "m_createSelectedContainer"        : True,
  "m_decorateSelectedObjects"        : True,
  "m_doJVT"                          : ZHF_config.JETBaseSel_doJVT,
  "m_noJVTVeto"                      : ZHF_config.JETBaseSel_noJVTVeto,
  "m_WorkingPointJVT"                : ZHF_config.JETBaseSel_JVTwp,
  "m_SFFileJVT"                      : ZHF_config.JETBaseSel_SFFileJVT+jetType+"Jets.root",
  "m_dofJVT"                         : ZHF_config.JETBaseSel_dofJVT,
  } )

c.algorithm("ElectronEfficiencyCorrector", {
  "m_name"                        : "ElectronEfficiencyCorrector",
  "m_msgLevel"                    : ZHF_config.msgLevel,
  "m_inContainerName"             : ZHF_config.ELEff_inContainer,
  "m_WorkingPointReco"            : ZHF_config.ELEff_WorkingPointReco,
  "m_WorkingPointPID"             : ZHF_config.ELEff_WorkingPointPID,
  "m_WorkingPointIso"             : ZHF_config.ELEff_WorkingPointIso,
  "m_WorkingPointTrig"            : ZHF_config.ELEff_WorkingPointTrig,
  } )

# Calibrate and select muons (just for MET)
c.algorithm("MuonCalibrator", {
  "m_name"                        : "MuonCalibrator",
  "m_sort"                        : True,
  "m_inContainerName"             : ZHF_config.MUCalib_inContainer,
  "m_outContainerName"            : ZHF_config.MUCalib_outContainer,
  } )
c.algorithm("MuonSelector", {
  "m_name"                        : "MuonBaseSelector",
  "m_msgLevel"                    : ZHF_config.msgLevel,
  "m_removeEventBadMuon"          : ZHF_config.MUBaseSel_rmEventBadMuon,
  "m_pT_min"                      : ZHF_config.MUBaseSel_pT_min,
  "m_eta_max"                     : ZHF_config.MUBaseSel_eta_max,
  "m_z0sintheta_max"              : ZHF_config.MUBaseSel_z0sintheta_max,
  "m_d0sig_max"                   : ZHF_config.MUBaseSel_d0sig_max,
  "m_IsoWPList"                   : ZHF_config.MUBaseSel_IsoWPList,
  "m_muonQualityStr"              : ZHF_config.MUBaseSel_Quality,
  "m_inContainerName"             : ZHF_config.MUBaseSel_inContainer,
  "m_outContainerName"            : ZHF_config.MUBaseSel_outContainer,
  "m_createSelectedContainer"     : True,
  "m_decorateSelectedObjects"     : True,
  } )

c.algorithm("METConstructor", {
  "m_name"                           : "met",
  "m_msgLevel"                       : ZHF_config.msgLevel,
  "m_referenceMETContainer"          : ZHF_config.referenceMETContainer+jetAlgo,
  "m_mapName"                        : ZHF_config.MET_mapName+jetAlgo,
  "m_coreName"                       : ZHF_config.MET_coreName+jetAlgo,
  "m_outputContainer"                : ZHF_config.MET_outputContainer,
  "m_inputMuons"                     : ZHF_config.MET_inputMuons,
  "m_inputElectrons"                 : ZHF_config.MET_inputElectrons,
  "m_inputJets"                      : ZHF_config.MET_inputJets,
  "m_doJVTCut"                       : ZHF_config.MET_doJVTCut,
  "m_dofJVTCut"	                     : ZHF_config.MET_dofJVTCut,
  "m_calculateSignificance"          : ZHF_config.MET_calculateSignificance,
  })

c.algorithm("OverlapRemover", {
  "m_name"                           : "OverlapRemover",
  "m_msgLevel"                       : ZHF_config.msgLevel,
  "m_inContainerName_Jets"           : ZHF_config.JETBaseSel_outContainer,
  "m_outContainerName_Jets"          : ZHF_config.OR_outJETContainer,
  "m_inContainerName_Electrons"      : ZHF_config.ELBaseSel_outContainer,
  "m_outContainerName_Electrons"     : ZHF_config.OR_outELContainer,
  "m_createSelectedContainers"       : True,
  } )

c.algorithm("ElectronSelector", {
  "m_name"                           : "ElectronSignalSelector",
  "m_msgLevel"                       : ZHF_config.msgLevel,
  "m_pT_min"                         : ZHF_config.ELSignalSel_pT_min,
  "m_eta_max"                        : ZHF_config.ELSignalSel_eta_max,
  "m_z0sintheta_max"                 : ZHF_config.ELSignalSel_z0sintheta_max,
  "m_d0sig_max"                      : ZHF_config.ELSignalSel_d0sig_max,
  "m_IsoWPList"                      : ZHF_config.ELSignalSel_IsoWPList,
  "m_inContainerName"                : ZHF_config.ELSignalSel_inContainer,
  "m_outContainerName"               : ZHF_config.ELSignalSel_outContainer,
  "m_readIDFlagsFromDerivation"      : ZHF_config.ELSignalSel_readIDFlags,
  "m_doLHPIDcut"                     : ZHF_config.ELSignalSel_doLHPIDcut,
  "m_LHOperatingPoint"               : ZHF_config.ELSignalSel_LHWP,
  "m_createSelectedContainer"        : True,
  "m_decorateSelectedObjects"        : True,
  } )

c.algorithm("JetSelector", {
  "m_name"                           : "JetSignalSelector",
  "m_msgLevel"                       : ZHF_config.msgLevel,
  "m_pT_min"                         : ZHF_config.JETSignalSel_pT_min,
  "m_rapidity_min"                   : ZHF_config.JETSignalSel_rap_min,
  "m_rapidity_max"                   : ZHF_config.JETSignalSel_rap_max,
  "m_inContainerName"                : ZHF_config.JETSignalSel_inContainer,
  "m_outContainerName"               : ZHF_config.JETSignalSel_outContainer,
  "m_cleanJets"                      : ZHF_config.JETSignalSel_cleanJets,
  "m_cleanEvent"                     : ZHF_config.JETSignalSel_cleanEvent,
  "m_doJVT"                          : ZHF_config.JETSignalSel_doJVT,
  "m_noJVTVeto"                      : ZHF_config.JETSignalSel_noJVTVeto,
  "m_WorkingPointJVT"                : ZHF_config.JETSignalSel_JVTwp,
  "m_SFFileJVT"                      : ZHF_config.JETSignalSel_SFFileJVT+jetType+"Jets.root",
  "m_dofJVT"                         : ZHF_config.JETSignalSel_dofJVT,
  "m_createSelectedContainer"        : ZHF_config.JETSignalSel_createContainer,
  "m_decorateSelectedObjects"        : ZHF_config.JETSignalSel_decorateObjects,
  } )

c.algorithm("BJetEfficiencyCorrector", { # continuous b-tagging
  "m_name"                           : "BJetEffCorrector_continuous",
  "m_msgLevel"	                     : ZHF_config.msgLevel,
  "m_inContainerName"                : ZHF_config.bJET_inContainer,
  "m_jetAuthor"                      : jetAlgo+"Jets",
  "m_decor"                          : ZHF_config.bJET_decor,
  "m_corrFileName"                   : ZHF_config.bJET_corrFileName,
  "m_taggerName"                     : ZHF_config.bJET_TaggerName,
  "m_operatingPt"                    : ZHF_config.bJET_OperatingPoint,
  "m_coneFlavourLabel"               : ZHF_config.bJET_coneFlavourLabel,
  "m_useDevelopmentFile"             : ZHF_config.bJET_useDevelopmentFile,
  } )

c.algorithm("BJetEfficiencyCorrector", { # calibrated working point
  "m_name"                           : "BJetEffCorrector_calibratedWP",
  "m_msgLevel"                       : ZHF_config.msgLevel,
  "m_inContainerName"                : ZHF_config.bJET_inContainer,
  "m_jetAuthor"                      : jetAlgo+"Jets",
  "m_decor"                          : ZHF_config.bJET_decor,
  "m_corrFileName"                   : ZHF_config.bJET_corrFileName,
  "m_taggerName"                     : ZHF_config.bJET_TaggerName,
  "m_operatingPt"                    : ZHF_config.bJET_FixedOperatingPoint,
  "m_coneFlavourLabel"               : ZHF_config.bJET_coneFlavourLabel,
  "m_useDevelopmentFile"             : ZHF_config.bJET_useDevelopmentFile,
  } )

c.algorithm("TruthSelector", {
  "m_name"                           : "TruthElectronSelector",
  "m_msgLevel"                       : ZHF_config.msgLevel,
  "m_type"                           : ZHF_config.TruthELSel_type,
  "m_origin"                         : ZHF_config.TruthELSel_origin,
  "m_pT_dressed_min"                 : ZHF_config.TruthELSel_pt_dressed_min,
  "m_eta_dressed_min"                : ZHF_config.TruthELSel_eta_dressed_min,
  "m_eta_dressed_max"                : ZHF_config.TruthELSel_eta_dressed_max,
  "m_inContainerName"                : "TruthParticles",
  "m_outContainerName"               : "TruthElectrons_Selected",
  "m_createSelectedContainer"        : True,
  "m_decorateSelectedObjects"        : False,
  } )

c.algorithm("JetSelector", {
  "m_name"                           : "TruthJetSelector",
  "m_msgLevel"                       : ZHF_config.msgLevel,
  "m_pT_min"                         : ZHF_config.TruthJETSel_pT_min,
  "m_inContainerName"                : ZHF_config.TruthJETSel_inContainer,
  "m_outContainerName"               : ZHF_config.TruthJETSel_outContainer,
  "m_createSelectedContainer"        : True,
  "m_decorateSelectedObjects"        : False,
  } )

c.algorithm("ZHFSelector", {
  "m_name"                           : "ZHFSelector",
  "m_msgLevel"                       : ZHF_config.msgLevel,
  "m_inContainerName_TruthElectrons" : "TruthElectrons_Selected",
  "m_inContainerName_TruthJets"      : ZHF_config.ZHFSel_inContainer_TruthJETS,
  "m_TruthDR_min"                    : ZHF_config.ZHFSel_TruthDR_min,
  "m_inContainerName_RecoElectrons"  : ZHF_config.OR_outELContainer,
  "m_RecoZmass_min"                  : ZHF_config.ZHFSel_RecoZmass_min,
  "m_RecoZmass_max"                  : ZHF_config.ZHFSel_RecoZmass_max,
  } )

## RecoEff and trigger SF are empty!

c.algorithm("ZHFTreeAlgo", {
  "m_name"                        : "ZHFTreeAlgo",
  "m_msgLevel"                    : ZHF_config.msgLevel,

  #### Jet collection ####
  "m_jetContainerName"            : ZHF_config.FinalJetContainerName,
  "m_truthJetContainerName"       : ZHF_config.truthJetContainerName,

  #### Eelectron collection ####
  "m_elContainerName"             : ZHF_config.FinalElectronContainerName,
  "m_truthParticlesContainerName" : "TruthElectrons_Selected",

  #### MET ####
  "m_METContainerName"            : ZHF_config.FinalMETContainerName,
  "m_truthMETContainerName"       : ZHF_config.truthMETContainerName,

  ### TTree branch options ###
  # eventDetailStr options in xAH's EventInfo.cxx (pileup, shapeEM, caloClus, etc.)
  "m_evtDetailStr"                : ZHF_config.evtDetailStrWeightsSys,
  # jetDetailStr options in xAH's JetContainer.cxx
  "m_jetDetailStr"                : ZHF_config.jetDetailStr,
  # muDetailStr options in xAH's ElectronContainer.cxx
  "m_elDetailStr"                 : ZHF_config.elDetailStr,
  # METDetailStr options in xAH's MetContainer.cxx
  "m_METDetailStr"                : ZHF_config.metDetailStr,
  # truthParticleDetailStr options in xAH's TreeAlgo.cxx
  "m_truthParticlesDetailStr"     : ZHF_config.truthMuonDetailStr,
  # truthJetDetailStr option in xAH's TreeAlgo.cxx
  "m_truthJetDetailStr"           : ZHF_config.truthJetDetailStr,
  # trigDetailStr options in xAH's HelpTreeBase.cxx (basic, passTriggers, etc.). Relevant for data 
  "m_trigDetailStr"               : ZHF_config.trigDetailStrMC,
  } )
