##################################################
#
# Purpose: Create every possible config
# Author:  Jona Bossio (jbossios@cern.ch)
# Date:    26 March 2020
#
##################################################

Samples = [
  '2015Data',
  '2016Data',
  '2017Data',
  '2018Data',
  'MC16a',
  'MC16d',
  'MC16e',
]

InputTypes = [
  'STDM3',   # DAOD_STDM3
  'STDM4',   # DAOD_STDM4
  'PHYS',    # DAOD_PHYS
  'AOD',
  'HIGG5D2', # DAOD_HIGG5D2
]

JetCollections = ['AntiKt4EMTopo','AntiKt4EMPFlow']

ConfigSetups = {
  'NominalOnlyBkgsAndData' : {'sysType' : 'NominalOnly', 'truth' : False, 'trigSel' : True},
  'NominalOnlySignal'      : {'sysType' : 'NominalOnly', 'truth' : True,  'trigSel' : False},
  'Systematics'            : {'sysType' : 'Full',        'truth' : False, 'trigSel' : True},
}

# Temporary fix due to missing links to origin containers from the btagging containers (needed for MET)
Fix4MissingLinks = True

####################
## Generate configs
####################

# Loop over data/MC samples and analyses
for sample in Samples:
  # Loop over derivation types
  for inputType in InputTypes:
    # Loop over jet collections
    for jetcoll in JetCollections:
      # Loop over different treatments of systematics
      for cftype,cfdict in ConfigSetups.items():
        if 'Data' in sample and cftype != 'NominalOnlyBkgsAndData': continue # skip creating of configs not meant for data
        # Name of config
        deriv = '_'+inputType if inputType != 'STDM3' else ''
        extra = '_'+cftype
        if cftype == 'NominalOnlyBkgsAndData':
          if 'Data' in sample:
            extra = ''
          else:
            extra = '_NominalOnlyBkgs'
        ConfigName    = 'config_Tree_{}_ZHFJETS_{}{}{}.py'.format(sample,jetcoll,extra,deriv)
        Config = open(ConfigName,'w')
        ######################
        # Header
        ######################
        Config.write('import ROOT\n')
        Config.write('from MakeTTrees_ZHF import CommonDefs as ZHF_config\n')
        Config.write('from xAODAnaHelpers import Config     as xAH_config\n')
        Config.write('\n')
        Config.write('c = xAH_config()\n')
        Config.write('\n')
        Config.write('jetAlgo = "'+jetcoll+'"\n')
        Config.write('jetType = jetAlgo.replace("AntiKt4","")\n')
        Config.write('\n')
        #########################
        # TruthJetReconstruction
        #########################
        if inputType == 'HIGG5D2':
          Config.write('c.algorithm("TruthJetReconstruction", {\n')
          Config.write('  "m_name"                        : "truthJetReconstrucionAlg",\n')
          Config.write('  "m_TruthJetCollection"          : "AntiKt4TruthWZJets",\n')
          Config.write('  } )\n')
          Config.write('\n')
        ######################
        # BasicEventSelection
        ######################
        Config.write('c.algorithm("BasicEventSelection", {\n')
        Config.write('  "m_name"                        : "BasicSelection",\n')
        Config.write('  "m_msgLevel"                    : ZHF_config.msgLevel,\n')
        if 'Data' in sample:
          Config.write('  "m_applyGRLCut"                 : True,\n')
          Config.write('  "m_GRLxml"                      : ZHF_config.GRL_'+sample[0:4]+',\n')
        else:
          Config.write('  "m_applyGRLCut"                 : False,\n')
        Config.write('  "m_derivationName"              : "'+inputType+'Kernel",\n')
        Config.write('  "m_useMetaData"                 : True,\n')
        Config.write('  "m_storePassHLT"                : True,\n')
        Config.write('  "m_storeTrigDecisions"          : True,\n')
        if cfdict['trigSel']: Config.write('  "m_applyTriggerCut"             : True,\n')
        else:                 Config.write('  "m_applyTriggerCut"             : False,\n')
        if 'Data' in sample:
          if cfdict['trigSel']: Config.write('  "m_triggerSelection"            : " | ".join(ZHF_config.ELTriggers_'+sample[0:4]+'+ZHF_config.MUTriggers_'+sample[0:4]+'),\n')
          else:                 Config.write('  "m_extraTriggerSelection"       : ",".join(ZHF_config.ELTriggers_'+sample[0:4]+'+ZHF_config.MUTriggers_'+sample[0:4]+'),\n')
          Config.write('  "m_checkDuplicatesData"         : True,\n')
          Config.write('  "m_applyJetCleaningEventFlag"   : ZHF_config.applyJetCleaningEventFlagData,\n')
        else:
          if 'MC16a' in sample:
            if cfdict['trigSel']: Config.write('  "m_triggerSelection"            : " | ".join(set(ZHF_config.ELTriggers_2015+ZHF_config.ELTriggers_2016+ZHF_config.MUTriggers_2015+ZHF_config.MUTriggers_2016)),\n')
            else:                 Config.write('  "m_extraTriggerSelection"       : ",".join(set(ZHF_config.ELTriggers_2015+ZHF_config.ELTriggers_2016+ZHF_config.MUTriggers_2015+ZHF_config.MUTriggers_2016)),\n')
          elif 'MC16d' in sample:
            if cfdict['trigSel']: Config.write('  "m_triggerSelection"            : " | ".join(ZHF_config.ELTriggers_2017+ZHF_config.MUTriggers_2017),\n')
            else:                 Config.write('  "m_extraTriggerSelection"       : ",".join(ZHF_config.ELTriggers_2017+ZHF_config.MUTriggers_2017),\n')
          elif 'MC16e' in sample:
            if cfdict['trigSel']: Config.write('  "m_triggerSelection"            : " | ".join(ZHF_config.ELTriggers_2018+ZHF_config.MUTriggers_2018),\n')
            else:                 Config.write('  "m_extraTriggerSelection"       : ",".join(ZHF_config.ELTriggers_2018+ZHF_config.MUTriggers_2018),\n')
          Config.write('  "m_checkDuplicatesMC"           : True,\n')
          if cfdict['truth']: # no jet cleaning selection when saving truth info
            Config.write('  "m_applyJetCleaningEventFlag"   : False,\n')
          else: # apply jet cleaning when not saving truth info
            Config.write('  "m_applyJetCleaningEventFlag"   : True,\n')
        Config.write('  "m_applyEventCleaningCut"       : ZHF_config.applyEventCleaningCut,\n')
        Config.write('  "m_PVNTrack"                    : ZHF_config.minPVnTrack,\n')
        Config.write('  "m_applyPrimaryVertexCut"       : ZHF_config.applyPrimaryVertexCut,\n')
        Config.write('  "m_vertexContainerName"         : ZHF_config.vertexContainerName,\n')
        if inputType != 'PHYS':
          Config.write('  "m_doPUreweighting"             : True,\n')
          if 'Data' in sample:
            if '2015' in sample or '2016' in sample:
              Config.write('  "m_lumiCalcFileNames"           : ",".join(ZHF_config.iLumis["MC16a"]),\n')
              Config.write('  "m_PRWFileNames"                : ",".join(ZHF_config.PRWs["MC16a"]),\n')
            elif '2017' in sample:
              Config.write('  "m_lumiCalcFileNames"           : ",".join(ZHF_config.iLumis["MC16d"]),\n')
              Config.write('  "m_PRWFileNames"                : ",".join(ZHF_config.PRWs["MC16d"]),\n')
            elif '2018' in sample:
              Config.write('  "m_lumiCalcFileNames"           : ",".join(ZHF_config.iLumis["MC16e"]),\n')
              Config.write('  "m_PRWFileNames"                : ",".join(ZHF_config.PRWs["MC16e"]),\n')
          else: # MC
            Config.write('  "m_lumiCalcFileNames"           : ",".join(ZHF_config.iLumis["'+sample[0:5]+'"]),\n')
            Config.write('  "m_PRWFileNames"                : ",".join(ZHF_config.PRWs["'+sample[0:5]+'"]),\n')
            if cfdict['sysType'] == 'NominalOnly':
              Config.write('  "m_doPUreweightingSys"          : True,\n')
        else: # PHYS
          Config.write('  "m_doPUreweighting"             : True,\n')
          Config.write('  "m_lumiCalcFileNames"           : ",".join(ZHF_config.iLumis["'+sample[0:5]+'"]),\n')
          Config.write('  "m_PRWFileNames"                : "MakeTTrees_ZHF/PRW_Zmumu_Ref1_Ref2_PHYS.root",\n')
        if 'Data' in sample:
          if '2015' in sample:
            Config.write('  "m_applyIsBadBatmanFlag"        : ZHF_config.applyIsBadBatmanFlag15,\n')
          elif '2016' in sample:
            Config.write('  "m_applyIsBadBatmanFlag"        : ZHF_config.applyIsBadBatmanFlag16,\n')
          elif '2017' in sample:
            Config.write('  "m_applyIsBadBatmanFlag"        : ZHF_config.applyIsBadBatmanFlag17,\n')
          elif '2018' in sample:
            Config.write('  "m_applyIsBadBatmanFlag"        : ZHF_config.applyIsBadBatmanFlag18,\n')
        Config.write('  } )\n')
        Config.write('\n')
        ######################
        # MuonCalibrator
        ######################
        Config.write('c.algorithm("MuonCalibrator", {\n')
        Config.write('  "m_name"                     : "MuonCalibrator",\n')
        Config.write('  "m_msgLevel"                 : ZHF_config.msgLevel,\n')
        Config.write('  "m_sort"                     : True,\n')
        Config.write('  "m_inContainerName"          : ZHF_config.MUCalib_inContainer,\n')
        Config.write('  "m_outContainerName"         : ZHF_config.MUCalib_outContainer,\n')
        Config.write('  "m_calibrationMode"          : ZHF_config.MUCalib_calibrationMode,\n')
        if 'Data' in sample: Config.write('  "m_forceDataCalib"           : ZHF_config.MUCalib_forceDataCalib,\n')
        Config.write('  "m_outputAlgoSystNames"      : "Muons_Calibrated_Algo",\n')
        if cfdict['sysType'] == 'Full':
          Config.write('  "m_systName"                 : "All",\n')
          Config.write('  "m_systVal"                  : 1.0,\n')
        Config.write('  } )\n')
        ######################
        # MuonSelector
        ######################
        Config.write('c.algorithm("MuonSelector", {\n')
        Config.write('  "m_name"                     : "MuonSelector",\n')
        Config.write('  "m_msgLevel"                 : ZHF_config.msgLevel,\n')
        Config.write('  "m_removeEventBadMuon"       : ZHF_config.MUBaseSel_rmEventBadMuon,\n')
        Config.write('  "m_pT_min"                   : ZHF_config.MUBaseSel_pT_min,\n')
        Config.write('  "m_eta_max"                  : ZHF_config.MUBaseSel_eta_max,\n')
        Config.write('  "m_z0sintheta_max"           : ZHF_config.MUBaseSel_z0sintheta_max,\n')
        Config.write('  "m_d0sig_max"                : ZHF_config.MUBaseSel_d0sig_max,\n')
        Config.write('  "m_IsoWPList"                : ZHF_config.MUBaseSel_IsoWPList,\n')
        Config.write('  "m_MinIsoWPCut"              : ZHF_config.MUBaseSel_MinIsoWPCut,\n')
        Config.write('  "m_muonQualityStr"           : ZHF_config.MUBaseSel_Quality,\n')
        Config.write('  "m_inContainerName"          : ZHF_config.MUBaseSel_inContainer,\n')
        Config.write('  "m_outContainerName"         : ZHF_config.MUBaseSel_outContainer,\n')
        if 'MC16a' in sample:
          Config.write('  "m_singleMuTrigChains"       : ",".join(set(ZHF_config.MUTriggers_2015+ZHF_config.MUTriggers_2016)),\n')
        elif '2015' in sample:
          Config.write('  "m_singleMuTrigChains"       : ",".join(ZHF_config.MUTriggers_2015),\n')
        elif '2016' in sample:
          Config.write('  "m_singleMuTrigChains"       : ",".join(ZHF_config.MUTriggers_2016),\n')
        elif 'MC16d' in sample or '2017' in sample:
          Config.write('  "m_singleMuTrigChains"       : ",".join(ZHF_config.MUTriggers_2017),\n')
        elif 'MC16e' in sample or '2018' in sample:
          Config.write('  "m_singleMuTrigChains"       : ",".join(ZHF_config.MUTriggers_2018),\n')
        Config.write('  "m_inputAlgoSystNames"       : "Muons_Calibrated_Algo",\n')
        Config.write('  "m_outputAlgoSystNames"      : "Muons_Selected_Algo",\n')
        Config.write('  "m_createSelectedContainer"  : True,\n')
        Config.write('  "m_decorateSelectedObjects"  : True,\n')
        Config.write('  } )\n')
        Config.write('\n')
        ######################
        # ElectronCalibrator
        ######################
        Config.write('c.algorithm("ElectronCalibrator", {\n')
        Config.write('  "m_name"                           : "ElectronCalibrator",\n')
        Config.write('  "m_sort"                           : True,\n')
        Config.write('  "m_esModel"                        : ZHF_config.ELCalib_esModel,\n')
        Config.write('  "m_decorrelationModel"             : ZHF_config.ELCalib_decorrelationModel,\n')
        Config.write('  "m_inContainerName"                : ZHF_config.ELCalib_inContainer,\n')
        Config.write('  "m_outContainerName"               : ZHF_config.ELCalib_outContainer,\n')
        Config.write('  "m_outputAlgoSystNames"            : "Electrons_Calibrated_Algo",\n')
        if cfdict['sysType'] == 'Full':
          Config.write('  "m_systName"                       : "All",\n')
          Config.write('  "m_systVal"                        : 1.0,\n')
        Config.write('  } )\n')
        ########################
        # ElectronSelector
        ########################
        Config.write('c.algorithm("ElectronSelector", {\n')
        Config.write('  "m_name"                           : "ElectronSelector",\n')
        Config.write('  "m_msgLevel"                       : ZHF_config.msgLevel,\n')
        Config.write('  "m_pT_min"                         : ZHF_config.ELBaseSel_pT_min,\n')
        Config.write('  "m_eta_max"                        : ZHF_config.ELBaseSel_eta_max,\n')
        Config.write('  "m_z0sintheta_max"                 : ZHF_config.ELBaseSel_z0sintheta_max,\n')
        Config.write('  "m_d0sig_max"                      : ZHF_config.ELBaseSel_d0sig_max,\n')
        Config.write('  "m_IsoWPList"                      : ZHF_config.ELBaseSel_IsoWPList,\n')
        Config.write('  "m_MinIsoWPCut"                    : ZHF_config.ELBaseSel_MinIsoWPCut,\n')
        Config.write('  "m_inContainerName"                : ZHF_config.ELBaseSel_inContainer,\n')
        Config.write('  "m_outContainerName"               : ZHF_config.ELBaseSel_outContainer,\n')
        Config.write('  "m_readIDFlagsFromDerivation"      : ZHF_config.ELBaseSel_readIDFlags,\n')
        Config.write('  "m_doLHPIDcut"                     : ZHF_config.ELBaseSel_doLHPIDcut,\n')
        Config.write('  "m_LHOperatingPoint"               : ZHF_config.ELBaseSel_LHWP,\n')
        if jetcoll == 'AntiKt4EMPFlow': # needed for MET
          Config.write('  "m_applyCrackVetoCleaning"         : ZHF_config.ELBaseSel_applyCrackVetoFix,\n')
        Config.write('  "m_vetoCrack"                      : False,\n') # it can be applied at the reader
        if 'MC16a' in sample:
          Config.write('  "m_singleElTrigChains"             : ",".join(set(ZHF_config.ELTriggers_2015+ZHF_config.ELTriggers_2016)),\n')
        elif '2015' in sample:
          Config.write('  "m_singleElTrigChains"             : ",".join(ZHF_config.ELTriggers_2015),\n')
        elif '2016' in sample:
          Config.write('  "m_singleElTrigChains"             : ",".join(ZHF_config.ELTriggers_2016),\n')
        elif '2017' in sample or 'MC16d' in sample:
          Config.write('  "m_singleElTrigChains"             : ",".join(ZHF_config.ELTriggers_2017),\n')
        elif '2018' in sample or 'MC16e' in sample:
          Config.write('  "m_singleElTrigChains"             : ",".join(ZHF_config.ELTriggers_2018),\n')
        Config.write('  "m_inputAlgoSystNames"             : "Electrons_Calibrated_Algo",\n')
        Config.write('  "m_outputAlgoSystNames"            : "Electrons_Selected_Algo",\n')
        Config.write('  "m_createSelectedContainer"        : True,\n')
        Config.write('  "m_decorateSelectedObjects"        : True,\n')
        Config.write('  } )\n')
        Config.write('\n')
        ########################
        # JetCalibrator
        ########################
        Config.write('c.algorithm("JetCalibrator", {\n')
        Config.write('  "m_name"                        : "JetCalibrator",\n')
        if jetcoll == 'AntiKt4EMTopo': #EMTopo
          if 'MC' in sample:
            Config.write('  "m_calibConfigFullSim"          : ZHF_config.EMcalibConfigFullSim,\n')
          else: # data
            Config.write('  "m_calibConfigData"             : ZHF_config.EMcalibConfigData,\n')
        else: #PFlow 
          if 'MC' in sample:
            Config.write('  "m_calibConfigFullSim"          : ZHF_config.PFcalibConfigFullSim,\n')
          else: # data
            Config.write('  "m_calibConfigData"             : ZHF_config.PFcalibConfigData,\n')
        if 'Data' in sample:
          Config.write('  "m_calibSequence"               : ZHF_config.calibSequenceData,\n')
        else:
          Config.write('  "m_calibSequence"               : ZHF_config.calibSequenceMC,\n')
        Config.write('  "m_sort"                        : True,\n')
        Config.write('  "m_doCleaning"                  : ZHF_config.doCleaning,\n')
        Config.write('  "m_jetCleanCutLevel"            : ZHF_config.jetCleanCutLevel,\n')
        Config.write('  "m_saveAllCleanDecisions"       : False,\n')
        Config.write('  "m_inContainerName"             : jetAlgo+"Jets"+ZHF_config.bJET_timeStamp,\n')
        Config.write('  "m_outContainerName"            : ZHF_config.JETCalib_outContainer,\n')
        Config.write('  "m_jetAlgo"                     : jetAlgo,\n')
        Config.write('  "m_redoJVT"                     : ZHF_config.redoJVT,\n')
        Config.write('  "m_calculatefJVT"               : ZHF_config.calculatefJVT,\n')
        Config.write('  "m_fJVTWorkingPoint"            : ZHF_config.WorkingPointfJVT,\n')
        Config.write('  "m_addGhostMuonsToJets"         : ZHF_config.addGhostMuonsToJets,\n')
        Config.write('  "m_outputAlgo"                  : "Jets_Calibrated_Algo",\n')
        if cfdict['sysType'] == 'Full':
          Config.write('  "m_uncertConfig"                : ZHF_config.uncertConfig,\n')
          Config.write('  "m_systName"                    : "All",\n')
          Config.write('  "m_systVal"                     : 1.0,\n')
        Config.write('  } )\n')
        Config.write('\n')
        ########################
        # Base JetSelector
        ########################
        Config.write('JVTwps  = ZHF_config.JETBaseSel_JVTwp[jetAlgo]\n')
        Config.write('nJVTwps = len(JVTwps)\n')
        Config.write('for iwp in range(0,nJVTwps):\n')
        Config.write('  if iwp == 0:\n')
        Config.write('    inContainer = ZHF_config.JETBaseSel_inContainer\n')
        Config.write('    inAlg       = "Jets_Calibrated_Algo"\n')
        Config.write('  else:\n')
        Config.write('    inContainer = "JETBaseSel_"+JVTwps[iwp-1]\n')
        Config.write('    inAlg       = "Jets_Selected_Algo_"+JVTwps[iwp-1]\n')
        Config.write('  if iwp == nJVTwps-1:\n')
        Config.write('    outContainer = ZHF_config.JETBaseSel_outContainer\n')
        Config.write('    outAlg       = "Jets_Selected_Algo"\n')
        Config.write('  else:\n')
        Config.write('    outContainer = "JETBaseSel_"+JVTwps[iwp]\n')
        Config.write('    outAlg       = "Jets_Selected_Algo_"+JVTwps[iwp]\n')
        Config.write('  c.algorithm("JetSelector", {\n')
        Config.write('    "m_name"                        : "JetBaseSelector_"+JVTwps[iwp],\n')
        Config.write('    "m_msgLevel"                    : ZHF_config.msgLevel,\n')
        Config.write('    "m_pT_min"                      : ZHF_config.JETBaseSel_pT_min,\n')
        Config.write('    "m_eta_min"                     : ZHF_config.JETBaseSel_eta_min,\n')
        Config.write('    "m_eta_max"                     : ZHF_config.JETBaseSel_eta_max,\n')
        Config.write('    "m_inContainerName"             : inContainer,\n')
        Config.write('    "m_outContainerName"            : outContainer,\n')
        Config.write('    "m_cleanJets"                   : ZHF_config.JETBaseSel_cleanJets,\n')
        Config.write('    "m_cleanEvent"                  : ZHF_config.JETBaseSel_cleanEvent,\n')
        Config.write('    "m_createSelectedContainer"     : True,\n')
        Config.write('    "m_decorateSelectedObjects"     : True,\n')
        Config.write('    "m_doJVT"                       : ZHF_config.JETBaseSel_doJVT,\n')
        Config.write('    "m_noJVTVeto"                   : ZHF_config.JETBaseSel_noJVTVeto,\n')
        Config.write('    "m_WorkingPointJVT"             : JVTwps[iwp],\n')
        Config.write('    "m_SFFileJVT"                   : ZHF_config.JETBaseSel_SFFileJVT+jetType+"Jets.root",\n')
        Config.write('    "m_dofJVT"                      : ZHF_config.JETBaseSel_dofJVT,\n')
        Config.write('    "m_dofJVTVeto"                  : ZHF_config.JETBaseSel_dofJVTVeto,\n')
        Config.write('    "m_WorkingPointfJVT"            : ZHF_config.WorkingPointfJVT,\n')
        Config.write('    "m_inputAlgo"                   : inAlg,\n')
        Config.write('    "m_outputAlgo"                  : outAlg,\n')
        if 'MC' in sample and cfdict['sysType']!='Full':
          Config.write('    "m_systValJVT"                  : 1.0,\n')
          Config.write('    "m_systNameJVT"                 : "All",\n')
        if 'MC' in sample and inputType == 'PHYS':
          Config.write('    "m_truthJetContainer"           : "AntiKt4TruthDressedWZJets",\n')
        if inputType == 'AOD':
          Config.write('    "m_haveTruthJets"               : False,\n')
        Config.write('  } )\n')
        Config.write('\n')
        ##########################
        # BJetEfficiencyCorrector
        ##########################
        Config.write('counter = 0\n')
        Config.write('for tagger in ZHF_config.bJET_TaggerNames:\n')
        if cfdict['sysType'] == 'Full':
          Config.write('  if tagger != ZHF_config.bJET_TaggerNames[0]: continue # skip non-nominal tagger for systematic TTrees\n')
        # continuous b-tagging
        Config.write('  c.algorithm("BJetEfficiencyCorrector", {\n')
        Config.write('    "m_name"                        : "BJetEffCorrector_continuous_"+tagger,\n')
        Config.write('    "m_msgLevel"                    : ZHF_config.msgLevel,\n')
        Config.write('    "m_inContainerName"             : ZHF_config.bJET_inContainer,\n')
        Config.write('    "m_jetAuthor"                   : jetAlgo+"Jets"+ZHF_config.bJET_timeStamp,\n')
        Config.write('    "m_minPt"                       : ZHF_config.bJET_MinPt,\n')
        Config.write('    "m_decor"                       : ZHF_config.bJET_decor,\n')
        Config.write('    "m_corrFileName"                : ZHF_config.bJET_corrFileName,\n')
        Config.write('    "m_taggerName"                  : tagger,\n')
        Config.write('    "m_operatingPt"                 : ZHF_config.bJET_OperatingPoint,\n')
        Config.write('    "m_coneFlavourLabel"            : ZHF_config.bJET_coneFlavourLabel,\n')
        Config.write('    "m_useDevelopmentFile"          : ZHF_config.bJET_useDevelopmentFile,\n')
        Config.write('    "m_inputAlgo"                   : "Jets_Selected_Algo",\n')
        Config.write('    "m_EfficiencyCalibration"       : ZHF_config.bJET_EfficiencyCalibration,\n')
        Config.write('    "m_setMapIndex"                 : True,\n')
        if 'MC' in sample and cfdict['sysType']!='Full': # b-tagging systematics only in nominal TTrees
          Config.write('      "m_systName"                    : "All",\n')
          Config.write('      "m_writeSystToMetadata"         : True,\n')
        Config.write('    } )\n')
        # calibrated working point
        Config.write('  for wp in ZHF_config.bJET_FixedOperatingPoints:\n')
        if cfdict['sysType'] == 'Full': Config.write('    if wp != ZHF_config.bJET_FixedOperatingPoints[0]: continue # skip non-nominal WP for systematic TTrees\n')
        Config.write('    c.algorithm("BJetEfficiencyCorrector", {\n')
        Config.write('      "m_name"                        : "BJetEffCorrector_calibratedWP_"+tagger+"_"+wp,\n')
        Config.write('      "m_msgLevel"                    : ZHF_config.msgLevel,\n')
        Config.write('      "m_inContainerName"             : ZHF_config.bJET_inContainer,\n')
        Config.write('      "m_jetAuthor"                   : jetAlgo+"Jets"+ZHF_config.bJET_timeStamp,\n')
        Config.write('      "m_minPt"                       : ZHF_config.bJET_MinPt,\n')
        Config.write('      "m_decor"                       : ZHF_config.bJET_decor,\n')
        Config.write('      "m_corrFileName"                : ZHF_config.bJET_corrFileName,\n')
        Config.write('      "m_taggerName"                  : tagger,\n')
        Config.write('      "m_operatingPt"                 : wp,\n')
        Config.write('      "m_coneFlavourLabel"            : ZHF_config.bJET_coneFlavourLabel,\n')
        Config.write('      "m_useDevelopmentFile"          : ZHF_config.bJET_useDevelopmentFile,\n')
        Config.write('      "m_inputAlgo"                   : "Jets_Selected_Algo",\n')
        Config.write('      "m_EfficiencyCalibration"       : ZHF_config.bJET_EfficiencyCalibration,\n')
        Config.write('      "m_setMapIndex"                 : True,\n')
        Config.write('      } )\n')
        Config.write('    counter += 1\n')
        Config.write('\n')
        ##########################
        # OverlapRemover
        ##########################
        Config.write('c.algorithm("OverlapRemover", {\n')
        Config.write('  "m_name"                        : "OverlapRemover",\n')
        Config.write('  "m_msgLevel"                    : ZHF_config.msgLevel,\n')
        Config.write('  "m_inContainerName_Jets"        : ZHF_config.OR_inJETContainer,\n')
        Config.write('  "m_outContainerName_Jets"       : ZHF_config.OR_outJETContainer,\n')
        Config.write('  "m_inContainerName_Muons"       : ZHF_config.OR_inMUContainer,\n')
        Config.write('  "m_outContainerName_Muons"      : ZHF_config.OR_outMUContainer,\n')
        Config.write('  "m_inContainerName_Electrons"   : ZHF_config.OR_inELContainer,\n')
        Config.write('  "m_outContainerName_Electrons"  : ZHF_config.OR_outELContainer,\n')
        Config.write('  "m_createSelectedContainers"    : True,\n')
        Config.write('  "m_doEleEleOR"                  : True,\n')
        if jetcoll == 'AntiKt4EMPFlow':
          Config.write('  "m_doMuPFJetOR"                 : True,\n') # To remove muons reconstructed as p-flow jets
        Config.write('  "m_inputAlgoJets"               : "Jets_Selected_Algo",\n')
        Config.write('  "m_inputAlgoMuons"              : "Muons_Selected_Algo",\n')
        Config.write('  "m_inputAlgoElectrons"          : "Electrons_Selected_Algo",\n')
        Config.write('  "m_outputAlgoSystNames"         : "OverlapRemover_Algo",\n')
        Config.write('  "m_useBoostedLeptons"           : ZHF_config.OR_useSlidingDR,\n')
        Config.write('  "m_applyRelPt"                  : ZHF_config.OR_applyRelPt,\n')
        Config.write('  } )\n')
        Config.write('\n')
        ##########################
        # TruthMuonSelector
        ##########################
        if 'MC' in sample and inputType != 'AOD' and cfdict['truth']:
          Config.write('c.algorithm("TruthSelector", {\n')
          Config.write('  "m_name"                        : "TruthMuonSelector",\n')
          Config.write('  "m_msgLevel"                    : ZHF_config.msgLevel,\n')
          Config.write('  "m_typeOptions"                 : ZHF_config.TruthMUSel_types,\n')
          Config.write('  "m_originOptions"               : ZHF_config.TruthMUSel_origins,\n')
          Config.write('  "m_pT_dressed_min"              : ZHF_config.TruthMUSel_pt_dressed_min,\n')
          Config.write('  "m_eta_dressed_min"             : ZHF_config.TruthMUSel_eta_dressed_min,\n')
          Config.write('  "m_eta_dressed_max"             : ZHF_config.TruthMUSel_eta_dressed_max,\n')
          if inputType != 'PHYS':
            Config.write('  "m_inContainerName"             : ZHF_config.TruthMUSel_inContainer,\n')
            Config.write('  "m_outContainerName"            : ZHF_config.TruthMUSel_outContainer,\n')
          else: # DAOD_PHYS
            Config.write('  "m_inContainerName"             : "TruthMuons",\n')
            Config.write('  "m_outContainerName"            : "TruthMuons_selected",\n')
          Config.write('  "m_createSelectedContainer"     : True,\n')
          Config.write('  "m_decorateSelectedObjects"     : False,\n')
          Config.write('  } )\n')
          Config.write('\n')
        ##########################
        # TruthElectronSelector
        ##########################
        if 'MC' in sample and inputType != 'AOD' and cfdict['truth']:
          Config.write('c.algorithm("TruthSelector", {\n')
          Config.write('  "m_name"                           : "TruthElectronSelector",\n')
          Config.write('  "m_msgLevel"                       : ZHF_config.msgLevel,\n')
          Config.write('  "m_typeOptions"                    : ZHF_config.TruthELSel_types,\n')
          Config.write('  "m_originOptions"                  : ZHF_config.TruthELSel_origins,\n')
          Config.write('  "m_pT_dressed_min"                 : ZHF_config.TruthELSel_pt_dressed_min,\n')
          Config.write('  "m_eta_dressed_min"                : ZHF_config.TruthELSel_eta_dressed_min,\n')
          Config.write('  "m_eta_dressed_max"                : ZHF_config.TruthELSel_eta_dressed_max,\n')
          if inputType != 'PHYS':
            Config.write('  "m_inContainerName"                : ZHF_config.TruthELSel_inContainer,\n')
            Config.write('  "m_outContainerName"               : ZHF_config.TruthELSel_outContainer,\n')
          else: # DAOD_PHYS
            Config.write('  "m_inContainerName"                : "TruthElectrons",\n')
            Config.write('  "m_outContainerName"               : "TruthElectrons_selected",\n')
          Config.write('  "m_createSelectedContainer"        : True,\n')
          Config.write('  "m_decorateSelectedObjects"        : False,\n')
          Config.write('  } )\n')
          Config.write('\n')
        ##########################
        # TruthJetSelector
        ##########################
        if 'MC' in sample and inputType != 'AOD' and cfdict['truth']:
          Config.write('c.algorithm("JetSelector", {\n')
          Config.write('  "m_name"                           : "TruthJetSelector",\n')
          Config.write('  "m_msgLevel"                       : ZHF_config.msgLevel,\n')
          Config.write('  "m_pT_min"                         : ZHF_config.TruthJETSel_pT_min,\n')
          if inputType != 'PHYS':
            Config.write('  "m_inContainerName"                : ZHF_config.TruthJETSel_inContainer,\n')
          else: # DAOD_PHYS
            Config.write('  "m_inContainerName"                : "AntiKt4TruthDressedWZJets",\n')
          Config.write('  "m_outContainerName"               : ZHF_config.TruthJETSel_outContainer,\n')
          Config.write('  "m_createSelectedContainer"        : True,\n')
          Config.write('  "m_decorateSelectedObjects"        : False,\n')
          Config.write('  } )\n')
          Config.write('\n')
        ##########################
        # ZHFSelector
        ##########################
        Config.write('c.algorithm("ZHFSelector", {\n')
        Config.write('  "m_name"                           : "ZHFSelector",\n')
        Config.write('  "m_msgLevel"                       : ZHF_config.msgLevel,\n')
        Config.write('  "m_inContainerName_RecoMuons"      : ZHF_config.OR_outMUContainer,\n')
        Config.write('  "m_inContainerName_RecoElectrons"  : ZHF_config.OR_outELContainer,\n')
        Config.write('  "m_RecoZmass_min"                  : ZHF_config.ZHFSel_RecoZmass_min,\n')
        Config.write('  "m_RecoZmass_max"                  : ZHF_config.ZHFSel_RecoZmass_max,\n')
        if 'MC' in sample:
          if inputType != 'AOD' and cfdict['truth']:
            if inputType != 'PHYS':
              Config.write('  "m_inContainerName_TruthElectrons" : ZHF_config.ZHFSel_inContainer_TruthEL,\n')
              Config.write('  "m_inContainerName_TruthMuons"     : ZHF_config.ZHFSel_inContainer_TruthMU,\n')
            else: # DAOD_PHYS
              Config.write('  "m_inContainerName_TruthElectrons" : "TruthElectrons_selected",\n')
              Config.write('  "m_inContainerName_TruthMuons"     : "TruthMuons_selected",\n')
          else: # AOD or no-truth
            Config.write('  "m_makeTruthSelection"             : False,\n')
        Config.write('  "m_inputAlgoSystNames"             : "OverlapRemover_Algo",\n')
        Config.write('  "m_outputAlgoSystNames"            : "ZHFSelection_Algo",\n')
        Config.write('  } )\n')
        Config.write('\n')
        ##########################
        # MuonEfficiencyCorrector
        ##########################
        if 'MC' in sample:
          Config.write('TrigLegs = set()\n')
          if 'MC16a' in sample:
            Config.write('TrigLegs.add("2015:"+"_OR_".join(ZHF_config.MUTriggers_2015))\n')
            Config.write('TrigLegs.add("2016:"+"_OR_".join(ZHF_config.MUTriggers_2016))\n')
          elif 'MC16d' in sample:
            Config.write('TrigLegs.add("2017:"+"_OR_".join(ZHF_config.MUTriggers_2017))\n')
          elif 'MC16e' in sample:
            Config.write('TrigLegs.add("2018:"+"_OR_".join(ZHF_config.MUTriggers_2017))\n')
          Config.write('c.algorithm("MuonEfficiencyCorrector", {\n')
          Config.write('  "m_name"                        : "MuonEfficiencyCorrector",\n')
          Config.write('  "m_msgLevel"                    : ZHF_config.msgLevel,\n')
          Config.write('  "m_inContainerName"             : ZHF_config.MUEff_inContainer,\n')
          Config.write('  "m_MuTrigLegs"                  : ",".join(TrigLegs),\n')
          Config.write('  "m_WorkingPointReco"            : ZHF_config.MUEff_WorkingPointReco,\n')
          Config.write('  "m_WorkingPointIso"             : ZHF_config.MUEff_WorkingPointIso,\n')
          Config.write('  "m_WorkingPointTTVA"            : ZHF_config.MUEff_WorkingPointTTVA,\n')
          Config.write('  "m_inputSystNamesMuons"         : "Muons_Selected_Algo",\n')
          Config.write('  "m_usePerMuonTriggerSFs"        : False,\n') # properly calculate single muon trigger SF (using all muons), same value is saved for every muon
          if cfdict['sysType']!='Full': # muon efficiency SF systematics only in nominal TTrees
            Config.write('  "m_systNameReco"                : "All",\n')
            Config.write('  "m_systNameIso"                 : "All",\n')
            Config.write('  "m_systNameTrig"                : "All",\n')
            Config.write('  "m_systNameTTVA"                : "All",\n')
            Config.write('  "m_systValReco"                 : 1.0,\n')
            Config.write('  "m_systValIso"                  : 1.0,\n')
            Config.write('  "m_systValTrig"                 : 1.0,\n')
            Config.write('  "m_systValTTVA"                 : 1.0,\n')
            Config.write('  "m_writeSystToMetadata"         : True,\n')
          Config.write('  } )\n')
          Config.write('\n')
        ##############################
        # ElectronEfficiencyCorrector
        ##############################
        if 'MC' in sample:
          Config.write('c.algorithm("ElectronEfficiencyCorrector", {\n')
          Config.write('  "m_name"                        : "ElectronEfficiencyCorrector",\n')
          Config.write('  "m_msgLevel"                    : ZHF_config.msgLevel,\n')
          Config.write('  "m_inContainerName"             : ZHF_config.ELEff_inContainer,\n')
          Config.write('  "m_WorkingPointReco"            : ZHF_config.ELEff_WorkingPointReco,\n')
          Config.write('  "m_WorkingPointPID"             : ZHF_config.ELEff_WorkingPointPID,\n')
          Config.write('  "m_WorkingPointIso"             : ZHF_config.ELEff_WorkingPointIso,\n')
          Config.write('  "m_WorkingPointTrig"            : ZHF_config.ELEff_WorkingPointTrig,\n')
          Config.write('  "m_correlationModel"            : ZHF_config.ELEff_CorrelationModel,\n')
          Config.write('  "m_inputSystNamesElectrons"     : "Electrons_Selected_Algo",\n')
          Config.write('  "m_usePerElectronTriggerSFs"    : False,\n') # properly calculate single electron trigger SF (using all leptons), same value is saved for every electron
          if cfdict['sysType']!='Full': # electron efficiency SF systematics only in nominal TTrees
            Config.write('  "m_systNameReco"                : "All",\n')
            Config.write('  "m_systNamePID"                 : "All",\n')
            Config.write('  "m_systNameIso"                 : "All",\n')
            Config.write('  "m_systNameTrig"                : "All",\n')
            Config.write('  "m_systValReco"                 : 1.0,\n')
            Config.write('  "m_systValPID"                  : 1.0,\n')
            Config.write('  "m_systValIso"                  : 1.0,\n')
            Config.write('  "m_systValTrig"                 : 1.0,\n')
            Config.write('  "m_writeSystToMetadata"         : True,\n')
          Config.write('  } )\n')
          Config.write('\n')
        ##########################
        # Fix for missing links
        ##########################
        if Fix4MissingLinks:
          Config.write('###########################################################################################\n')
          Config.write('# Temporary (due to missing links to origin containers from the btagging containers)\n')
          Config.write('c.algorithm("JetCalibrator", {\n')
          Config.write('  "m_name"                        : "JetCalibrator_orig",\n')
          if jetcoll == 'AntiKt4EMTopo':
            if 'MC' in sample:
              Config.write('  "m_calibConfigFullSim"          : ZHF_config.EMcalibConfigFullSim,\n')
            else: # data
              Config.write('  "m_calibConfigData"             : ZHF_config.EMcalibConfigData,\n')
          elif jetcoll == 'AntiKt4EMPFlow': # PFlow
            if 'MC' in sample:
              Config.write('  "m_calibConfigFullSim"          : ZHF_config.PFcalibConfigFullSim,\n')
            else:
              Config.write('  "m_calibConfigData"             : ZHF_config.PFcalibConfigData,\n')
          if 'MC' in sample:
            Config.write('  "m_calibSequence"               : ZHF_config.calibSequenceMC,\n')
          else: # data
            Config.write('  "m_calibSequence"               : ZHF_config.calibSequenceData,\n')
          Config.write('  "m_sort"                        : True,\n')
          Config.write('  "m_doCleaning"                  : ZHF_config.doCleaning,\n')
          Config.write('  "m_jetCleanCutLevel"            : ZHF_config.jetCleanCutLevel,\n')
          Config.write('  "m_saveAllCleanDecisions"       : False,\n')
          Config.write('  "m_inContainerName"             : jetAlgo+"Jets",\n')
          Config.write('  "m_outContainerName"            : "JetCalibrator_orig_out",\n')
          Config.write('  "m_jetAlgo"                     : jetAlgo,\n')
          Config.write('  "m_redoJVT"                     : ZHF_config.redoJVT,\n')
          Config.write('  "m_calculatefJVT"               : ZHF_config.calculatefJVT,\n')
          Config.write('  "m_addGhostMuonsToJets"         : ZHF_config.addGhostMuonsToJets,\n')
          Config.write('  "m_outputAlgo"                  : "JetCalibrator_orig_Algo",\n')
          if cfdict['sysType'] == 'Full':
            Config.write('  "m_systName"                    : "All",\n')
            Config.write('  "m_systVal"                     : 1.0,\n')
          Config.write('  } )\n')
          Config.write('###########################################################################################\n')
          Config.write('\n')
        ##########################
        # METConstructor
        ##########################
        Config.write('c.algorithm("METConstructor", {\n')
        Config.write('  "m_name"                        : "met",\n')
        Config.write('  "m_msgLevel"                    : ZHF_config.msgLevel,\n')
        Config.write('  "m_referenceMETContainer"       : ZHF_config.referenceMETContainer+jetAlgo,\n')
        Config.write('  "m_mapName"                     : ZHF_config.MET_mapName+jetAlgo,\n')
        Config.write('  "m_coreName"                    : ZHF_config.MET_coreName+jetAlgo,\n')
        Config.write('  "m_outputContainer"             : ZHF_config.MET_outputContainer,\n')
        Config.write('  "m_inputMuons"                  : ZHF_config.MET_inputMuons,\n')
        if Fix4MissingLinks:
          Config.write('  "m_inputJets"                   : "JetCalibrator_orig_out",\n')
        else:
          Config.write('  "m_inputJets"                   : ZHF_config.MET_inputJets,\n')
        Config.write('  "m_inputElectrons"              : ZHF_config.MET_inputElectrons,\n')
        if jetcoll == 'AntiKt4EMPFlow':
          Config.write('  "m_doPFlow"                     : True,\n')
          Config.write('  "m_doMuonPFlowBugfix"           : True,\n')
        Config.write('  "m_doJVTCut"                    : ZHF_config.MET_doJVTCut,\n')
        Config.write('  "m_dofJVTCut"                   : ZHF_config.MET_dofJVTCut,\n')
        Config.write('  "m_calculateSignificance"       : ZHF_config.MET_calculateSignificance,\n')
        if Fix4MissingLinks:
          Config.write('  "m_jetSystematics"              : "JetCalibrator_orig_Algo",\n')
        else:
          Config.write('  "m_jetSystematics"              : "Jets_Calibrated_Algo",\n')
        Config.write('  "m_outputAlgoSystNames"         : "MET_Algo",\n')
        Config.write('  "m_muonSystematics"             : "Muons_Selected_Algo",\n')
        Config.write('  "m_eleSystematics"              : "Electrons_Selected_Algo",\n')
        if cfdict['sysType'] == 'Full':
          Config.write('  "m_runNominal"                  : False,\n')
        Config.write('  } )\n')
        Config.write('\n')
        ##########################
        # ZHFTreeAlgo
        ##########################
        # Add trigger SFs
        if 'MC' in sample:
          Config.write('muDetailStr = ZHF_config.muDetailStr\n')
          if '2015' in sample or 'MC16a' in sample:
            Config.write('muDetailStr += " TRIG_"\n')
            Config.write('counter = 0\n')
            Config.write('for trigger in ZHF_config.MUTriggers_2015:\n')
            Config.write('  if counter == 0:\n')
            Config.write('    muDetailStr += trigger\n')
            Config.write('  else:\n')
            Config.write('    muDetailStr += "_OR_"+trigger\n')
            Config.write('  counter += 1\n')
          if '2016' in sample or 'MC16a' in sample:
            Config.write('muDetailStr += " TRIG_"\n')
            Config.write('counter = 0\n')
            Config.write('for trigger in ZHF_config.MUTriggers_2016:\n')
            Config.write('  if counter == 0:\n')
            Config.write('    muDetailStr += trigger\n')
            Config.write('  else:\n')
            Config.write('    muDetailStr += "_OR_"+trigger\n')
            Config.write('  counter += 1\n')
          if '2017' in sample or 'MC16d' in sample:
            Config.write('muDetailStr += " TRIG_"\n')
            Config.write('counter = 0\n')
            Config.write('for trigger in ZHF_config.MUTriggers_2017:\n')
            Config.write('  if counter == 0:\n')
            Config.write('    muDetailStr += trigger\n')
            Config.write('  else:\n')
            Config.write('    muDetailStr += "_OR_"+trigger\n')
            Config.write('  counter += 1\n')
          if '2018' in sample or 'MC16e' in sample:
            Config.write('muDetailStr += " TRIG_"\n')
            Config.write('counter = 0\n')
            Config.write('for trigger in ZHF_config.MUTriggers_2018:\n')
            Config.write('  if counter == 0:\n')
            Config.write('    muDetailStr += trigger\n')
            Config.write('  else:\n')
            Config.write('    muDetailStr += "_OR_"+trigger\n')
            Config.write('  counter += 1\n')
        # Add jet details
        Config.write('jetDetailStr = ZHF_config.jetDetailStr\n')
        if 'MC' in sample: Config.write('jetDetailStr += " Truth"\n')
        Config.write('for wp in JVTwps:\n')
        Config.write('  jetDetailStr += " sfJVT"+wp\n')
        Config.write('for tagger in ZHF_config.bJET_TaggerNames:\n')
        if cfdict['sysType'] == 'Full':
          Config.write('  if tagger != ZHF_config.bJET_TaggerNames[0]: continue # skip non-nominal tagger for systematic TTrees\n')
        Config.write('  jetDetailStr += " BTagging_"+tagger+"_"+ZHF_config.bJET_OperatingPoint\n')
        Config.write('  for wp in ZHF_config.bJET_FixedOperatingPoints:\n')
        if cfdict['sysType'] == 'Full': Config.write('    if wp != ZHF_config.bJET_FixedOperatingPoints[0]: continue # skip non-nominal WP for systematic TTrees\n')
        Config.write('    jetDetailStr += " jetBTag_"+tagger+"_"+wp\n')
        Config.write('\n')
        # Configure ZHFTreeAlgo
        Config.write('c.algorithm("ZHFTreeAlgo", {\n')
        Config.write('  "m_name"                        : "ZHFTreeAlgo",\n')
        Config.write('  "m_msgLevel"                    : ZHF_config.msgLevel,\n')
        if cfdict['sysType'] == 'Full':
          Config.write('  "m_autoFlush"                   : -500000,\n')
        Config.write('  "m_jetContainerName"            : ZHF_config.OR_outJETContainer,\n')
        if 'MC' in sample:
          if cfdict['sysType'] == 'Full':
            Config.write('  "m_jetSystsVec"                 : "Jets_Selected_Algo",\n')
	  else: # nominal
            if inputType != 'AOD' and cfdict['truth']:
              Config.write('  "m_truthJetContainerName"       : ZHF_config.truthJetContainerName,\n')
        if cfdict['sysType'] == 'Full':
          Config.write('  "m_muSystsVec"                  : "ZHFSelection_Algo",\n')
          Config.write('  "m_elSystsVec"                  : "ZHFSelection_Algo",\n')
        Config.write('  "m_METContainerName"            : ZHF_config.FinalMETContainerName,\n')
        if 'MC' in sample:
          if cfdict['sysType'] == 'Full':
            Config.write('  "m_metSystsVec"                 : "MET_Algo",\n')
            Config.write('  "m_evtDetailStr"                : ZHF_config.evtDetailStrMC_Systs,\n')
            Config.write('  "m_skipNominal"                 : True,\n')
	  else: # nominal
            if cftype == 'NominalOnlyBkgsAndData':
              Config.write('  "m_evtDetailStr"                : ZHF_config.evtDetailStrMC_NominalBkgs,\n')
            else: # Signal
              Config.write('  "m_evtDetailStr"                : ZHF_config.evtDetailStrMC_NominalSignal,\n')
        else:
          Config.write('  "m_evtDetailStr"                : ZHF_config.evtDetailStrData,\n')
        Config.write('  "m_jetDetailStr"                : jetDetailStr,\n')
        Config.write('  "m_elContainerName"             : ZHF_config.OR_outELContainer,\n')
        Config.write('  "m_muContainerName"             : ZHF_config.OR_outMUContainer,\n')
        if 'MC' in sample:
          if cfdict['sysType'] == 'NominalOnly':
            if inputType != 'PHYS' and inputType != 'AOD' and cfdict['truth']:
              Config.write('  "m_truthParticlesContainerName" : ZHF_config.truthElectronContainerName+" "+ZHF_config.truthMuonContainerName,\n')
            elif inputType == 'PHYS' and cfdict['truth']:
              Config.write('  "m_truthParticlesContainerName" : "TruthElectrons_selected TruthMuons_selected",\n')
        if 'MC' in sample:
          Config.write('  "m_muDetailStr"                 : muDetailStr,\n')
        else: # data
          Config.write('  "m_muDetailStr"                 : ZHF_config.muDetailStr,\n')
        Config.write('  "m_elDetailStr"                 : ZHF_config.elDetailStr,\n')
        Config.write('  "m_METDetailStr"                : ZHF_config.metDetailStr,\n')
        if 'MC' in sample:
          if inputType != 'AOD' and cfdict['sysType'] == 'NominalOnly' and cfdict['truth']:
            Config.write('  "m_truthParticlesDetailStr"     : ZHF_config.truthParticlesDetailStr+"|"+ZHF_config.truthParticlesDetailStr,\n')
            Config.write('  "m_truthParticlesBranchName"    : "truthElectron truthMuon",\n')
            Config.write('  "m_truthJetDetailStr"           : ZHF_config.truthJetDetailStr,\n')
          Config.write('  "m_trigDetailStr"               : ZHF_config.trigDetailStrMC,\n')
        else:
          Config.write('  "m_trigDetailStr"               : ZHF_config.trigDetailStrData,\n')
        Config.write('  } )\n')
        Config.write('\n')
        ##########################
        # WeightNamesAlgo
        ##########################
        if 'MC' in sample and inputType != 'AOD' and cfdict['sysType'] != 'Full':
          Config.write('c.algorithm("WeightNamesAlgo", {\n')
          Config.write('  "m_name"                           : "WeightNamesAlgo",\n')
          Config.write('  "m_msgLevel"                       : ZHF_config.msgLevel,\n')
          Config.write('  } )\n')
          Config.write('\n')
        Config.close()

print('>>> ALL DONE <<<')

