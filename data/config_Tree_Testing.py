import ROOT
from MakeTTrees_ZHF import CommonDefs as ZHF_config
from xAODAnaHelpers import Config     as xAH_config

c = xAH_config()

jetAlgo = "AntiKt4EMPFlow"
jetType = jetAlgo.replace("AntiKt4","")

c.algorithm("BasicEventSelection", {
  "m_name"                        : "BasicSelection",
  "m_msgLevel"                    : ZHF_config.msgLevel,
  "m_applyGRLCut"                 : False,
  "m_derivationName"              : "STDM3Kernel",
  "m_useMetaData"                 : True,
  "m_storePassHLT"                : True,
  "m_storeTrigDecisions"          : True,
  "m_applyTriggerCut"             : True,
  "m_triggerSelection"            : " | ".join(set(ZHF_config.MUTriggers_2015+ZHF_config.MUTriggers_2016)),
  "m_checkDuplicatesMC"           : True,
  "m_applyJetCleaningEventFlag"   : ZHF_config.applyJetCleaningEventFlagMC,
  "m_applyEventCleaningCut"       : ZHF_config.applyEventCleaningCut,
  "m_PVNTrack"                    : ZHF_config.minPVnTrack,
  "m_applyPrimaryVertexCut"       : ZHF_config.applyPrimaryVertexCut,
  "m_vertexContainerName"         : ZHF_config.vertexContainerName,
  "m_doPUreweighting"             : True,
  "m_lumiCalcFileNames"           : ",".join(ZHF_config.iLumis["MC16a"]),
  "m_PRWFileNames"                : ",".join(ZHF_config.PRWs["MC16a"]),
  } )

c.algorithm("MuonCalibrator", {
  "m_name"                     : "MuonCalibrator",
  "m_msgLevel"                 : ZHF_config.msgLevel,
  "m_sort"                     : True,
  "m_inContainerName"          : ZHF_config.MUCalib_inContainer,
  "m_outContainerName"         : ZHF_config.MUCalib_outContainer,
  "m_statComb1516"             : ZHF_config.MUCalib_statComb,
  "m_sagittaCorr1516"          : ZHF_config.MUCalib_sagittaCorr,
  "m_doSagittaMCDistortion1516": ZHF_config.MUCalib_doSagittaMCDistortion,
  "m_sagittaCorrPhaseSpace1516": ZHF_config.MUCalib_sagittaCorrPhaseSpace,
  "m_outputAlgoSystNames"      : "Muons_Calibrated_Algo",
  "m_systName"                 : "All",
  "m_systVal"                  : 1.0,
  } )
c.algorithm("MuonSelector", {
  "m_name"                     : "MuonBaseSelector",
  "m_msgLevel"                 : ZHF_config.msgLevel,
  "m_removeEventBadMuon"       : ZHF_config.MUBaseSel_rmEventBadMuon,
  "m_pT_min"                   : ZHF_config.MUBaseSel_pT_min,
  "m_eta_max"                  : ZHF_config.MUBaseSel_eta_max,
  "m_z0sintheta_max"           : ZHF_config.MUBaseSel_z0sintheta_max,
  "m_d0sig_max"                : ZHF_config.MUBaseSel_d0sig_max,
  "m_IsoWPList"                : ZHF_config.MUBaseSel_IsoWPList,
  "m_muonQualityStr"           : ZHF_config.MUBaseSel_Quality,
  "m_inContainerName"          : ZHF_config.MUBaseSel_inContainer,
  "m_outContainerName"         : ZHF_config.MUBaseSel_outContainer,
  "m_singleMuTrigChains"       : ",".join(set(ZHF_config.MUTriggers_2015+ZHF_config.MUTriggers_2016)),
  "m_inputAlgoSystNames"       : "Muons_Calibrated_Algo",
  "m_outputAlgoSystNames"      : "Muons_BaseSelected_Algo",
  "m_createSelectedContainer"  : True,
  "m_decorateSelectedObjects"  : True,
  } )

# Calibrate electrons and select events with no electrons
c.algorithm("ElectronCalibrator", {
  "m_name"                           : "ElectronCalibrator",
  "m_sort"                           : True,
  "m_esModel"                        : ZHF_config.ELCalib_esModel,
  "m_decorrelationModel"             : ZHF_config.ELCalib_decorrelationModel,
  "m_inContainerName"                : ZHF_config.ELCalib_inContainer,
  "m_outContainerName"               : ZHF_config.ELCalib_outContainer,
  } )
c.algorithm("ElectronSelector", {
  "m_name"                           : "ElectronBaseSelector",
  "m_msgLevel"                       : ZHF_config.msgLevel,
  "m_pass_max"                       : 0,
  "m_pT_min"                         : ZHF_config.ELBaseSel_pT_min,
  "m_eta_max"                        : ZHF_config.ELBaseSel_eta_max,
  "m_z0sintheta_max"                 : ZHF_config.ELBaseSel_z0sintheta_max,
  "m_d0sig_max"                      : ZHF_config.ELBaseSel_d0sig_max,
  "m_IsoWPList"                      : ZHF_config.ELBaseSel_IsoWPList,
  "m_inContainerName"                : ZHF_config.ELBaseSel_inContainer,
  "m_outContainerName"               : ZHF_config.ELBaseSel_outContainer,
  "m_readIDFlagsFromDerivation"      : ZHF_config.ELBaseSel_readIDFlags,
  "m_doLHPIDcut"                     : ZHF_config.ELBaseSel_doLHPIDcut,
  "m_LHOperatingPoint"               : ZHF_config.ELBaseSel_LHWP,
  "m_createSelectedContainer"        : True,
  "m_decorateSelectedObjects"        : True,
  } )

c.algorithm("JetCalibrator", {
  "m_name"                        : "JetCalibrator",
  "m_calibConfigFullSim"          : ZHF_config.PFcalibConfigFullSim,
  "m_calibSequence"               : ZHF_config.calibSequenceMC,
  "m_sort"                        : True,
  "m_doCleaning"                  : ZHF_config.doCleaning,
  "m_jetCleanCutLevel"            : ZHF_config.jetCleanCutLevel,
  "m_saveAllCleanDecisions"       : False,
  "m_inContainerName"             : jetAlgo+"Jets"+ZHF_config.bJET_timeStamp,
  "m_outContainerName"            : ZHF_config.JETCalib_outContainer,
  "m_jetAlgo"                     : jetAlgo,
  "m_redoJVT"                     : ZHF_config.redoJVT,
  "m_calculatefJVT"               : ZHF_config.calculatefJVT,
  "m_addGhostMuonsToJets"         : ZHF_config.addGhostMuonsToJets,
  "m_outputAlgo"                  : "Jets_Calibrated_Algo",
  "m_uncertConfig"                : ZHF_config.uncertConfig,
  "m_systName"                    : "All",
  "m_systVal"                     : 1.0,
  } )

JVTwps  = ZHF_config.JETBaseSel_JVTwp[jetAlgo]
nJVTwps = len(JVTwps)
for iwp in range(0,nJVTwps):
  if iwp == 0:
    inContainer = ZHF_config.JETBaseSel_inContainer
    inAlg       = "Jets_Calibrated_Algo"
  else:
    inContainer = "JETBaseSel_"+JVTwps[iwp-1]
    inAlg       = "Jets_BaseSelected_Algo_"+JVTwps[iwp-1]
  if iwp == nJVTwps-1:
    outContainer = ZHF_config.JETBaseSel_outContainer
    outAlg       = "Jets_BaseSelected_Algo"
  else:
    outContainer = "JETBaseSel_"+JVTwps[iwp]
    outAlg       = "Jets_BaseSelected_Algo_"+JVTwps[iwp]
  c.algorithm("JetSelector", {
    "m_name"                        : "JetBaseSelector_"+JVTwps[iwp],
    "m_msgLevel"                    : ZHF_config.msgLevel,
    "m_pT_min"                      : ZHF_config.JETBaseSel_pT_min,
    "m_eta_min"                     : ZHF_config.JETBaseSel_eta_min,
    "m_eta_max"                     : ZHF_config.JETBaseSel_eta_max,
    "m_inContainerName"             : inContainer,
    "m_outContainerName"            : outContainer,
    "m_cleanJets"                   : ZHF_config.JETBaseSel_cleanJets,
    "m_cleanEvent"                  : ZHF_config.JETBaseSel_cleanEvent,
    "m_createSelectedContainer"     : True,
    "m_decorateSelectedObjects"     : True,
    "m_doJVT"                       : ZHF_config.JETBaseSel_doJVT,
    "m_noJVTVeto"                   : ZHF_config.JETBaseSel_noJVTVeto,
    "m_WorkingPointJVT"             : JVTwps[iwp],
    "m_SFFileJVT"                   : ZHF_config.JETBaseSel_SFFileJVT+jetType+"Jets.root",
    "m_dofJVT"                      : ZHF_config.JETBaseSel_dofJVT,
    "m_inputAlgo"                   : inAlg,
    "m_outputAlgo"                  : outAlg,
    "m_systValJVT"                  : 1.0,
    "m_systNameJVT"                 : "All",
  } )

counter = 0
#for tagger in ZHF_config.bJET_TaggerNames:
for itagger in range(0,len(ZHF_config.bJET_TaggerNames)):
  c.algorithm("BJetEfficiencyCorrector", {
    "m_name"                        : "BJetEffCorrector_continuous_"+ZHF_config.bJET_TaggerNames[itagger],
    "m_msgLevel"                    : ZHF_config.msgLevel,
    "m_inContainerName"             : ZHF_config.bJET_inContainer,
    "m_jetAuthor"                   : jetAlgo+"Jets"+ZHF_config.bJET_timeStamp,
    "m_minPt"                       : ZHF_config.bJET_MinPt,
    "m_decor"                       : ZHF_config.bJET_decor,
    "m_corrFileName"                : ZHF_config.bJET_corrFileName,
    "m_taggerName"                  : ZHF_config.bJET_TaggerNames[itagger],
    "m_operatingPt"                 : ZHF_config.bJET_OperatingPoint,
    "m_coneFlavourLabel"            : ZHF_config.bJET_coneFlavourLabel,
    "m_useDevelopmentFile"          : ZHF_config.bJET_useDevelopmentFile,
    "m_inputAlgo"                   : "Jets_BaseSelected_Algo",
    } )
  #for wp in ZHF_config.bJET_FixedOperatingPoints:
  for iwp in range(0,len(ZHF_config.bJET_FixedOperatingPoints)):
    #if counter == 0:
    #inAlgo  = "Jets_BaseSelected_Algo"
      #outAlgo = "BJet_Signal_Algo_"+ZHF_config.bJET_TaggerNames[itagger]+"_"+ZHF_config.bJET_FixedOperatingPoints[iwp]
    #else:
      #inAlgo  = "BJet_Signal_Algo_"+ZHF_config.bJET_TaggerNames[itagger-1]+"_"+ZHF_config.bJET_FixedOperatingPoints[iwp-1]
      #outAlgo = "BJet_Signal_Algo"
    c.algorithm("BJetEfficiencyCorrector", {
      "m_name"                        : "BJetEffCorrector_calibratedWP_"+ZHF_config.bJET_TaggerNames[itagger]+"_"+ZHF_config.bJET_FixedOperatingPoints[iwp],
      "m_msgLevel"                    : ZHF_config.msgLevel,
      "m_inContainerName"             : ZHF_config.bJET_inContainer,
      "m_jetAuthor"                   : jetAlgo+"Jets"+ZHF_config.bJET_timeStamp,
      "m_minPt"                       : ZHF_config.bJET_MinPt,
      "m_decor"                       : ZHF_config.bJET_decor,
      "m_corrFileName"                : ZHF_config.bJET_corrFileName,
      "m_taggerName"                  : ZHF_config.bJET_TaggerNames[itagger],
      "m_operatingPt"                 : ZHF_config.bJET_FixedOperatingPoints[iwp],
      "m_coneFlavourLabel"            : ZHF_config.bJET_coneFlavourLabel,
      "m_useDevelopmentFile"          : ZHF_config.bJET_useDevelopmentFile,
      "m_inputAlgo"                   : "Jets_BaseSelected_Algo",
      "m_systName"                    : "All",
      } )
    counter += 1

TrigLegs = set()
TrigLegs.add("2015:"+"_OR_".join(ZHF_config.MUTriggers_2015))
TrigLegs.add("2016:"+"_OR_".join(ZHF_config.MUTriggers_2016))
c.algorithm("MuonEfficiencyCorrector", {
  "m_name"                        : "MuonEfficiencyCorrector",
  "m_msgLevel"                    : ZHF_config.msgLevel,
  "m_inContainerName"             : ZHF_config.MUEff_inContainer,
  "m_MuTrigLegs"                  : ",".join(TrigLegs),
  "m_WorkingPointReco"            : ZHF_config.MUEff_WorkingPointReco,
  "m_WorkingPointIso"             : ZHF_config.MUEff_WorkingPointIso,
  "m_WorkingPointTTVA"            : ZHF_config.MUEff_WorkingPointTTVA,
  "m_inputSystNamesMuons"         : "Muons_BaseSelected_Algo",
  "m_systNameReco"                : "All",
  "m_systNameIso"                 : "All",
  "m_systNameTrig"                : "All",
  "m_systNameTTVA"                : "All",
  "m_systValReco"                 : 1.0,
  "m_systValIso"                  : 1.0,
  "m_systValTrig"                 : 1.0,
  "m_systValTTVA"                 : 1.0,
  "m_outputSystNamesReco"         : "MuonEfficiencyCorrector_RecoSyst",
  "m_outputSystNamesIso"          : "MuonEfficiencyCorrector_IsoSyst",
  "m_outputSystNamesTrig"         : "MuonEfficiencyCorrector_TrigSyst",
  "m_outputSystNamesTTVA"         : "MuonEfficiencyCorrector_TTVASyst",
  } )

###########################################################################################
# Temporary (due to missing links to origin containers from the btagging containers)
c.algorithm("JetCalibrator", {
  "m_name"                        : "JetCalibrator_orig",
  "m_calibConfigFullSim"          : ZHF_config.PFcalibConfigFullSim,
  "m_calibSequence"               : ZHF_config.calibSequenceMC,
  "m_sort"                        : True,
  "m_doCleaning"                  : ZHF_config.doCleaning,
  "m_jetCleanCutLevel"            : ZHF_config.jetCleanCutLevel,
  "m_saveAllCleanDecisions"       : False,
  "m_inContainerName"             : jetAlgo+"Jets",
  "m_outContainerName"            : "JetCalibrator_orig_out",
  "m_jetAlgo"                     : jetAlgo,
  "m_redoJVT"                     : ZHF_config.redoJVT,
  "m_calculatefJVT"               : ZHF_config.calculatefJVT,
  "m_addGhostMuonsToJets"         : ZHF_config.addGhostMuonsToJets,
  "m_outputAlgo"                  : "JetCalibrator_orig_Algo",
  } )
c.algorithm("JetSelector", {
  "m_name"                        : "JetBaseSelector_orig",
  "m_msgLevel"                    : ZHF_config.msgLevel,
  "m_pT_min"                      : ZHF_config.JETBaseSel_pT_min,
  "m_eta_min"                     : ZHF_config.JETBaseSel_eta_min,
  "m_eta_max"                     : ZHF_config.JETBaseSel_eta_max,
  "m_inContainerName"             : "JetCalibrator_orig_out",
  "m_outContainerName"            : "JetSelector_orig_out",
  "m_cleanJets"                   : ZHF_config.JETBaseSel_cleanJets,
  "m_cleanEvent"                  : ZHF_config.JETBaseSel_cleanEvent,
  "m_createSelectedContainer"     : True,
  "m_decorateSelectedObjects"     : True,
  "m_doJVT"                       : False,
  "m_dofJVT"                      : ZHF_config.JETBaseSel_dofJVT,
  "m_inputAlgo"                   : "JetCalibrator_orig_Algo",
  "m_outputAlgo"                  : "JetSelector_orig_Algo",
  "m_systValJVT"                  : 1.0,
  "m_systNameJVT"                 : "All",
  } )
###########################################################################################

c.algorithm("METConstructor", {
  "m_name"                        : "met",
  "m_msgLevel"                    : ZHF_config.msgLevel,
  "m_referenceMETContainer"       : ZHF_config.referenceMETContainer+jetAlgo,
  "m_mapName"                     : ZHF_config.MET_mapName+jetAlgo,
  "m_coreName"                    : ZHF_config.MET_coreName+jetAlgo,
  "m_outputContainer"             : ZHF_config.MET_outputContainer,
  "m_inputMuons"                  : ZHF_config.MET_inputMuons,
  "m_inputJets"                   : "JetSelector_orig_out",
  "m_inputElectrons"              : ZHF_config.MET_inputElectrons,
  "m_doPFlow"                     : True,
  "m_doJVTCut"                    : ZHF_config.MET_doJVTCut,
  "m_dofJVTCut"                   : ZHF_config.MET_dofJVTCut,
  "m_calculateSignificance"       : ZHF_config.MET_calculateSignificance,
  "m_jetSystematics"              : "JetSelector_orig_Algo",
  "m_muonSystematics"             : "Muons_BaseSelected_Algo",
  "m_outputAlgoSystNames"         : "MET_Algo",
  "m_runNominal"                  : False,
  } )

c.algorithm("OverlapRemover", {
  "m_name"                        : "OverlapRemover",
  "m_msgLevel"                    : ZHF_config.msgLevel,
  "m_inContainerName_Jets"        : ZHF_config.OR_inJETContainer,
  "m_outContainerName_Jets"       : ZHF_config.OR_outJETContainer,
  "m_inContainerName_Muons"       : ZHF_config.OR_inMUContainer,
  "m_outContainerName_Muons"      : ZHF_config.OR_outMUContainer,
  "m_createSelectedContainers"    : True,
  "m_doMuPFJetOR"                 : True,
  "m_inputAlgoJets"               : "Jets_BaseSelected_Algo",
#  "m_inputAlgoJets"               : "BJet_Signal_Algo",
  "m_inputAlgoMuons"              : "Muons_BaseSelected_Algo",
#  "m_inputAlgoJets"               : "Jets_BaseSelected_Algo,MET_Algo",
#  "m_inputAlgoMuons"              : "Muons_BaseSelected_Algo,MET_Algo",
  "m_outputAlgoSystNames"         : "OverlapRemover_Algo",
  } )

c.algorithm("JetSelector", {
  "m_name"                        : "JetSignalSelector",
  "m_msgLevel"                    : ZHF_config.msgLevel,
  "m_pT_min"                      : ZHF_config.JETSignalSel_pT_min,
  "m_eta_min"                     : ZHF_config.JETSignalSel_eta_min,
  "m_eta_max"                     : ZHF_config.JETSignalSel_eta_max,
  "m_inContainerName"             : ZHF_config.JETSignalSel_inContainer,
  "m_outContainerName"            : ZHF_config.JETSignalSel_outContainer,
  "m_cleanJets"                   : ZHF_config.JETSignalSel_cleanJets,
  "m_cleanEvent"                  : ZHF_config.JETSignalSel_cleanEvent,
  "m_doJVT"                       : ZHF_config.JETSignalSel_doJVT,
  "m_dofJVT"                      : ZHF_config.JETSignalSel_dofJVT,
  "m_createSelectedContainer"     : ZHF_config.JETSignalSel_createContainer,
  "m_decorateSelectedObjects"     : ZHF_config.JETSignalSel_decorateObjects,
  "m_inputAlgo"                   : "OverlapRemover_Algo",
  "m_outputAlgo"                  : "Jets_Signal_Algo",
  } )

c.algorithm("MuonSelector", {
  "m_name"                        : "MuonSignalSelector",
  "m_msgLevel"                    : ZHF_config.msgLevel,
  "m_removeEventBadMuon"          : ZHF_config.MUSignalSel_rmEventBadMuon,
  "m_pT_min"                      : ZHF_config.MUSignalSel_pT_min,
  "m_eta_max"                     : ZHF_config.MUSignalSel_eta_max,
  "m_z0sintheta_max"              : ZHF_config.MUSignalSel_z0sintheta_max,
  "m_d0sig_max"                   : ZHF_config.MUSignalSel_d0sig_max,
  "m_IsoWPList"                   : ZHF_config.MUSignalSel_IsoWPList,
  "m_muonQualityStr"              : ZHF_config.MUSignalSel_Quality,
  "m_inContainerName"             : ZHF_config.MUSignalSel_inContainer,
  "m_outContainerName"            : ZHF_config.MUSignalSel_outContainer,
  "m_singleMuTrigChains"          : ",".join(set(ZHF_config.MUTriggers_2015+ZHF_config.MUTriggers_2016)),
  "m_createSelectedContainer"     : True,
  "m_decorateSelectedObjects"     : True,
  "m_inputAlgoSystNames"          : "OverlapRemover_Algo",
  "m_outputAlgoSystNames"         : "Muons_Signal_Algo",
  } )

c.algorithm("TruthSelector", {
  "m_name"                        : "TruthMuonSelector",
  "m_msgLevel"                    : ZHF_config.msgLevel,
  "m_type"                        : ZHF_config.TruthMUSel_type,
  "m_originOptions"               : ZHF_config.TruthMUSel_origins,
  "m_pT_dressed_min"              : ZHF_config.TruthMUSel_pt_dressed_min,
  "m_eta_dressed_min"             : ZHF_config.TruthMUSel_eta_dressed_min,
  "m_eta_dressed_max"             : ZHF_config.TruthMUSel_eta_dressed_max,
  "m_inContainerName"             : ZHF_config.TruthMUSel_inContainer,
  "m_outContainerName"            : ZHF_config.TruthMUSel_outContainer,
  "m_createSelectedContainer"     : True,
  "m_decorateSelectedObjects"     : False,
  } )

c.algorithm("JetSelector", {
  "m_name"                           : "TruthJetSelector",
  "m_msgLevel"                       : ZHF_config.msgLevel,
  "m_pT_min"                         : ZHF_config.TruthJETSel_pT_min,
  "m_inContainerName"                : ZHF_config.TruthJETSel_inContainer,
  "m_outContainerName"               : ZHF_config.TruthJETSel_outContainer,
  "m_createSelectedContainer"        : True,
  "m_decorateSelectedObjects"        : False,
  } )

c.algorithm("ZHFSelector", {
  "m_name"                           : "ZHFSelector",
  "m_msgLevel"                       : ZHF_config.msgLevel,
  "m_inContainerName_RecoMuons"      : ZHF_config.ZHFSel_inContainer_RecoMU,
  "m_RecoZmass_min"                  : ZHF_config.ZHFSel_RecoZmass_min,
  "m_RecoZmass_max"                  : ZHF_config.ZHFSel_RecoZmass_max,
  "m_inContainerName_TruthJets"      : ZHF_config.ZHFSel_inContainer_TruthJETS,
  "m_TruthDR_min"                    : ZHF_config.ZHFSel_TruthDR_min,
  "m_inContainerName_TruthMuons"     : ZHF_config.ZHFSel_inContainer_TruthMU,
  "m_inputAlgoSystNames"             : "Muons_Signal_Algo",
  "m_outputAlgoSystNames"            : "ZHFSelection_Algo",
  } )

muDetailStr = ZHF_config.muDetailStr
muDetailStr += " TRIG_"
counter = 0
for trigger in ZHF_config.MUTriggers_2015:
  if counter == 0:
    muDetailStr += trigger
  else:
    muDetailStr += "_OR_"+trigger
  counter += 1
muDetailStr += " TRIG_"
counter = 0
for trigger in ZHF_config.MUTriggers_2016:
  if counter == 0:
    muDetailStr += trigger
  else:
    muDetailStr += "_OR_"+trigger
  counter += 1

c.algorithm("ZHFTreeAlgo", {
  "m_name"                        : "ZHFTreeAlgo",
  "m_msgLevel"                    : ZHF_config.msgLevel,
  "m_jetContainerName"            : ZHF_config.FinalJetContainerName,
  "m_truthJetContainerName"       : ZHF_config.truthJetContainerName,
  "m_jetSystsVec"                 : "Jets_Signal_Algo",
  "m_muContainerName"             : ZHF_config.FinalMuonContainerName,
  "m_truthParticlesContainerName" : ZHF_config.truthMuonContainerName,
  "m_muSystsVec"                  : "ZHFSelection_Algo",
  "m_METContainerName"            : ZHF_config.FinalMETContainerName,
  "m_metSystsVec"                 : "MET_Algo",
  "m_evtDetailStr"                : ZHF_config.evtDetailStrWeightsSys,
  "m_jetDetailStr"                : ZHF_config.jetDetailStr,
  "m_muDetailStr"                 : muDetailStr,
  "m_METDetailStr"                : ZHF_config.metDetailStr,
  "m_truthParticlesDetailStr"     : ZHF_config.truthParticlesDetailStr,
  "m_truthJetDetailStr"           : ZHF_config.truthJetDetailStr,
  "m_trigDetailStr"               : ZHF_config.trigDetailStrMC,
  } )

