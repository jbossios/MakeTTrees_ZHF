
# General (level of verbosity) - Options: "info", "debug"
msgLevel                     = "info"

####################################################################################################################
# Triggers selection
####################################################################################################################

# Unprescaled single muon triggers
MUTriggers_2015              = ["HLT_mu20_iloose_L1MU15","HLT_mu50"]
MUTriggers_2016              = ["HLT_mu26_ivarmedium","HLT_mu50"]
MUTriggers_2017              = ["HLT_mu26_ivarmedium","HLT_mu50"]
MUTriggers_2018              = ["HLT_mu26_ivarmedium","HLT_mu50"]
# Unprescaled multi electron triggers
ELTriggers_2015              = ['HLT_e24_lhmedium_L1EM20VH','HLT_e60_lhmedium','HLT_e120_lhloose']
ELTriggers_2016              = ['HLT_e26_lhtight_nod0_ivarloose','HLT_e60_lhmedium_nod0','HLT_e140_lhloose_nod0','HLT_e300_etcut']
ELTriggers_2017              = ['HLT_e26_lhtight_nod0_ivarloose','HLT_e60_lhmedium_nod0','HLT_e140_lhloose_nod0','HLT_e300_etcut']
ELTriggers_2018              = ['HLT_e26_lhtight_nod0_ivarloose','HLT_e60_lhmedium_nod0','HLT_e140_lhloose_nod0','HLT_e300_etcut']

####################################################################################################################
# General event-level selections
####################################################################################################################

# BasicEventSelection (GRL, trigger, PVnTrack, etc selections)
derivationName               = "STDM3Kernel"
vertexContainerName          = "PrimaryVertices"
applyJetCleaningEventFlagMC  = False
applyJetCleaningEventFlagData= True
applyPrimaryVertexCut        = True
minPVnTrack                  = 2
applyEventCleaningCut        = True # only applied to data
applyIsBadBatmanFlag15       = True
applyIsBadBatmanFlag16       = True
applyIsBadBatmanFlag17       = False
applyIsBadBatmanFlag18       = False

# GRLs
GRL_2015 = "MakeTTrees_ZHF/data15_13TeV.periodAllYear_DetStatus-v89-pro21-02_Unknown_PHYS_StandardGRL_All_Good_25ns.xml"
GRL_2016 = "MakeTTrees_ZHF/data16_13TeV.periodAllYear_DetStatus-v89-pro21-01_DQDefects-00-02-04_PHYS_StandardGRL_All_Good_25ns.xml"
GRL_2017 = "MakeTTrees_ZHF/data17_13TeV.periodAllYear_DetStatus-v99-pro22-01_Unknown_PHYS_StandardGRL_All_Good_25ns_Triggerno17e33prim.xml"
GRL_2018 = "MakeTTrees_ZHF/data18_13TeV.periodAllYear_DetStatus-v102-pro22-04_Unknown_PHYS_StandardGRL_All_Good_25ns_Triggerno17e33prim.xml"

# PRW inputs
PRWs = dict()
PRWs["MC16a"] = [str("MakeTTrees_ZHF/PRW_MC16a_v11_06072022.root")]
PRWs["MC16d"] = [str("MakeTTrees_ZHF/PRW_MC16d_v11_06072022.root"),str("MakeTTrees_ZHF/2017_physics_25ns_Triggerno17e33prim.actualMu.OflLumi-13TeV-010.root")]
PRWs["MC16e"] = [str("MakeTTrees_ZHF/PRW_MC16e_v11_06072022.root"),str("MakeTTrees_ZHF/2018_physics_25ns_Triggerno17e33prim.actualMu.OflLumi-13TeV-010.root")]
iLumis = dict()
iLumis["MC16a"] = [
  str("GoodRunsLists/data15_13TeV/20170619/PHYS_StandardGRL_All_Good_25ns_276262-284484_OflLumi-13TeV-008.root"),
  str("GoodRunsLists/data16_13TeV/20180129/PHYS_StandardGRL_All_Good_25ns_297730-311481_OflLumi-13TeV-009.root"),
]
iLumis["MC16d"] = [str("GoodRunsLists/data17_13TeV/20180619/physics_25ns_Triggerno17e33prim.lumicalc.OflLumi-13TeV-010.root")]
iLumis["MC16e"] = [str("GoodRunsLists/data18_13TeV/20190318/ilumicalc_histograms_None_348885-364292_OflLumi-13TeV-010.root")]

####################################################################################################################
# Setup for muons
####################################################################################################################

# MuonCalibrator (calibration of reco muons)
MUCalib_inContainer     = "Muons"
MUCalib_outContainer    = "Muons_Calibrated"
MUCalib_calibrationMode = "correctData_CB"
MUCalib_forceDataCalib  = True
# not used in resolved configs
MUCalib_statComb              = 0
MUCalib_sagittaCorr           = 0
MUCalib_doSagittaMCDistortion = 1
MUCalib_sagittaCorrPhaseSpace = 0
MUCalib_forceDataCalib        = True

# Baseline Muon Selection (selection of reco muons after calibration / before OR)
MUBaseSel_inContainer        = MUCalib_outContainer
MUBaseSel_rmEventBadMuon     = False
MUBaseSel_outContainer       = "Muons_Selected"
MUBaseSel_pT_min             = 27e3 # 27 GeV muons @ calibrated pt
MUBaseSel_eta_max            = 2.5  # |eta|<2.5
MUBaseSel_z0sintheta_max     = 0.5
MUBaseSel_d0sig_max          = 3    # d0 significance (at BL) < 3
MUBaseSel_Quality            = "Medium"
MUBaseSel_IsoWPList          = "FCTight_FixedRad" # decorate w/ 'isIsolated_*' 4 each WP (but not dumped to TTrees)
MUBaseSel_MinIsoWPCut        = "FCTight_FixedRad" # selects only muons passing this
MUBaseSel_IsoWPList_boost    = "FCTight"          # decorate w/ 'isIsolated_*' 4 each WP (but not dumped to TTrees)
MUBaseSel_MinIsoWPCut_boost  = "FCTight"          # selects only muons passing this

# Muon Efficiency SFs (reco, iso, trigger, TTVA) | MC only
MUEff_inContainer            = MUBaseSel_outContainer
MUEff_WorkingPointReco       = MUBaseSel_Quality
MUEff_WorkingPointIso        = "FCTight_FixedRad"
MUEff_WorkingPointIso_boost  = "FCTight"
MUEff_WorkingPointTTVA       = "TTVA"

####################################################################################################################
# Setup for electrons
####################################################################################################################

# ElectronCalibrator (calibration of reco electrons)
ELCalib_inContainer          = "Electrons"
ELCalib_outContainer         = "Electrons_Calibrated"
ELCalib_esModel              = "es2018_R21_v0"
ELCalib_decorrelationModel   = "1NP_v1"

# Baseline Electron Selection (selection of reco electrons after calibration / before OR)
ELBaseSel_inContainer        = ELCalib_outContainer
ELBaseSel_outContainer       = "Electrons_Selected"
ELBaseSel_pT_min             = 27e3 # 27 GeV electrons @ calibrated pt
ELBaseSel_eta_max            = 2.47  # |eta|<2.47
ELBaseSel_z0sintheta_max     = 0.5
ELBaseSel_d0sig_max          = 5    # d0 significance (at BL) < 5
ELBaseSel_IsoWPList          = "Tight" # decorate w/ 'isIsolated_*' 4 each WP (but not dumped to TTrees)
ELBaseSel_MinIsoWPCut        = "Tight" # selects only muons passing this
ELBaseSel_readIDFlags        = True
ELBaseSel_doLHPIDcut         = True
ELBaseSel_LHWP               = "Tight"
ELBaseSel_applyCrackVetoFix  = True # discard events with an electron having DFCommonCrackVetoCleaning = False

# Electron Efficiency SFs (reco, PID, iso, trigger) | MC only
ELEff_inContainer            = ELBaseSel_outContainer
ELEff_WorkingPointReco       = "Reconstruction"
ELEff_WorkingPointPID        = "Tight"
ELEff_WorkingPointIso        = "Tight"
ELEff_WorkingPointTrig       = "SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0"
ELEff_CorrelationModel       = "TOTAL"

####################################################################################################################
# Setup for jets
####################################################################################################################

##############################################################
# Jet calibration and selection for resolved analysis
# 1. JetCalibrator (calibration of reco jets)
JETCalib_outContainer        = "Jets_Calibrated"
EMcalibConfigData            = "JES_MC16Recommendation_Consolidated_EMTopo_Apr2019_Rel21.config"
EMcalibConfigFullSim         = "JES_MC16Recommendation_Consolidated_EMTopo_Apr2019_Rel21.config"
PFcalibConfigData            = "JES_MC16Recommendation_Consolidated_PFlow_Apr2019_Rel21.config"
PFcalibConfigFullSim         = "JES_MC16Recommendation_Consolidated_PFlow_Apr2019_Rel21.config"
calibSequenceData            = "JetArea_Residual_EtaJES_GSC_Insitu"
calibSequenceMC              = "JetArea_Residual_EtaJES_GSC_Smear"
doCleaning                   = False
jetCleanCutLevel             = "LooseBad"
redoJVT                      = True
calculatefJVT                = False
WorkingPointfJVT             = "Tight" # not used if calculatefJVT is False
uncertConfig                 = "rel21/Summer2019/R4_CategoryReduction_SimpleJER.config"
addGhostMuonsToJets          = True # needed for MET
# 2. Baseline jet selection (selection of reco jets after calibration / before OR)
JETBaseSel_inContainer       = JETCalib_outContainer
JETBaseSel_outContainer      = "Jets_Selected"
JETBaseSel_pT_min            = 20e3  # 20 GeV jets @ calibrated pt
JETBaseSel_eta_min           = -4.5
JETBaseSel_eta_max           = 4.5
JETBaseSel_doJVT             = True  # identify jets not passing JVT (needed in MC?)
JETBaseSel_noJVTVeto         = True  # keep JVT-rejected jets and decorate passing status
JETBaseSel_JVTwp             = dict()
JETBaseSel_JVTwp['AntiKt4EMPFlow'] = ["Tight"]  # Temporary (need fix in xAH to use more than one WP)
JETBaseSel_JVTwp['AntiKt4EMTopo']  = ["Medium"] # Temporary (need fix in xAH to use more than one WP)
JETBaseSel_SFFileJVT         = "JetJvtEfficiency/Moriond2018/JvtSFFile_" # EMTopoJets.root or EMPFlowJets.root is appended in the config
JETBaseSel_dofJVT            = False # identify jets not passing fJVT?
JETBaseSel_dofJVTVeto        = False # keep fJVT-rejected jets and decorate passing status? (only used if dofJVT is True)
JETBaseSel_cleanJets         = False
JETBaseSel_cleanEvent        = False  # remove events not passing jet cleaning
#Jets_SFFilefJVT             = "JetJvtEfficiency/Moriond2018/fJvtSFFile.root"

##############################################################
# Baseline jet calibration and selection for boosted analysis
# 1. JetCalibrator for large-R jets
FatJETCalib_outContainer     = "Fat_Jets_Calibrated"
FatJetcalibConfigData        = "JES_MC16recommendation_FatJet_Trimmed_JMS_comb_3April2019.config"
FatJetcalibConfigFullSim     = "JES_MC16recommendation_FatJet_Trimmed_JMS_comb_17Oct2018.config"
FatcalibSequenceMC           = "EtaJES_JMS"
FatcalibSequenceData         = "EtaJES_JMS_Insitu_InsituCombinedMass"
FatcalibName                 = "FatJetCalibTool"
FatuncertConfig              = "rel21/Summer2019/R10_CategoryReduction.config"
# 2. Baseline selection for large-R jets
FatJETBaseSel_inContainer    = FatJETCalib_outContainer
FatJETBaseSel_outContainer   = "FatJets_Selected"
FatJETBaseSel_pT_min         = 200e3  # 200 GeV jets @ calibrated pt
FatJETBaseSel_eta_min        = -4.5
FatJETBaseSel_eta_max        = 4.5
JETBaseSel_Fat               = dict()
# 3. Baseline selection for track-jets
TrackJETBaseSel_outContainer   = "TrackJets_Selected"
TrackbJET_MinPt                = 7e3 # 7 GeV
TrackbJET_MinEta               = -2.5
TrackbJET_MaxEta               = 2.5
#
SmallRJETCalib_outContainer     = "SmallR_Jets_Calibrated"
SmallRJETBaseSel_inContainer    = SmallRJETCalib_outContainer
SmallRJETBaseSel_outContainer   = "SmallRJets_Selected"
SmallRJET_MinPt                 = 20e3 # 7 GeV
####################################################################################################################
# Setup for b-tagging
####################################################################################################################

# Following https://twiki.cern.ch/twiki/bin/view/AtlasProtected/BTagCalib2017#Recommendations_for_MC16_Based_A

# MCtoMC SFs
bJET_EfficiencyCalibration   = "410470;700122;410250;600666;410558;410464;700122"

# BJetEfficiencyCorrector (b-tagging decoration for calibrated WPs and SFs)
bJET_FixedOperatingPoints    = ["FixedCutBEff_85","FixedCutBEff_77","FixedCutBEff_70","FixedCutBEff_60"]

# BJetEfficiencyCorrector (continuous b-tagging for resolved analysis)
bJET_inContainer             = JETBaseSel_outContainer
bJET_corrFileName            = "xAODBTaggingEfficiency/13TeV/2020-21-13TeV-MC16-CDI-2021-04-16_v1.root"
bJET_TaggerNames             = ["DL1r"] # DL1 is also possible
bJET_TaggerNames_boost       = ["DL1r","DL1"]
bJET_MinPt                   = 20000 # 20 GeV
bJET_OperatingPoint          = "Continuous"
bJET_decor                   = "BTag"        # WP is automatically added
bJET_coneFlavourLabel        = True
bJET_useDevelopmentFile      = False
bJET_timeStamp               = "_BTagging201903"

# Track-jet b-tagging decoration for boosted analysis (doesn't seem to be used, FIXME)
bTrackJET_inContainer        = TrackJETBaseSel_outContainer
TrackbJET_decor              = "BTag"        # WP is automatically added

# smallR-jet b-tagging decoration for boosted analysis (doesn't seem to be used, FIXME)
bSmallJET_inContainer        = SmallRJETCalib_outContainer
SmallbJET_decor              = "BTag"        # WP is automatically added

####################################################################################################################
# Setup for overlap removal
####################################################################################################################

# Overlap Remover
OR_inJETContainer            = JETBaseSel_outContainer
OR_outJETContainer           = "Jets_Selected_passingOR"
# MU channel
OR_inMUContainer             = MUBaseSel_outContainer
OR_outMUContainer            = "Muons_Selected_passingOR"
# EL channel
OR_inELContainer             = ELBaseSel_outContainer
OR_outELContainer            = "Electrons_Selected_passingOR"
OR_useSlidingDR              = True
OR_applyRelPt                = True

####################################################################################################################
# Setup for MET
####################################################################################################################

# METConstructor
# Note: Tight is used by default (JetSelection == 'Tight') which is the recommended WP for PFlow
referenceMETContainer        = "MET_Reference_" # jetAlgo is appended in the config
MET_mapName                  = "METAssoc_"      # jetAlgo is appended in the config
MET_coreName                 = "MET_Core_"      # jetAlgo is appended in the config
MET_outputContainer          = "MET"
MET_doJVTCut	             = True
MET_dofJVTCut	             = False
MET_calculateSignificance    = False
MET_inputJets                = JETCalib_outContainer
MET_boostinputJets           = SmallRJETCalib_outContainer
MET_inputMuons               = MUBaseSel_outContainer
MET_inputElectrons           = ELBaseSel_outContainer

####################################################################################################################
# Truth object selections
####################################################################################################################

# Truth Muon Selector
TruthMUSel_inContainer       = "STDMTruthMuons"
TruthMUSel_outContainer      = "STDMTruthMuons_Selected"
TruthMUSel_type              = 6
TruthMUSel_types             = "5|6|7|8"
TruthMUSel_origins           = "10|12|13"
TruthMUSel_pt_dressed_min    = 25e3
TruthMUSel_eta_dressed_min   = -2.5
TruthMUSel_eta_dressed_max   = 2.5

# Truth Electron Selector
TruthELSel_inContainer       = "STDMTruthElectrons"
TruthELSel_outContainer      = "STDMTruthElectrons_Selected"
TruthELSel_type              = 2
TruthELSel_types             = "1|2|3|4"
TruthELSel_origins           = "10|12|13"
TruthELSel_pt_dressed_min    = 25e3
TruthELSel_eta_dressed_min   = -2.5
TruthELSel_eta_dressed_max   = 2.5

# Truth small-R jet selector for resolved analysis
TruthJETSel_inContainer      = "AntiKt4TruthWZJets"
TruthJETSel_outContainer     = "AntiKt4TruthWZJets_Selected"
TruthJETSel_pT_min           = 7e3  # 7 GeV truth jets

# Truth large-R jet selector for boosted analysis
FatTruthJETSel_inContainer   = "AntiKt10TruthTrimmedPtFrac5SmallR20Jets"
FatTruthJETSel_outContainer  = "AntiKt10TruthTrimmedPtFrac5SmallR20Jets_Selected"
FatTruthJETSel_pT_min        = 100e3 #100 GeV requirement

# Truth track-jet selector for boosted analysis
TrackTruthJETSel_inContainer   = "AntiKtVR30Rmax4Rmin02TruthChargedJets"
TrackTruthJETSel_outContainer  = "AntiKtVR30Rmax4Rmin02TruthChargedJets_Selected"
TrackTruthJETSel_pT_min        = 500 #500 MeV requirement


####################################################################################################################
# Event-level truth selections
####################################################################################################################

# ZHF Selector (select events passing reco or truth selections)
# Truth selection: exactly two opposite-sign leptons not matching a truth jet
# Reco selection:  exactly two opposite-sign leptons consistent with Zmass
ZHFSel_inContainer_TruthMU   = TruthMUSel_outContainer
ZHFSel_inContainer_TruthEL   = TruthELSel_outContainer
ZHFSel_inContainer_TruthJETS = TruthJETSel_outContainer
ZHFSel_TruthDR_min           = 0.4
ZHFSel_RecoZmass_min         = 71.  # -1 would apply no cut
ZHFSel_RecoZmass_max         = 111. # -1 would apply no cut

# Truth large-R jet container name
ZHFSel_inContainer_TruthFatJETS = FatTruthJETSel_outContainer
ZHFSel_inContainer_TruthTrackJETS = TrackTruthJETSel_outContainer

####################################################################################################################
# Configure TTree
####################################################################################################################

# ZHFTreeAlgo (dump what we need into a TTree/Ntuple)
truthJetContainerName        = TruthJETSel_outContainer
truthMuonContainerName       = TruthMUSel_outContainer
truthElectronContainerName   = TruthELSel_outContainer
FinalMETContainerName        = MET_outputContainer
truthMETContainerName        = "MET_Truth"
evtDetailStrData             = "pileup"
evtDetailStrMC_NominalSignal = "pileup pileupsys weightsSys truth noDataInfo PassRecoSel PassTruthSel JetCleaning"
evtDetailStrMC_NominalBkgs   = "pileup pileupsys weightsSys truth noDataInfo"
evtDetailStrMC_Systs         = "pileup noDataInfo PassRecoSel"
jetDetailStr                 = "noMultiplicity kinematic"
muDetailStr                  = "noMultiplicity kinematic trigger effSF RECO_Medium ISOL_FCTight_FixedRad"
muDetailStr_boost            = "kinematic trigger effSF RECO_Medium ISOL_FCTight"
elDetailStr                  = "noMultiplicity kinematic trigger effSF PIDSF_Tight ISOL_Tight TRIG_"+ELEff_WorkingPointTrig
metDetailStr                 = "noExtra"
trigDetailStrMC              = "basic passTriggers"
trigDetailStrData            = "basic passTriggers"
truthJetDetailStr            = "noMultiplicity kinematic Truth"
truthParticlesDetailStr      = "noMultiplicity dressed origin particleType"

# Final containers and jet details for boosted analysis
FinalFatJetContainerName     = FatJETBaseSel_outContainer
FinalTrackJetContainerName   = TrackJETBaseSel_outContainer
FinalSmallRJetContainerName  = SmallRJETBaseSel_outContainer
truthFatJetContainerName     = FatTruthJETSel_outContainer
truthTrackJetContainerName   = TrackTruthJETSel_outContainer
truthSmallRJetContainerName  = TruthJETSel_outContainer
fatjetDetailStr              = "kinematic"
fatjetDetailStr              += " substructure"
trackjetDetailStr            = "kinematic"
for tagger in bJET_TaggerNames_boost:
  trackjetDetailStr              += " BTagging_"+tagger
  trackjetDetailStr              += "_"
  trackjetDetailStr              += bJET_OperatingPoint
  for wp in bJET_FixedOperatingPoints:
    trackjetDetailStr            += " jetBTag_"
    trackjetDetailStr            += tagger
    trackjetDetailStr            += "_"
    trackjetDetailStr            += wp
trackjetDetailStr            += " flavorTag"
smallRjetDetailStr           = "kinematic"
for tagger in bJET_TaggerNames_boost:
  smallRjetDetailStr              += " BTagging_"+tagger
  smallRjetDetailStr              += "_"
  smallRjetDetailStr              += bJET_OperatingPoint
  for wp in bJET_FixedOperatingPoints:
    smallRjetDetailStr            += " jetBTag_"
    smallRjetDetailStr            += tagger
    smallRjetDetailStr            += "_"
    smallRjetDetailStr            += wp
smallRjetDetailStr            += " flavorTag"
truthFatJetDetailStr         = "kinematic"
truthTrackJetDetailStr       = "kinematic"

################################################################################################################################
# Tips
################################################################################################################################

# BTagging in JetSelector (in case is needed)
# set m_doBTagCut to True
# common for b- and c-tagging
#corrFileName                 = "xAODBTaggingEfficiency/13TeV/2017-21-13TeV-MC16-CDI-2018-10-19_v1.root" # https://twiki.cern.ch/twiki/bin/view/AtlasProtected/BTagCalib2017#Recommendations_for_MC16_Based_A
#jetAuthor                    = "AntiKt4EMTopoJets"
# c-tagging
#c_JETSelect_outContainer     = "cJets_Selected"
#c_taggerName                 = "DL1"             # m_taggerName
#c_operatingPt                = "CTag_Tight"      # m_operatingPt # add this WP to xAH!
# This could be helpful
# we could do something like
# operatingPt = "FixedCutBEff_70_Veto_DL1_CTag_Loose"
# the selection tool will require the jet to be tagged by the first working point and to not be tagged by the secondary tagger and working point

