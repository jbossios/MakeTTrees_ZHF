import ROOT
from MakeTTrees_ZHF import CommonDefs as ZHF_config
from xAODAnaHelpers import Config     as xAH_config

c = xAH_config()

jetAlgo = "AntiKt10LCTopoTrimmedPtFrac5SmallR20"
jetType = jetAlgo.replace("AntiKt4","")

c.algorithm("BasicEventSelection", {
  "m_name"                        : "BasicSelection",
  "m_msgLevel"                    : ZHF_config.msgLevel,
  "m_applyGRLCut"                 : False,
  "m_derivationName"              : "AODKernel",
  "m_useMetaData"                 : True,
  "m_storePassHLT"                : True,
  "m_storeTrigDecisions"          : True,
  "m_applyTriggerCut"             : True,
  "m_triggerSelection"            : " | ".join(ZHF_config.ELTriggers_2017+ZHF_config.MUTriggers_2017),
  "m_checkDuplicatesMC"           : True,
  "m_applyEventCleaningCut"       : ZHF_config.applyEventCleaningCut,
  "m_PVNTrack"                    : ZHF_config.minPVnTrack,
  "m_applyPrimaryVertexCut"       : ZHF_config.applyPrimaryVertexCut,
  "m_vertexContainerName"         : ZHF_config.vertexContainerName,
  "m_doPUreweighting"             : True,
  "m_lumiCalcFileNames"           : ",".join(ZHF_config.iLumis["MC16d"]),
  "m_PRWFileNames"                : ",".join(ZHF_config.PRWs["MC16d"]),
  } )

c.algorithm("MuonCalibrator", {
  "m_name"                     : "MuonCalibrator",
  "m_msgLevel"                 : ZHF_config.msgLevel,
  "m_sort"                     : True,
  "m_inContainerName"          : ZHF_config.MUCalib_inContainer,
  "m_outContainerName"         : ZHF_config.MUCalib_outContainer,
  "m_statComb17"               : ZHF_config.MUCalib_statComb,
  "m_sagittaCorr17"            : ZHF_config.MUCalib_sagittaCorr,
  "m_doSagittaMCDistortion17"  : ZHF_config.MUCalib_doSagittaMCDistortion,
  "m_sagittaCorrPhaseSpace17"  : ZHF_config.MUCalib_sagittaCorrPhaseSpace,
  "m_outputAlgoSystNames"      : "Muons_Calibrated_Algo",
  } )
c.algorithm("MuonSelector", {
  "m_name"                     : "MuonSelector",
  "m_msgLevel"                 : ZHF_config.msgLevel,
  "m_removeEventBadMuon"       : ZHF_config.MUBaseSel_rmEventBadMuon,
  "m_pT_min"                   : ZHF_config.MUBaseSel_pT_min,
  "m_eta_max"                  : ZHF_config.MUBaseSel_eta_max,
  "m_z0sintheta_max"           : ZHF_config.MUBaseSel_z0sintheta_max,
  "m_d0sig_max"                : ZHF_config.MUBaseSel_d0sig_max,
  "m_IsoWPList"                : ZHF_config.MUBaseSel_IsoWPList_boost,
  "m_MinIsoWPCut"              : ZHF_config.MUBaseSel_MinIsoWPCut_boost,
  "m_muonQualityStr"           : ZHF_config.MUBaseSel_Quality,
  "m_inContainerName"          : ZHF_config.MUBaseSel_inContainer,
  "m_outContainerName"         : ZHF_config.MUBaseSel_outContainer,
  "m_singleMuTrigChains"       : ",".join(ZHF_config.MUTriggers_2017),
  "m_inputAlgoSystNames"       : "Muons_Calibrated_Algo",
  "m_outputAlgoSystNames"      : "Muons_Selected_Algo",
  "m_createSelectedContainer"  : True,
  "m_decorateSelectedObjects"  : True,
  } )

c.algorithm("ElectronCalibrator", {
  "m_name"                           : "ElectronCalibrator",
  "m_sort"                           : True,
  "m_esModel"                        : ZHF_config.ELCalib_esModel,
  "m_decorrelationModel"             : ZHF_config.ELCalib_decorrelationModel,
  "m_inContainerName"                : ZHF_config.ELCalib_inContainer,
  "m_outContainerName"               : ZHF_config.ELCalib_outContainer,
  "m_outputAlgoSystNames"            : "Electrons_Calibrated_Algo",
  } )
c.algorithm("ElectronSelector", {
  "m_name"                           : "ElectronSelector",
  "m_msgLevel"                       : ZHF_config.msgLevel,
  "m_pT_min"                         : ZHF_config.ELBaseSel_pT_min,
  "m_eta_max"                        : ZHF_config.ELBaseSel_eta_max,
  "m_z0sintheta_max"                 : ZHF_config.ELBaseSel_z0sintheta_max,
  "m_d0sig_max"                      : ZHF_config.ELBaseSel_d0sig_max,
  "m_IsoWPList"                      : ZHF_config.ELBaseSel_IsoWPList,
  "m_MinIsoWPCut"                    : ZHF_config.ELBaseSel_MinIsoWPCut,
  "m_inContainerName"                : ZHF_config.ELBaseSel_inContainer,
  "m_outContainerName"               : ZHF_config.ELBaseSel_outContainer,
  "m_readIDFlagsFromDerivation"      : ZHF_config.ELBaseSel_readIDFlags,
  "m_doLHPIDcut"                     : ZHF_config.ELBaseSel_doLHPIDcut,
  "m_LHOperatingPoint"               : ZHF_config.ELBaseSel_LHWP,
  "m_applyCrackVetoCleaning"         : ZHF_config.ELBaseSel_applyCrackVetoFix,
  "m_vetoCrack"                      : False,
  "m_singleElTrigChains"             : ",".join(ZHF_config.ELTriggers_2017),
  "m_inputAlgoSystNames"             : "Electrons_Calibrated_Algo",
  "m_outputAlgoSystNames"            : "Electrons_Selected_Algo",
  "m_createSelectedContainer"        : True,
  "m_decorateSelectedObjects"        : True,
  } )

c.algorithm("JetCalibrator", {
  "m_name"                        : "JetCalibrator",
  "m_calibConfigFullSim"          : ZHF_config.FatJetcalibConfigFullSim,
  "m_calibSequence"               : ZHF_config.FatcalibSequenceMC,
  "m_sort"                        : True,
  "m_inContainerName"             : jetAlgo+"Jets",
  "m_outContainerName"            : ZHF_config.FatJETCalib_outContainer,
  "m_jetAlgo"                     : jetAlgo,
  "m_addGhostMuonsToJets"         : ZHF_config.addGhostMuonsToJets,
  "m_outputAlgo"                  : "Jets_Calibrated_Algo",
  } )

c.algorithm("JetCalibrator", {
  "m_name"                        : "JetCalibrator",
  "m_calibConfigFullSim"          : ZHF_config.PFcalibConfigFullSim,
  "m_calibSequence"               : ZHF_config.calibSequenceMC,
  "m_sort"                        : True,
  "m_inContainerName"             : "AntiKt4EMPFlowJets"+ZHF_config.bJET_timeStamp,
  "m_outContainerName"            : ZHF_config.SmallRJETCalib_outContainer,
  "m_jetAlgo"                     : "AntiKt4EMPFlow",
  "m_redoJVT"                     : ZHF_config.redoJVT,
  "m_calculatefJVT"               : ZHF_config.calculatefJVT,
  "m_fJVTWorkingPoint"            : ZHF_config.WorkingPointfJVT,
  "m_addGhostMuonsToJets"         : ZHF_config.addGhostMuonsToJets,
  "m_outputAlgo"                  : "Jets_SmallRCalibrated_Algo",
  } )

c.algorithm("JetSelector", {
  "m_name"                        : "JetBaseSelector_Fat",
  "m_msgLevel"                    : ZHF_config.msgLevel,
  "m_pT_min"                      : ZHF_config.FatJETBaseSel_pT_min,
  "m_eta_min"                     : ZHF_config.JETBaseSel_eta_min,
  "m_eta_max"                     : ZHF_config.JETBaseSel_eta_max,
  "m_inContainerName"             : ZHF_config.FatJETBaseSel_inContainer,
  "m_outContainerName"            : ZHF_config.FatJETBaseSel_outContainer,
  "m_createSelectedContainer"     : True,
  "m_decorateSelectedObjects"     : True,
  "m_inputAlgo"                   : "Jets_Calibrated_Algo",
  "m_outputAlgo"                  : "Jets_BaseSelected_Algo",
    } )

JVTwps  = ZHF_config.JETBaseSel_JVTwp["AntiKt4EMPFlow"]
nJVTwps = len(JVTwps)
for iwp in range(0,nJVTwps):
  if iwp == 0:
    inContainer = ZHF_config.SmallRJETBaseSel_inContainer
    inAlg       = "Jets_SmallRCalibrated_Algo"
  else:
    inContainer = "JETBaseSel_"+JVTwps[iwp-1]
    inAlg       = "Jets_Selected_Algo_"+JVTwps[iwp-1]
  if iwp == nJVTwps-1:
    outContainer = ZHF_config.SmallRJETBaseSel_outContainer
    outAlg       = "Jets_SmallRSelected_Algo"
  else:
    outContainer = "JETBaseSel_"+JVTwps[iwp]
    outAlg       = "Jets_Selected_Algo_"+JVTwps[iwp]
  c.algorithm("JetSelector", {
    "m_name"                        : "JetBaseSelector_SmallR"+JVTwps[iwp],
    "m_msgLevel"                    : ZHF_config.msgLevel,
    "m_pT_min"                      : ZHF_config.JETBaseSel_pT_min,
    "m_eta_min"                     : ZHF_config.JETBaseSel_eta_min,
    "m_eta_max"                     : ZHF_config.JETBaseSel_eta_max,
    "m_inContainerName"             : inContainer,
    "m_outContainerName"            : outContainer,
    "m_cleanJets"                   : ZHF_config.JETBaseSel_cleanJets,
    "m_cleanEvent"                  : ZHF_config.JETBaseSel_cleanEvent,
    "m_createSelectedContainer"     : True,
    "m_decorateSelectedObjects"     : True,
    "m_doJVT"                       : ZHF_config.JETBaseSel_doJVT,
    "m_noJVTVeto"                   : ZHF_config.JETBaseSel_noJVTVeto,
    "m_WorkingPointJVT"             : JVTwps[iwp],
    "m_SFFileJVT"                   : ZHF_config.JETBaseSel_SFFileJVT+"EMPFlowJets.root",
    "m_dofJVT"                      : ZHF_config.JETBaseSel_dofJVT,
    "m_dofJVTVeto"                  : ZHF_config.JETBaseSel_dofJVTVeto,
    "m_WorkingPointfJVT"            : ZHF_config.WorkingPointfJVT,
    "m_inputAlgo"                   : inAlg,
    "m_outputAlgo"                  : outAlg,
    "m_systValJVT"                  : 1.0,
    "m_systNameJVT"                 : "All",
  } )

c.algorithm("JetSelector", {
  "m_name"                        : "JetBaseSelector_Track",
  "m_msgLevel"                    : ZHF_config.msgLevel,
  "m_pT_min"                      : ZHF_config.TrackbJET_MinPt,
  "m_eta_min"                     : ZHF_config.TrackbJET_MinEta,
  "m_eta_max"                     : ZHF_config.TrackbJET_MaxEta,
  "m_inContainerName"             : "AntiKtVR30Rmax4Rmin02TrackJets_BTagging201903",
  "m_outContainerName"            : ZHF_config.TrackJETBaseSel_outContainer,
  "m_createSelectedContainer"     : True,
  "m_decorateSelectedObjects"     : True,
  "m_outputAlgo"                  : "TrackJets_Selected_Algo",
      "m_systName"                    : "All",
  } )

counter = 0
for tagger in ZHF_config.bJET_TaggerNames_boost:
  c.algorithm("BJetEfficiencyCorrector", {
    "m_name"                        : "BJetEffCorrector_continuous_"+tagger,
    "m_msgLevel"                    : ZHF_config.msgLevel,
    "m_inContainerName"             : ZHF_config.bTrackJET_inContainer,
    "m_jetAuthor"                   : "AntiKtVR30Rmax4Rmin02TrackJets_BTagging201903",
    "m_minPt"                       : ZHF_config.TrackbJET_MinPt,
    "m_decor"                       : ZHF_config.bJET_decor,
    "m_corrFileName"                : ZHF_config.bJET_corrFileName,
    "m_taggerName"                  : tagger,
    "m_operatingPt"                 : ZHF_config.bJET_OperatingPoint,
    "m_coneFlavourLabel"            : ZHF_config.bJET_coneFlavourLabel,
    "m_useDevelopmentFile"          : ZHF_config.bJET_useDevelopmentFile,
    "m_inputAlgo"                   : "Jets_BaseSelected_Algo",
    "m_EfficiencyCalibration"       : ZHF_config.bJET_EfficiencyCalibration,
    "m_setMapIndex"                 : True,
    } )
  for wp in ZHF_config.bJET_FixedOperatingPoints:
    c.algorithm("BJetEfficiencyCorrector", {
      "m_name"                        : "BJetEffCorrector_calibratedWP_"+tagger+"_"+wp,
      "m_msgLevel"                    : ZHF_config.msgLevel,
      "m_inContainerName"             : ZHF_config.bTrackJET_inContainer,
      "m_jetAuthor"                   : "AntiKtVR30Rmax4Rmin02TrackJets_BTagging201903",
      "m_minPt"                       : ZHF_config.TrackbJET_MinPt,
      "m_decor"                       : ZHF_config.TrackbJET_decor,
      "m_corrFileName"                : ZHF_config.bJET_corrFileName,
      "m_taggerName"                  : tagger,
      "m_operatingPt"                 : wp,
      "m_coneFlavourLabel"            : ZHF_config.bJET_coneFlavourLabel,
      "m_useDevelopmentFile"          : ZHF_config.bJET_useDevelopmentFile,
      "m_inputAlgo"                   : "Jets_BaseSelected_Algo",
      "m_EfficiencyCalibration"       : ZHF_config.bJET_EfficiencyCalibration,
      "m_setMapIndex"                 : True,
      "m_systName"                    : "All",
      } )
    counter += 1

counter = 0
for tagger in ZHF_config.bJET_TaggerNames_boost:
  c.algorithm("BJetEfficiencyCorrector", {
    "m_name"                        : "BsmallJetEffCorrector_continuous_"+tagger,
    "m_msgLevel"                    : ZHF_config.msgLevel,
    "m_inContainerName"             : ZHF_config.bSmallJET_inContainer,
    "m_jetAuthor"                   : "AntiKt4EMPFlowJets"+ZHF_config.bJET_timeStamp,
    "m_minPt"                       : ZHF_config.SmallRJET_MinPt,
    "m_decor"                       : ZHF_config.SmallbJET_decor,
    "m_corrFileName"                : ZHF_config.bJET_corrFileName,
    "m_taggerName"                  : tagger,
    "m_operatingPt"                 : ZHF_config.bJET_OperatingPoint,
    "m_coneFlavourLabel"            : ZHF_config.bJET_coneFlavourLabel,
    "m_useDevelopmentFile"          : ZHF_config.bJET_useDevelopmentFile,
    "m_inputAlgo"                   : "Jets_BaseSelected_Algo",
    "m_EfficiencyCalibration"       : ZHF_config.bJET_EfficiencyCalibration,
    "m_setMapIndex"                 : True,
    } )
  for wp in ZHF_config.bJET_FixedOperatingPoints:
    c.algorithm("BJetEfficiencyCorrector", {
      "m_name"                        : "BsmallJetEffCorrector_calibratedWP_"+tagger+"_"+wp,
      "m_msgLevel"                    : ZHF_config.msgLevel,
      "m_inContainerName"             : ZHF_config.bSmallJET_inContainer,
      "m_jetAuthor"                   : "AntiKt4EMPFlowJets"+ZHF_config.bJET_timeStamp,
      "m_minPt"                       : ZHF_config.SmallRJET_MinPt,
      "m_decor"                       : ZHF_config.SmallbJET_decor,
      "m_corrFileName"                : ZHF_config.bJET_corrFileName,
      "m_taggerName"                  : tagger,
      "m_operatingPt"                 : wp,
      "m_coneFlavourLabel"            : ZHF_config.bJET_coneFlavourLabel,
      "m_useDevelopmentFile"          : ZHF_config.bJET_useDevelopmentFile,
      "m_inputAlgo"                   : "Jets_BaseSelected_Algo",
      "m_EfficiencyCalibration"       : ZHF_config.bJET_EfficiencyCalibration,
      "m_setMapIndex"                 : True,
      "m_systName"                    : "All",
      } )
    counter += 1

TrigLegs = set()
TrigLegs.add("2017:"+"_OR_".join(ZHF_config.MUTriggers_2017))
c.algorithm("MuonEfficiencyCorrector", {
  "m_name"                        : "MuonEfficiencyCorrector",
  "m_msgLevel"                    : ZHF_config.msgLevel,
  "m_inContainerName"             : ZHF_config.MUEff_inContainer,
  "m_MuTrigLegs"                  : ",".join(TrigLegs),
  "m_WorkingPointReco"            : ZHF_config.MUEff_WorkingPointReco,
  "m_WorkingPointIso"             : ZHF_config.MUEff_WorkingPointIso_boost,
  "m_WorkingPointTTVA"            : ZHF_config.MUEff_WorkingPointTTVA,
  "m_inputSystNamesMuons"         : "Muons_Selected_Algo",
  "m_usePerMuonTriggerSFs"        : False,
  "m_systNameReco"                : "All",
  "m_systNameIso"                 : "All",
  "m_systNameTrig"                : "All",
  "m_systNameTTVA"                : "All",
  "m_systValReco"                 : 1.0,
  "m_systValIso"                  : 1.0,
  "m_systValTrig"                 : 1.0,
  "m_systValTTVA"                 : 1.0,
  } )

c.algorithm("ElectronEfficiencyCorrector", {
  "m_name"                        : "ElectronEfficiencyCorrector",
  "m_msgLevel"                    : ZHF_config.msgLevel,
  "m_inContainerName"             : ZHF_config.ELEff_inContainer,
  "m_WorkingPointReco"            : ZHF_config.ELEff_WorkingPointReco,
  "m_WorkingPointPID"             : ZHF_config.ELEff_WorkingPointPID,
  "m_WorkingPointIso"             : ZHF_config.ELEff_WorkingPointIso,
  "m_WorkingPointTrig"            : ZHF_config.ELEff_WorkingPointTrig,
  "m_correlationModel"            : ZHF_config.ELEff_CorrelationModel,
  "m_inputSystNamesElectrons"     : "Electrons_Selected_Algo",
  "m_usePerElectronTriggerSFs"    : False,
  "m_systNameReco"                : "All",
  "m_systNamePID"                 : "All",
  "m_systNameIso"                 : "All",
  "m_systNameTrig"                : "All",
  "m_systValReco"                 : 1.0,
  "m_systValPID"                  : 1.0,
  "m_systValIso"                  : 1.0,
  "m_systValTrig"                 : 1.0,
  } )

###########################################################################################
# Temporary (due to missing links to origin containers from the btagging containers)
c.algorithm("JetCalibrator", {
  "m_name"                        : "JetCalibrator_orig",
  "m_calibConfigFullSim"          : ZHF_config.PFcalibConfigFullSim,
  "m_calibSequence"               : ZHF_config.calibSequenceMC,
  "m_sort"                        : True,
  "m_doCleaning"                  : ZHF_config.doCleaning,
  "m_jetCleanCutLevel"            : ZHF_config.jetCleanCutLevel,
  "m_saveAllCleanDecisions"       : False,
  "m_inContainerName"             : "AntiKt4EMPFlowJets",
  "m_outContainerName"            : "JetCalibrator_orig_out",
  "m_jetAlgo"                     : "AntiKt4EMPFlow",
  "m_redoJVT"                     : ZHF_config.redoJVT,
  "m_calculatefJVT"               : ZHF_config.calculatefJVT,
  "m_addGhostMuonsToJets"         : ZHF_config.addGhostMuonsToJets,
  "m_outputAlgo"                  : "JetCalibrator_orig_Algo",
  } )
###########################################################################################

c.algorithm("METConstructor", {
  "m_name"                        : "met",
  "m_msgLevel"                    : ZHF_config.msgLevel,
  "m_referenceMETContainer"       : ZHF_config.referenceMETContainer+"AntiKt4EMPFlow",
  "m_mapName"                     : ZHF_config.MET_mapName+"AntiKt4EMPFlow",
  "m_coreName"                    : ZHF_config.MET_coreName+"AntiKt4EMPFlow",
  "m_outputContainer"             : ZHF_config.MET_outputContainer,
  "m_inputMuons"                  : ZHF_config.MET_inputMuons,
  "m_inputJets"                   : "JetCalibrator_orig_out",
  "m_inputElectrons"              : ZHF_config.MET_inputElectrons,
  "m_doPFlow"                     : True,
  "m_doJVTCut"                    : ZHF_config.MET_doJVTCut,
  "m_dofJVTCut"                   : ZHF_config.MET_dofJVTCut,
  "m_calculateSignificance"       : ZHF_config.MET_calculateSignificance,
  "m_jetSystematics"              : "JetCalibrator_orig_Algo",
  "m_outputAlgoSystNames"         : "MET_Algo",
  "m_muonSystematics"             : "Muons_Selected_Algo",
  "m_eleSystematics"              : "Electrons_Selected_Algo",
  } )

c.algorithm("ZHFSelector", {
  "m_name"                           : "ZHFSelector",
  "m_msgLevel"                       : ZHF_config.msgLevel,
  "m_inContainerName_RecoMuons"      : ZHF_config.MUBaseSel_outContainer,
  "m_inContainerName_RecoElectrons"  : ZHF_config.ELBaseSel_outContainer,
  "m_RecoZmass_min"                  : ZHF_config.ZHFSel_RecoZmass_min,
  "m_RecoZmass_max"                  : ZHF_config.ZHFSel_RecoZmass_max,
  "m_makeTruthSelection"             : False,
  } )

muDetailStr = ZHF_config.muDetailStr_boost
muDetailStr += " TRIG_"
counter = 0
for trigger in ZHF_config.MUTriggers_2017:
  if counter == 0:
    muDetailStr += trigger
  else:
    muDetailStr += "_OR_"+trigger
  counter += 1
fatjetDetailStr = ZHF_config.fatjetDetailStr
trackjetDetailStr = ZHF_config.trackjetDetailStr
smallRjetDetailStr = ZHF_config.smallRjetDetailStr
smallRjetDetailStr += " Truth"
for wp in JVTwps:
  smallRjetDetailStr += " sfJVT"+wp
c.algorithm("ZHFTreeAlgo", {
  "m_name"                        : "ZHFTreeAlgo",
  "m_msgLevel"                    : ZHF_config.msgLevel,
  "m_fatJetContainerName"         : ZHF_config.FinalFatJetContainerName,
  "m_jetContainerName"       : ZHF_config.FinalSmallRJetContainerName,
  "m_trackJetContainerName"       : ZHF_config.FinalTrackJetContainerName,
  "m_METContainerName"            : ZHF_config.FinalMETContainerName,
  "m_evtDetailStr"                : ZHF_config.evtDetailStrMC_NominalSignal,
  "m_fatJetDetailStr"             : fatjetDetailStr,
  "m_jetDetailStr"                : smallRjetDetailStr,
  "m_trackJetDetailStr"           : trackjetDetailStr,
  "m_elContainerName"             : ZHF_config.ELBaseSel_outContainer,
  "m_muContainerName"             : ZHF_config.MUBaseSel_outContainer,
  "m_muDetailStr"                 : muDetailStr,
  "m_elDetailStr"                 : ZHF_config.elDetailStr,
  "m_METDetailStr"                : ZHF_config.metDetailStr,
  "m_trigDetailStr"               : ZHF_config.trigDetailStrMC,
  } )

