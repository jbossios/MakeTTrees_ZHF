import ROOT
from MakeTTrees_ZHF import CommonDefs as ZHF_config
from xAODAnaHelpers import Config     as xAH_config

c = xAH_config()

jetAlgo = "AntiKt4EMTopo"
jetType = jetAlgo.replace("AntiKt4","")

c.algorithm("BasicEventSelection", {
  "m_name"                        : "BasicSelection",
  "m_msgLevel"                    : ZHF_config.msgLevel,
  "m_applyGRLCut"                 : False,
  "m_derivationName"              : "STDM4Kernel",
  "m_useMetaData"                 : True,
  "m_storePassHLT"                : True,
  "m_storeTrigDecisions"          : True,
  "m_applyTriggerCut"             : False,
  "m_extraTriggerSelection"       : ",".join(ZHF_config.ELTriggers_2017+ZHF_config.MUTriggers_2017),
  "m_checkDuplicatesMC"           : True,
  "m_applyJetCleaningEventFlag"   : False,
  "m_applyEventCleaningCut"       : ZHF_config.applyEventCleaningCut,
  "m_PVNTrack"                    : ZHF_config.minPVnTrack,
  "m_applyPrimaryVertexCut"       : ZHF_config.applyPrimaryVertexCut,
  "m_vertexContainerName"         : ZHF_config.vertexContainerName,
  "m_doPUreweighting"             : True,
  "m_lumiCalcFileNames"           : ",".join(ZHF_config.iLumis["MC16d"]),
  "m_PRWFileNames"                : ",".join(ZHF_config.PRWs["MC16d"]),
  "m_doPUreweightingSys"          : True,
  } )

c.algorithm("MuonCalibrator", {
  "m_name"                     : "MuonCalibrator",
  "m_msgLevel"                 : ZHF_config.msgLevel,
  "m_sort"                     : True,
  "m_inContainerName"          : ZHF_config.MUCalib_inContainer,
  "m_outContainerName"         : ZHF_config.MUCalib_outContainer,
  "m_calibrationMode"          : ZHF_config.MUCalib_calibrationMode,
  "m_outputAlgoSystNames"      : "Muons_Calibrated_Algo",
  } )
c.algorithm("MuonSelector", {
  "m_name"                     : "MuonSelector",
  "m_msgLevel"                 : ZHF_config.msgLevel,
  "m_removeEventBadMuon"       : ZHF_config.MUBaseSel_rmEventBadMuon,
  "m_pT_min"                   : ZHF_config.MUBaseSel_pT_min,
  "m_eta_max"                  : ZHF_config.MUBaseSel_eta_max,
  "m_z0sintheta_max"           : ZHF_config.MUBaseSel_z0sintheta_max,
  "m_d0sig_max"                : ZHF_config.MUBaseSel_d0sig_max,
  "m_IsoWPList"                : ZHF_config.MUBaseSel_IsoWPList,
  "m_MinIsoWPCut"              : ZHF_config.MUBaseSel_MinIsoWPCut,
  "m_muonQualityStr"           : ZHF_config.MUBaseSel_Quality,
  "m_inContainerName"          : ZHF_config.MUBaseSel_inContainer,
  "m_outContainerName"         : ZHF_config.MUBaseSel_outContainer,
  "m_singleMuTrigChains"       : ",".join(ZHF_config.MUTriggers_2017),
  "m_inputAlgoSystNames"       : "Muons_Calibrated_Algo",
  "m_outputAlgoSystNames"      : "Muons_Selected_Algo",
  "m_createSelectedContainer"  : True,
  "m_decorateSelectedObjects"  : True,
  } )

c.algorithm("ElectronCalibrator", {
  "m_name"                           : "ElectronCalibrator",
  "m_sort"                           : True,
  "m_esModel"                        : ZHF_config.ELCalib_esModel,
  "m_decorrelationModel"             : ZHF_config.ELCalib_decorrelationModel,
  "m_inContainerName"                : ZHF_config.ELCalib_inContainer,
  "m_outContainerName"               : ZHF_config.ELCalib_outContainer,
  "m_outputAlgoSystNames"            : "Electrons_Calibrated_Algo",
  } )
c.algorithm("ElectronSelector", {
  "m_name"                           : "ElectronSelector",
  "m_msgLevel"                       : ZHF_config.msgLevel,
  "m_pT_min"                         : ZHF_config.ELBaseSel_pT_min,
  "m_eta_max"                        : ZHF_config.ELBaseSel_eta_max,
  "m_z0sintheta_max"                 : ZHF_config.ELBaseSel_z0sintheta_max,
  "m_d0sig_max"                      : ZHF_config.ELBaseSel_d0sig_max,
  "m_IsoWPList"                      : ZHF_config.ELBaseSel_IsoWPList,
  "m_MinIsoWPCut"                    : ZHF_config.ELBaseSel_MinIsoWPCut,
  "m_inContainerName"                : ZHF_config.ELBaseSel_inContainer,
  "m_outContainerName"               : ZHF_config.ELBaseSel_outContainer,
  "m_readIDFlagsFromDerivation"      : ZHF_config.ELBaseSel_readIDFlags,
  "m_doLHPIDcut"                     : ZHF_config.ELBaseSel_doLHPIDcut,
  "m_LHOperatingPoint"               : ZHF_config.ELBaseSel_LHWP,
  "m_vetoCrack"                      : False,
  "m_singleElTrigChains"             : ",".join(ZHF_config.ELTriggers_2017),
  "m_inputAlgoSystNames"             : "Electrons_Calibrated_Algo",
  "m_outputAlgoSystNames"            : "Electrons_Selected_Algo",
  "m_createSelectedContainer"        : True,
  "m_decorateSelectedObjects"        : True,
  } )

c.algorithm("JetCalibrator", {
  "m_name"                        : "JetCalibrator",
  "m_calibConfigFullSim"          : ZHF_config.EMcalibConfigFullSim,
  "m_calibSequence"               : ZHF_config.calibSequenceMC,
  "m_sort"                        : True,
  "m_doCleaning"                  : ZHF_config.doCleaning,
  "m_jetCleanCutLevel"            : ZHF_config.jetCleanCutLevel,
  "m_saveAllCleanDecisions"       : False,
  "m_inContainerName"             : jetAlgo+"Jets"+ZHF_config.bJET_timeStamp,
  "m_outContainerName"            : ZHF_config.JETCalib_outContainer,
  "m_jetAlgo"                     : jetAlgo,
  "m_redoJVT"                     : ZHF_config.redoJVT,
  "m_calculatefJVT"               : ZHF_config.calculatefJVT,
  "m_fJVTWorkingPoint"            : ZHF_config.WorkingPointfJVT,
  "m_addGhostMuonsToJets"         : ZHF_config.addGhostMuonsToJets,
  "m_outputAlgo"                  : "Jets_Calibrated_Algo",
  } )

JVTwps  = ZHF_config.JETBaseSel_JVTwp[jetAlgo]
nJVTwps = len(JVTwps)
for iwp in range(0,nJVTwps):
  if iwp == 0:
    inContainer = ZHF_config.JETBaseSel_inContainer
    inAlg       = "Jets_Calibrated_Algo"
  else:
    inContainer = "JETBaseSel_"+JVTwps[iwp-1]
    inAlg       = "Jets_Selected_Algo_"+JVTwps[iwp-1]
  if iwp == nJVTwps-1:
    outContainer = ZHF_config.JETBaseSel_outContainer
    outAlg       = "Jets_Selected_Algo"
  else:
    outContainer = "JETBaseSel_"+JVTwps[iwp]
    outAlg       = "Jets_Selected_Algo_"+JVTwps[iwp]
  c.algorithm("JetSelector", {
    "m_name"                        : "JetBaseSelector_"+JVTwps[iwp],
    "m_msgLevel"                    : ZHF_config.msgLevel,
    "m_pT_min"                      : ZHF_config.JETBaseSel_pT_min,
    "m_eta_min"                     : ZHF_config.JETBaseSel_eta_min,
    "m_eta_max"                     : ZHF_config.JETBaseSel_eta_max,
    "m_inContainerName"             : inContainer,
    "m_outContainerName"            : outContainer,
    "m_cleanJets"                   : ZHF_config.JETBaseSel_cleanJets,
    "m_cleanEvent"                  : ZHF_config.JETBaseSel_cleanEvent,
    "m_createSelectedContainer"     : True,
    "m_decorateSelectedObjects"     : True,
    "m_doJVT"                       : ZHF_config.JETBaseSel_doJVT,
    "m_noJVTVeto"                   : ZHF_config.JETBaseSel_noJVTVeto,
    "m_WorkingPointJVT"             : JVTwps[iwp],
    "m_SFFileJVT"                   : ZHF_config.JETBaseSel_SFFileJVT+jetType+"Jets.root",
    "m_dofJVT"                      : ZHF_config.JETBaseSel_dofJVT,
    "m_dofJVTVeto"                  : ZHF_config.JETBaseSel_dofJVTVeto,
    "m_WorkingPointfJVT"            : ZHF_config.WorkingPointfJVT,
    "m_inputAlgo"                   : inAlg,
    "m_outputAlgo"                  : outAlg,
    "m_systValJVT"                  : 1.0,
    "m_systNameJVT"                 : "All",
  } )

counter = 0
for tagger in ZHF_config.bJET_TaggerNames:
  c.algorithm("BJetEfficiencyCorrector", {
    "m_name"                        : "BJetEffCorrector_continuous_"+tagger,
    "m_msgLevel"                    : ZHF_config.msgLevel,
    "m_inContainerName"             : ZHF_config.bJET_inContainer,
    "m_jetAuthor"                   : jetAlgo+"Jets"+ZHF_config.bJET_timeStamp,
    "m_minPt"                       : ZHF_config.bJET_MinPt,
    "m_decor"                       : ZHF_config.bJET_decor,
    "m_corrFileName"                : ZHF_config.bJET_corrFileName,
    "m_taggerName"                  : tagger,
    "m_operatingPt"                 : ZHF_config.bJET_OperatingPoint,
    "m_coneFlavourLabel"            : ZHF_config.bJET_coneFlavourLabel,
    "m_useDevelopmentFile"          : ZHF_config.bJET_useDevelopmentFile,
    "m_inputAlgo"                   : "Jets_Selected_Algo",
    "m_EfficiencyCalibration"       : ZHF_config.bJET_EfficiencyCalibration,
    "m_setMapIndex"                 : True,
      "m_systName"                    : "All",
      "m_writeSystToMetadata"         : True,
    } )
  for wp in ZHF_config.bJET_FixedOperatingPoints:
    c.algorithm("BJetEfficiencyCorrector", {
      "m_name"                        : "BJetEffCorrector_calibratedWP_"+tagger+"_"+wp,
      "m_msgLevel"                    : ZHF_config.msgLevel,
      "m_inContainerName"             : ZHF_config.bJET_inContainer,
      "m_jetAuthor"                   : jetAlgo+"Jets"+ZHF_config.bJET_timeStamp,
      "m_minPt"                       : ZHF_config.bJET_MinPt,
      "m_decor"                       : ZHF_config.bJET_decor,
      "m_corrFileName"                : ZHF_config.bJET_corrFileName,
      "m_taggerName"                  : tagger,
      "m_operatingPt"                 : wp,
      "m_coneFlavourLabel"            : ZHF_config.bJET_coneFlavourLabel,
      "m_useDevelopmentFile"          : ZHF_config.bJET_useDevelopmentFile,
      "m_inputAlgo"                   : "Jets_Selected_Algo",
      "m_EfficiencyCalibration"       : ZHF_config.bJET_EfficiencyCalibration,
      "m_setMapIndex"                 : True,
      } )
    counter += 1

c.algorithm("OverlapRemover", {
  "m_name"                        : "OverlapRemover",
  "m_msgLevel"                    : ZHF_config.msgLevel,
  "m_inContainerName_Jets"        : ZHF_config.OR_inJETContainer,
  "m_outContainerName_Jets"       : ZHF_config.OR_outJETContainer,
  "m_inContainerName_Muons"       : ZHF_config.OR_inMUContainer,
  "m_outContainerName_Muons"      : ZHF_config.OR_outMUContainer,
  "m_inContainerName_Electrons"   : ZHF_config.OR_inELContainer,
  "m_outContainerName_Electrons"  : ZHF_config.OR_outELContainer,
  "m_createSelectedContainers"    : True,
  "m_doEleEleOR"                  : True,
  "m_inputAlgoJets"               : "Jets_Selected_Algo",
  "m_inputAlgoMuons"              : "Muons_Selected_Algo",
  "m_inputAlgoElectrons"          : "Electrons_Selected_Algo",
  "m_outputAlgoSystNames"         : "OverlapRemover_Algo",
  "m_useBoostedLeptons"           : ZHF_config.OR_useSlidingDR,
  "m_applyRelPt"                  : ZHF_config.OR_applyRelPt,
  } )

c.algorithm("TruthSelector", {
  "m_name"                        : "TruthMuonSelector",
  "m_msgLevel"                    : ZHF_config.msgLevel,
  "m_typeOptions"                 : ZHF_config.TruthMUSel_types,
  "m_originOptions"               : ZHF_config.TruthMUSel_origins,
  "m_pT_dressed_min"              : ZHF_config.TruthMUSel_pt_dressed_min,
  "m_eta_dressed_min"             : ZHF_config.TruthMUSel_eta_dressed_min,
  "m_eta_dressed_max"             : ZHF_config.TruthMUSel_eta_dressed_max,
  "m_inContainerName"             : ZHF_config.TruthMUSel_inContainer,
  "m_outContainerName"            : ZHF_config.TruthMUSel_outContainer,
  "m_createSelectedContainer"     : True,
  "m_decorateSelectedObjects"     : False,
  } )

c.algorithm("TruthSelector", {
  "m_name"                           : "TruthElectronSelector",
  "m_msgLevel"                       : ZHF_config.msgLevel,
  "m_typeOptions"                    : ZHF_config.TruthELSel_types,
  "m_originOptions"                  : ZHF_config.TruthELSel_origins,
  "m_pT_dressed_min"                 : ZHF_config.TruthELSel_pt_dressed_min,
  "m_eta_dressed_min"                : ZHF_config.TruthELSel_eta_dressed_min,
  "m_eta_dressed_max"                : ZHF_config.TruthELSel_eta_dressed_max,
  "m_inContainerName"                : ZHF_config.TruthELSel_inContainer,
  "m_outContainerName"               : ZHF_config.TruthELSel_outContainer,
  "m_createSelectedContainer"        : True,
  "m_decorateSelectedObjects"        : False,
  } )

c.algorithm("JetSelector", {
  "m_name"                           : "TruthJetSelector",
  "m_msgLevel"                       : ZHF_config.msgLevel,
  "m_pT_min"                         : ZHF_config.TruthJETSel_pT_min,
  "m_inContainerName"                : ZHF_config.TruthJETSel_inContainer,
  "m_outContainerName"               : ZHF_config.TruthJETSel_outContainer,
  "m_createSelectedContainer"        : True,
  "m_decorateSelectedObjects"        : False,
  } )

c.algorithm("ZHFSelector", {
  "m_name"                           : "ZHFSelector",
  "m_msgLevel"                       : ZHF_config.msgLevel,
  "m_inContainerName_RecoMuons"      : ZHF_config.OR_outMUContainer,
  "m_inContainerName_RecoElectrons"  : ZHF_config.OR_outELContainer,
  "m_RecoZmass_min"                  : ZHF_config.ZHFSel_RecoZmass_min,
  "m_RecoZmass_max"                  : ZHF_config.ZHFSel_RecoZmass_max,
  "m_inContainerName_TruthElectrons" : ZHF_config.ZHFSel_inContainer_TruthEL,
  "m_inContainerName_TruthMuons"     : ZHF_config.ZHFSel_inContainer_TruthMU,
  "m_inputAlgoSystNames"             : "OverlapRemover_Algo",
  "m_outputAlgoSystNames"            : "ZHFSelection_Algo",
  } )

TrigLegs = set()
TrigLegs.add("2017:"+"_OR_".join(ZHF_config.MUTriggers_2017))
c.algorithm("MuonEfficiencyCorrector", {
  "m_name"                        : "MuonEfficiencyCorrector",
  "m_msgLevel"                    : ZHF_config.msgLevel,
  "m_inContainerName"             : ZHF_config.MUEff_inContainer,
  "m_MuTrigLegs"                  : ",".join(TrigLegs),
  "m_WorkingPointReco"            : ZHF_config.MUEff_WorkingPointReco,
  "m_WorkingPointIso"             : ZHF_config.MUEff_WorkingPointIso,
  "m_WorkingPointTTVA"            : ZHF_config.MUEff_WorkingPointTTVA,
  "m_inputSystNamesMuons"         : "Muons_Selected_Algo",
  "m_usePerMuonTriggerSFs"        : False,
  "m_systNameReco"                : "All",
  "m_systNameIso"                 : "All",
  "m_systNameTrig"                : "All",
  "m_systNameTTVA"                : "All",
  "m_systValReco"                 : 1.0,
  "m_systValIso"                  : 1.0,
  "m_systValTrig"                 : 1.0,
  "m_systValTTVA"                 : 1.0,
  "m_writeSystToMetadata"         : True,
  } )

c.algorithm("ElectronEfficiencyCorrector", {
  "m_name"                        : "ElectronEfficiencyCorrector",
  "m_msgLevel"                    : ZHF_config.msgLevel,
  "m_inContainerName"             : ZHF_config.ELEff_inContainer,
  "m_WorkingPointReco"            : ZHF_config.ELEff_WorkingPointReco,
  "m_WorkingPointPID"             : ZHF_config.ELEff_WorkingPointPID,
  "m_WorkingPointIso"             : ZHF_config.ELEff_WorkingPointIso,
  "m_WorkingPointTrig"            : ZHF_config.ELEff_WorkingPointTrig,
  "m_correlationModel"            : ZHF_config.ELEff_CorrelationModel,
  "m_inputSystNamesElectrons"     : "Electrons_Selected_Algo",
  "m_usePerElectronTriggerSFs"    : False,
  "m_systNameReco"                : "All",
  "m_systNamePID"                 : "All",
  "m_systNameIso"                 : "All",
  "m_systNameTrig"                : "All",
  "m_systValReco"                 : 1.0,
  "m_systValPID"                  : 1.0,
  "m_systValIso"                  : 1.0,
  "m_systValTrig"                 : 1.0,
  "m_writeSystToMetadata"         : True,
  } )

###########################################################################################
# Temporary (due to missing links to origin containers from the btagging containers)
c.algorithm("JetCalibrator", {
  "m_name"                        : "JetCalibrator_orig",
  "m_calibConfigFullSim"          : ZHF_config.EMcalibConfigFullSim,
  "m_calibSequence"               : ZHF_config.calibSequenceMC,
  "m_sort"                        : True,
  "m_doCleaning"                  : ZHF_config.doCleaning,
  "m_jetCleanCutLevel"            : ZHF_config.jetCleanCutLevel,
  "m_saveAllCleanDecisions"       : False,
  "m_inContainerName"             : jetAlgo+"Jets",
  "m_outContainerName"            : "JetCalibrator_orig_out",
  "m_jetAlgo"                     : jetAlgo,
  "m_redoJVT"                     : ZHF_config.redoJVT,
  "m_calculatefJVT"               : ZHF_config.calculatefJVT,
  "m_addGhostMuonsToJets"         : ZHF_config.addGhostMuonsToJets,
  "m_outputAlgo"                  : "JetCalibrator_orig_Algo",
  } )
###########################################################################################

c.algorithm("METConstructor", {
  "m_name"                        : "met",
  "m_msgLevel"                    : ZHF_config.msgLevel,
  "m_referenceMETContainer"       : ZHF_config.referenceMETContainer+jetAlgo,
  "m_mapName"                     : ZHF_config.MET_mapName+jetAlgo,
  "m_coreName"                    : ZHF_config.MET_coreName+jetAlgo,
  "m_outputContainer"             : ZHF_config.MET_outputContainer,
  "m_inputMuons"                  : ZHF_config.MET_inputMuons,
  "m_inputJets"                   : "JetCalibrator_orig_out",
  "m_inputElectrons"              : ZHF_config.MET_inputElectrons,
  "m_doJVTCut"                    : ZHF_config.MET_doJVTCut,
  "m_dofJVTCut"                   : ZHF_config.MET_dofJVTCut,
  "m_calculateSignificance"       : ZHF_config.MET_calculateSignificance,
  "m_jetSystematics"              : "JetCalibrator_orig_Algo",
  "m_outputAlgoSystNames"         : "MET_Algo",
  "m_muonSystematics"             : "Muons_Selected_Algo",
  "m_eleSystematics"              : "Electrons_Selected_Algo",
  } )

muDetailStr = ZHF_config.muDetailStr
muDetailStr += " TRIG_"
counter = 0
for trigger in ZHF_config.MUTriggers_2017:
  if counter == 0:
    muDetailStr += trigger
  else:
    muDetailStr += "_OR_"+trigger
  counter += 1
jetDetailStr = ZHF_config.jetDetailStr
jetDetailStr += " Truth"
for wp in JVTwps:
  jetDetailStr += " sfJVT"+wp
for tagger in ZHF_config.bJET_TaggerNames:
  jetDetailStr += " BTagging_"+tagger+"_"+ZHF_config.bJET_OperatingPoint
  for wp in ZHF_config.bJET_FixedOperatingPoints:
    jetDetailStr += " jetBTag_"+tagger+"_"+wp

c.algorithm("ZHFTreeAlgo", {
  "m_name"                        : "ZHFTreeAlgo",
  "m_msgLevel"                    : ZHF_config.msgLevel,
  "m_jetContainerName"            : ZHF_config.OR_outJETContainer,
  "m_truthJetContainerName"       : ZHF_config.truthJetContainerName,
  "m_METContainerName"            : ZHF_config.FinalMETContainerName,
  "m_evtDetailStr"                : ZHF_config.evtDetailStrMC_NominalSignal,
  "m_jetDetailStr"                : jetDetailStr,
  "m_elContainerName"             : ZHF_config.OR_outELContainer,
  "m_muContainerName"             : ZHF_config.OR_outMUContainer,
  "m_truthParticlesContainerName" : ZHF_config.truthElectronContainerName+" "+ZHF_config.truthMuonContainerName,
  "m_muDetailStr"                 : muDetailStr,
  "m_elDetailStr"                 : ZHF_config.elDetailStr,
  "m_METDetailStr"                : ZHF_config.metDetailStr,
  "m_truthParticlesDetailStr"     : ZHF_config.truthParticlesDetailStr+"|"+ZHF_config.truthParticlesDetailStr,
  "m_truthParticlesBranchName"    : "truthElectron truthMuon",
  "m_truthJetDetailStr"           : ZHF_config.truthJetDetailStr,
  "m_trigDetailStr"               : ZHF_config.trigDetailStrMC,
  } )

c.algorithm("WeightNamesAlgo", {
  "m_name"                           : "WeightNamesAlgo",
  "m_msgLevel"                       : ZHF_config.msgLevel,
  } )

