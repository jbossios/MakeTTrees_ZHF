##################################################
#
# Purpose: Create every possible config
# Author:  Jona Bossio (jbossios@cern.ch)
# Date:    26 March 2020
#
##################################################

Samples = [
  '2015Data',
  '2016Data',
  '2017Data',
  '2018Data',
  'MC16a',
  'MC16d',
  'MC16e',
]

InputTypes = [
  'STDM3', # DAOD_STDM3
  'STDM4', # DAOD_STDM4
  'PHYS',  # DAOD_PHYS
  'AOD',
]

JetCollections = ['AntiKt10LCTopoTrimmedPtFrac5SmallR20']

TrackJetCollections = ['AntiKtVR30Rmax4Rmin02TrackJets_BTagging201810','AntiKtVR30Rmax4Rmin02TrackJets_BTagging201903']

SystematicTreatments = [
  'NominalOnly',
  'Full',
]

Fix4MissingLinks = True

####################
## Generate configs
####################

# Loop over data/MC samples and analyses
for sample in Samples:
  # Loop over derivation types
  for inputType in InputTypes:
    # Loop over jet collections
    for jetcoll in JetCollections:
      # Loop over different treatments of systematics
      for sysType in SystematicTreatments:
        if 'Data' in sample and sysType == 'Full': continue # skip full systematics in data
        # Name of config
        deriv      = '_'+inputType if inputType != 'STDM3'     else ''
        syst       = '_'+sysType  if sysType  == 'NominalOnly' else ''
        if 'Data' in sample: syst = ''
        extraAnalysis = '_Boosted'
        ConfigName    = 'config_Tree_{}_ZHFJETS{}_{}{}{}.py'.format(sample,extraAnalysis,jetcoll,syst,deriv)
        Config = open(ConfigName,'w')
        ######################
        # Header
        ######################
        Config.write('import ROOT\n')
        Config.write('from MakeTTrees_ZHF import CommonDefs as ZHF_config\n')
        Config.write('from xAODAnaHelpers import Config     as xAH_config\n')
        Config.write('\n')
        Config.write('c = xAH_config()\n')
        Config.write('\n')
        Config.write('jetAlgo = "'+jetcoll+'"\n')
        Config.write('jetType = jetAlgo.replace("AntiKt4","")\n')
        Config.write('\n')
        ######################
        # BasicEventSelection
        ######################
        Config.write('c.algorithm("BasicEventSelection", {\n')
        Config.write('  "m_name"                        : "BasicSelection",\n')
        Config.write('  "m_msgLevel"                    : ZHF_config.msgLevel,\n')
        if 'Data' in sample:
          Config.write('  "m_applyGRLCut"                 : True,\n')
          Config.write('  "m_GRLxml"                      : ZHF_config.GRL_'+sample[0:4]+',\n')
        else:
          Config.write('  "m_applyGRLCut"                 : False,\n')
        Config.write('  "m_derivationName"              : "'+inputType+'Kernel",\n')
        Config.write('  "m_useMetaData"                 : True,\n')
        Config.write('  "m_storePassHLT"                : True,\n')
        Config.write('  "m_storeTrigDecisions"          : True,\n')
        Config.write('  "m_applyTriggerCut"             : True,\n')
        if 'Data' in sample:
          Config.write('  "m_triggerSelection"            : " | ".join(ZHF_config.ELTriggers_'+sample[0:4]+'+ZHF_config.MUTriggers_'+sample[0:4]+'),\n')
          Config.write('  "m_checkDuplicatesData"         : True,\n')
        else:
          if 'MC16a' in sample:
            Config.write('  "m_triggerSelection"            : " | ".join(set(ZHF_config.ELTriggers_2015+ZHF_config.ELTriggers_2016+ZHF_config.MUTriggers_2015+ZHF_config.MUTriggers_2016)),\n')
          elif 'MC16d' in sample:
            Config.write('  "m_triggerSelection"            : " | ".join(ZHF_config.ELTriggers_2017+ZHF_config.MUTriggers_2017),\n')
          elif 'MC16e' in sample:
            Config.write('  "m_triggerSelection"            : " | ".join(ZHF_config.ELTriggers_2018+ZHF_config.MUTriggers_2018),\n')
          Config.write('  "m_checkDuplicatesMC"           : True,\n')
        Config.write('  "m_applyEventCleaningCut"       : ZHF_config.applyEventCleaningCut,\n')
        Config.write('  "m_PVNTrack"                    : ZHF_config.minPVnTrack,\n')
        Config.write('  "m_applyPrimaryVertexCut"       : ZHF_config.applyPrimaryVertexCut,\n')
        Config.write('  "m_vertexContainerName"         : ZHF_config.vertexContainerName,\n')
        if inputType != 'PHYS':
          Config.write('  "m_doPUreweighting"             : True,\n')
          if 'Data' in sample:
            if '2015' in sample or '2016' in sample:
              Config.write('  "m_lumiCalcFileNames"           : ",".join(ZHF_config.iLumis["MC16a"]),\n')
              Config.write('  "m_PRWFileNames"                : ",".join(ZHF_config.PRWs["MC16a"]),\n')
            elif '2017' in sample:
              Config.write('  "m_lumiCalcFileNames"           : ",".join(ZHF_config.iLumis["MC16d"]),\n')
              Config.write('  "m_PRWFileNames"                : ",".join(ZHF_config.PRWs["MC16d"]),\n')
            elif '2018' in sample:
              Config.write('  "m_lumiCalcFileNames"           : ",".join(ZHF_config.iLumis["MC16e"]),\n')
              Config.write('  "m_PRWFileNames"                : ",".join(ZHF_config.PRWs["MC16e"]),\n')
          else: # MC
            Config.write('  "m_lumiCalcFileNames"           : ",".join(ZHF_config.iLumis["'+sample[0:5]+'"]),\n')
            Config.write('  "m_PRWFileNames"                : ",".join(ZHF_config.PRWs["'+sample[0:5]+'"]),\n')
        else: # PHYS
          Config.write('  "m_doPUreweighting"             : False,\n')
          Config.write('  "m_lumiCalcFileNames"           : ",".join(ZHF_config.iLumis["'+sample[0:5]+'"]),\n')
          Config.write('  "m_PRWFileNames"                : "MakeTTrees_ZHF/PRW_PHYS.root",\n')
        if 'Data' in sample:
          if '2015' in sample:
            Config.write('  "m_applyIsBadBatmanFlag"        : ZHF_config.applyIsBadBatmanFlag15,\n')
          elif '2016' in sample:
            Config.write('  "m_applyIsBadBatmanFlag"        : ZHF_config.applyIsBadBatmanFlag16,\n')
          elif '2017' in sample:
            Config.write('  "m_applyIsBadBatmanFlag"        : ZHF_config.applyIsBadBatmanFlag17,\n')
          elif '2018' in sample:
            Config.write('  "m_applyIsBadBatmanFlag"        : ZHF_config.applyIsBadBatmanFlag18,\n')
        Config.write('  } )\n')
        Config.write('\n')
        ######################
        # MuonCalibrator
        ######################
        Config.write('c.algorithm("MuonCalibrator", {\n')
        Config.write('  "m_name"                     : "MuonCalibrator",\n')
        Config.write('  "m_msgLevel"                 : ZHF_config.msgLevel,\n')
        Config.write('  "m_sort"                     : True,\n')
        Config.write('  "m_inContainerName"          : ZHF_config.MUCalib_inContainer,\n')
        Config.write('  "m_outContainerName"         : ZHF_config.MUCalib_outContainer,\n')
        if 'Data' in sample: Config.write('  "m_forceDataCalib"           : ZHF_config.MUCalib_forceDataCalib,\n')
        if '2015' in sample or '2016' in sample or 'MC16a' in sample:
          Config.write('  "m_statComb1516"             : ZHF_config.MUCalib_statComb,\n')
          Config.write('  "m_sagittaCorr1516"          : ZHF_config.MUCalib_sagittaCorr,\n')
          Config.write('  "m_doSagittaMCDistortion1516": ZHF_config.MUCalib_doSagittaMCDistortion,\n')
          Config.write('  "m_sagittaCorrPhaseSpace1516": ZHF_config.MUCalib_sagittaCorrPhaseSpace,\n')
        elif '2017' in sample or 'MC16d' in sample:
          Config.write('  "m_statComb17"               : ZHF_config.MUCalib_statComb,\n')
          Config.write('  "m_sagittaCorr17"            : ZHF_config.MUCalib_sagittaCorr,\n')
          Config.write('  "m_doSagittaMCDistortion17"  : ZHF_config.MUCalib_doSagittaMCDistortion,\n')
          Config.write('  "m_sagittaCorrPhaseSpace17"  : ZHF_config.MUCalib_sagittaCorrPhaseSpace,\n')
        elif '2018' in sample or 'MC16e' in sample:
          Config.write('  "m_statComb18"               : ZHF_config.MUCalib_statComb,\n')
          Config.write('  "m_sagittaCorr18"            : ZHF_config.MUCalib_sagittaCorr,\n')
          Config.write('  "m_doSagittaMCDistortion18"  : ZHF_config.MUCalib_doSagittaMCDistortion,\n')
          Config.write('  "m_sagittaCorrPhaseSpace18"  : ZHF_config.MUCalib_sagittaCorrPhaseSpace,\n')
        Config.write('  "m_outputAlgoSystNames"      : "Muons_Calibrated_Algo",\n')
        if sysType == 'Full':
          Config.write('  "m_systName"                 : "All",\n')
          Config.write('  "m_systVal"                  : 1.0,\n')
        Config.write('  } )\n')
        ######################
        # MuonSelector
        ######################
        Config.write('c.algorithm("MuonSelector", {\n')
        Config.write('  "m_name"                     : "MuonSelector",\n')
        Config.write('  "m_msgLevel"                 : ZHF_config.msgLevel,\n')
        Config.write('  "m_removeEventBadMuon"       : ZHF_config.MUBaseSel_rmEventBadMuon,\n')
        Config.write('  "m_pT_min"                   : ZHF_config.MUBaseSel_pT_min,\n')
        Config.write('  "m_eta_max"                  : ZHF_config.MUBaseSel_eta_max,\n')
        Config.write('  "m_z0sintheta_max"           : ZHF_config.MUBaseSel_z0sintheta_max,\n')
        Config.write('  "m_d0sig_max"                : ZHF_config.MUBaseSel_d0sig_max,\n')
        Config.write('  "m_IsoWPList"                : ZHF_config.MUBaseSel_IsoWPList_boost,\n')
        Config.write('  "m_MinIsoWPCut"              : ZHF_config.MUBaseSel_MinIsoWPCut_boost,\n')
        Config.write('  "m_muonQualityStr"           : ZHF_config.MUBaseSel_Quality,\n')
        Config.write('  "m_inContainerName"          : ZHF_config.MUBaseSel_inContainer,\n')
        Config.write('  "m_outContainerName"         : ZHF_config.MUBaseSel_outContainer,\n')
        if inputType != 'PHYS': # Temporary
          if 'MC16a' in sample:
            Config.write('  "m_singleMuTrigChains"       : ",".join(set(ZHF_config.MUTriggers_2015+ZHF_config.MUTriggers_2016)),\n')
          elif '2015' in sample:
            Config.write('  "m_singleMuTrigChains"       : ",".join(ZHF_config.MUTriggers_2015),\n')
          elif '2016' in sample:
            Config.write('  "m_singleMuTrigChains"       : ",".join(ZHF_config.MUTriggers_2016),\n')
          elif 'MC16d' in sample or '2017' in sample:
            Config.write('  "m_singleMuTrigChains"       : ",".join(ZHF_config.MUTriggers_2017),\n')
          elif 'MC16e' in sample or '2018' in sample:
            Config.write('  "m_singleMuTrigChains"       : ",".join(ZHF_config.MUTriggers_2018),\n')
        Config.write('  "m_inputAlgoSystNames"       : "Muons_Calibrated_Algo",\n')
        Config.write('  "m_outputAlgoSystNames"      : "Muons_Selected_Algo",\n')
        Config.write('  "m_createSelectedContainer"  : True,\n')
        Config.write('  "m_decorateSelectedObjects"  : True,\n')
        Config.write('  } )\n')
        Config.write('\n')
        ######################
        # ElectronCalibrator
        ######################
        Config.write('c.algorithm("ElectronCalibrator", {\n')
        Config.write('  "m_name"                           : "ElectronCalibrator",\n')
        Config.write('  "m_sort"                           : True,\n')
        Config.write('  "m_esModel"                        : ZHF_config.ELCalib_esModel,\n')
        Config.write('  "m_decorrelationModel"             : ZHF_config.ELCalib_decorrelationModel,\n')
        Config.write('  "m_inContainerName"                : ZHF_config.ELCalib_inContainer,\n')
        Config.write('  "m_outContainerName"               : ZHF_config.ELCalib_outContainer,\n')
        Config.write('  "m_outputAlgoSystNames"            : "Electrons_Calibrated_Algo",\n')
        if sysType == 'Full':
          Config.write('  "m_systName"                       : "All",\n')
          Config.write('  "m_systVal"                        : 1.0,\n')
        Config.write('  } )\n')
        ########################
        # ElectronSelector
        ########################
        Config.write('c.algorithm("ElectronSelector", {\n')
        Config.write('  "m_name"                           : "ElectronSelector",\n')
        Config.write('  "m_msgLevel"                       : ZHF_config.msgLevel,\n')
        Config.write('  "m_pT_min"                         : ZHF_config.ELBaseSel_pT_min,\n')
        Config.write('  "m_eta_max"                        : ZHF_config.ELBaseSel_eta_max,\n')
        Config.write('  "m_z0sintheta_max"                 : ZHF_config.ELBaseSel_z0sintheta_max,\n')
        Config.write('  "m_d0sig_max"                      : ZHF_config.ELBaseSel_d0sig_max,\n')
        Config.write('  "m_IsoWPList"                      : ZHF_config.ELBaseSel_IsoWPList,\n')
        Config.write('  "m_MinIsoWPCut"                    : ZHF_config.ELBaseSel_MinIsoWPCut,\n')
        Config.write('  "m_inContainerName"                : ZHF_config.ELBaseSel_inContainer,\n')
        Config.write('  "m_outContainerName"               : ZHF_config.ELBaseSel_outContainer,\n')
        Config.write('  "m_readIDFlagsFromDerivation"      : ZHF_config.ELBaseSel_readIDFlags,\n')
        Config.write('  "m_doLHPIDcut"                     : ZHF_config.ELBaseSel_doLHPIDcut,\n')
        Config.write('  "m_LHOperatingPoint"               : ZHF_config.ELBaseSel_LHWP,\n')
        Config.write('  "m_applyCrackVetoCleaning"         : ZHF_config.ELBaseSel_applyCrackVetoFix,\n')
        Config.write('  "m_vetoCrack"                      : False,\n') # it can be applied at the reader
        if inputType != 'PHYS': # Temporary
          if 'MC16a' in sample:
            Config.write('  "m_singleElTrigChains"             : ",".join(set(ZHF_config.ELTriggers_2015+ZHF_config.ELTriggers_2016)),\n')
          elif '2015' in sample:
            Config.write('  "m_singleElTrigChains"             : ",".join(ZHF_config.ELTriggers_2015),\n')
          elif '2016' in sample:
            Config.write('  "m_singleElTrigChains"             : ",".join(ZHF_config.ELTriggers_2016),\n')
          elif '2017' in sample or 'MC16d' in sample:
            Config.write('  "m_singleElTrigChains"             : ",".join(ZHF_config.ELTriggers_2017),\n')
          elif '2018' in sample or 'MC16e' in sample:
            Config.write('  "m_singleElTrigChains"             : ",".join(ZHF_config.ELTriggers_2018),\n')
        Config.write('  "m_inputAlgoSystNames"             : "Electrons_Calibrated_Algo",\n')
        Config.write('  "m_outputAlgoSystNames"            : "Electrons_Selected_Algo",\n')
        Config.write('  "m_createSelectedContainer"        : True,\n')
        Config.write('  "m_decorateSelectedObjects"        : True,\n')
        Config.write('  } )\n')
        Config.write('\n')
        ########################
        # Large-R JetCalibrator
        ########################
        Config.write('c.algorithm("JetCalibrator", {\n')
        Config.write('  "m_name"                        : "JetCalibrator",\n')
        if 'MC' in sample:
          Config.write('  "m_calibConfigFullSim"          : ZHF_config.FatJetcalibConfigFullSim,\n')
          Config.write('  "m_calibSequence"               : ZHF_config.FatcalibSequenceMC,\n')
        else:
          Config.write('  "m_calibConfigData"             : ZHF_config.FatJetcalibConfigData,\n')
          Config.write('  "m_calibSequence"               : ZHF_config.FatcalibSequenceData,\n')
        Config.write('  "m_sort"                        : True,\n')
        Config.write('  "m_inContainerName"             : jetAlgo+"Jets",\n')
        Config.write('  "m_outContainerName"            : ZHF_config.FatJETCalib_outContainer,\n')
        Config.write('  "m_jetAlgo"                     : jetAlgo,\n')
        Config.write('  "m_addGhostMuonsToJets"         : ZHF_config.addGhostMuonsToJets,\n')
        Config.write('  "m_outputAlgo"                  : "Jets_Calibrated_Algo",\n')
        if sysType == 'Full':
          Config.write('  "m_uncertConfig"                : ZHF_config.FatuncertConfig,\n')
          Config.write('  "m_systName"                    : "All",\n')
          Config.write('  "m_systVal"                     : 1.0,\n')
        Config.write('  } )\n')
        Config.write('\n')

        ########################
        #Small-R jet calibrator
        ########################
        Config.write('c.algorithm("JetCalibrator", {\n')
        Config.write('  "m_name"                        : "JetCalibrator",\n')
        if 'MC' in sample:
          Config.write('  "m_calibConfigFullSim"          : ZHF_config.PFcalibConfigFullSim,\n')
          Config.write('  "m_calibSequence"               : ZHF_config.calibSequenceMC,\n')
        else:
          Config.write('  "m_calibConfigData"             : ZHF_config.PFcalibConfigData,\n')
          Config.write('  "m_calibSequence"               : ZHF_config.calibSequenceData,\n')
        Config.write('  "m_sort"                        : True,\n')
        Config.write('  "m_inContainerName"             : "AntiKt4EMPFlowJets"+ZHF_config.bJET_timeStamp,\n')
        Config.write('  "m_outContainerName"            : ZHF_config.SmallRJETCalib_outContainer,\n')
        Config.write('  "m_jetAlgo"                     : "AntiKt4EMPFlow",\n')
        Config.write('  "m_redoJVT"                     : ZHF_config.redoJVT,\n')
        Config.write('  "m_calculatefJVT"               : ZHF_config.calculatefJVT,\n')
        Config.write('  "m_fJVTWorkingPoint"            : ZHF_config.WorkingPointfJVT,\n')
        Config.write('  "m_addGhostMuonsToJets"         : ZHF_config.addGhostMuonsToJets,\n')
        Config.write('  "m_outputAlgo"                  : "Jets_SmallRCalibrated_Algo",\n')
        if sysType == 'Full':
          Config.write('  "m_uncertConfig"                : ZHF_config.uncertConfig,\n')
          Config.write('  "m_systName"                    : "All",\n')
          Config.write('  "m_systVal"                     : 1.0,\n')
        Config.write('  } )\n')
        Config.write('\n')

        ##############################
        # Large-R jet Base JetSelector
        ##############################
        Config.write('c.algorithm("JetSelector", {\n')
        Config.write('  "m_name"                        : "JetBaseSelector_Fat",\n')
        Config.write('  "m_msgLevel"                    : ZHF_config.msgLevel,\n')
        Config.write('  "m_pT_min"                      : ZHF_config.FatJETBaseSel_pT_min,\n')
        Config.write('  "m_eta_min"                     : ZHF_config.JETBaseSel_eta_min,\n')
        Config.write('  "m_eta_max"                     : ZHF_config.JETBaseSel_eta_max,\n')
        Config.write('  "m_inContainerName"             : ZHF_config.FatJETBaseSel_inContainer,\n')
        Config.write('  "m_outContainerName"            : ZHF_config.FatJETBaseSel_outContainer,\n')
        Config.write('  "m_createSelectedContainer"     : True,\n')
        Config.write('  "m_decorateSelectedObjects"     : True,\n')
        Config.write('  "m_inputAlgo"                   : "Jets_Calibrated_Algo",\n')
        Config.write('  "m_outputAlgo"                  : "Jets_BaseSelected_Algo",\n')
        if 'MC' in sample and inputType == 'PHYS':
           Config.write('  "m_truthJetContainer"           : "AntiKt4TruthDressedWZJets",\n')
        Config.write('    } )\n')
        Config.write('\n')

        ########################
        #Small-R jet JetSelector
        ########################
        Config.write('JVTwps  = ZHF_config.JETBaseSel_JVTwp["AntiKt4EMPFlow"]\n')
        Config.write('nJVTwps = len(JVTwps)\n')
        Config.write('for iwp in range(0,nJVTwps):\n')
        Config.write('  if iwp == 0:\n')
        Config.write('    inContainer = ZHF_config.SmallRJETBaseSel_inContainer\n')
        Config.write('    inAlg       = "Jets_SmallRCalibrated_Algo"\n')
        Config.write('  else:\n')
        Config.write('    inContainer = "JETBaseSel_"+JVTwps[iwp-1]\n')
        Config.write('    inAlg       = "Jets_Selected_Algo_"+JVTwps[iwp-1]\n')
        Config.write('  if iwp == nJVTwps-1:\n')
        Config.write('    outContainer = ZHF_config.SmallRJETBaseSel_outContainer\n')
        Config.write('    outAlg       = "Jets_SmallRSelected_Algo"\n')
        Config.write('  else:\n')
        Config.write('    outContainer = "JETBaseSel_"+JVTwps[iwp]\n')
        Config.write('    outAlg       = "Jets_Selected_Algo_"+JVTwps[iwp]\n')
        Config.write('  c.algorithm("JetSelector", {\n')
        Config.write('    "m_name"                        : "JetBaseSelector_SmallR"+JVTwps[iwp],\n')
        Config.write('    "m_msgLevel"                    : ZHF_config.msgLevel,\n')
        Config.write('    "m_pT_min"                      : ZHF_config.JETBaseSel_pT_min,\n')
        Config.write('    "m_eta_min"                     : ZHF_config.JETBaseSel_eta_min,\n')
        Config.write('    "m_eta_max"                     : ZHF_config.JETBaseSel_eta_max,\n')
        Config.write('    "m_inContainerName"             : inContainer,\n')
        Config.write('    "m_outContainerName"            : outContainer,\n')
        Config.write('    "m_cleanJets"                   : ZHF_config.JETBaseSel_cleanJets,\n')
        Config.write('    "m_cleanEvent"                  : ZHF_config.JETBaseSel_cleanEvent,\n')
        Config.write('    "m_createSelectedContainer"     : True,\n')
        Config.write('    "m_decorateSelectedObjects"     : True,\n')
        Config.write('    "m_doJVT"                       : ZHF_config.JETBaseSel_doJVT,\n')
        Config.write('    "m_noJVTVeto"                   : ZHF_config.JETBaseSel_noJVTVeto,\n')
        Config.write('    "m_WorkingPointJVT"             : JVTwps[iwp],\n')
        Config.write('    "m_SFFileJVT"                   : ZHF_config.JETBaseSel_SFFileJVT+"EMPFlowJets.root",\n')
        Config.write('    "m_dofJVT"                      : ZHF_config.JETBaseSel_dofJVT,\n')
        Config.write('    "m_dofJVTVeto"                  : ZHF_config.JETBaseSel_dofJVTVeto,\n')
        Config.write('    "m_WorkingPointfJVT"            : ZHF_config.WorkingPointfJVT,\n')
        Config.write('    "m_inputAlgo"                   : inAlg,\n')
        Config.write('    "m_outputAlgo"                  : outAlg,\n')
        if 'MC' in sample:
          Config.write('    "m_systValJVT"                  : 1.0,\n')
          Config.write('    "m_systNameJVT"                 : "All",\n')
        Config.write('  } )\n')
        Config.write('\n')

        ##########################
        # Track-jet selector
        ##########################
        Config.write('c.algorithm("JetSelector", {\n')
        Config.write('  "m_name"                        : "JetBaseSelector_Track",\n')
        Config.write('  "m_msgLevel"                    : ZHF_config.msgLevel,\n')
        Config.write('  "m_pT_min"                      : ZHF_config.TrackbJET_MinPt,\n')
        Config.write('  "m_eta_min"                     : ZHF_config.TrackbJET_MinEta,\n')
        Config.write('  "m_eta_max"                     : ZHF_config.TrackbJET_MaxEta,\n')
        Config.write('  "m_inContainerName"             : "AntiKtVR30Rmax4Rmin02TrackJets_BTagging201903",\n')
        Config.write('  "m_outContainerName"            : ZHF_config.TrackJETBaseSel_outContainer,\n')
        Config.write('  "m_createSelectedContainer"     : True,\n')
        Config.write('  "m_decorateSelectedObjects"     : True,\n')
        Config.write('  "m_outputAlgo"                  : "TrackJets_Selected_Algo",\n')
        if 'MC' in sample:
          Config.write('      "m_systName"                    : "All",\n')
        Config.write('  } )\n')
        Config.write('\n')

        ##########################
        # BJetEfficiencyCorrector
        ##########################
        Config.write('counter = 0\n')
        Config.write('for tagger in ZHF_config.bJET_TaggerNames_boost:\n')
        Config.write('  c.algorithm("BJetEfficiencyCorrector", {\n')
        Config.write('    "m_name"                        : "BJetEffCorrector_continuous_"+tagger,\n')
        Config.write('    "m_msgLevel"                    : ZHF_config.msgLevel,\n')
        Config.write('    "m_inContainerName"             : ZHF_config.bTrackJET_inContainer,\n')
        Config.write('    "m_jetAuthor"                   : "AntiKtVR30Rmax4Rmin02TrackJets_BTagging201903",\n')
        Config.write('    "m_minPt"                       : ZHF_config.TrackbJET_MinPt,\n')
        Config.write('    "m_decor"                       : ZHF_config.bJET_decor,\n')
        Config.write('    "m_corrFileName"                : ZHF_config.bJET_corrFileName,\n')
        Config.write('    "m_taggerName"                  : tagger,\n')
        Config.write('    "m_operatingPt"                 : ZHF_config.bJET_OperatingPoint,\n')
        Config.write('    "m_coneFlavourLabel"            : ZHF_config.bJET_coneFlavourLabel,\n')
        Config.write('    "m_useDevelopmentFile"          : ZHF_config.bJET_useDevelopmentFile,\n')
        Config.write('    "m_inputAlgo"                   : "Jets_BaseSelected_Algo",\n')
        Config.write('    "m_EfficiencyCalibration"       : ZHF_config.bJET_EfficiencyCalibration,\n')
        Config.write('    "m_setMapIndex"                 : True,\n')
        Config.write('    } )\n')
        #calibrated WP
        Config.write('  for wp in ZHF_config.bJET_FixedOperatingPoints:\n')
        Config.write('    c.algorithm("BJetEfficiencyCorrector", {\n')
        Config.write('      "m_name"                        : "BJetEffCorrector_calibratedWP_"+tagger+"_"+wp,\n')
        Config.write('      "m_msgLevel"                    : ZHF_config.msgLevel,\n')
        Config.write('      "m_inContainerName"             : ZHF_config.bTrackJET_inContainer,\n')
        Config.write('      "m_jetAuthor"                   : "AntiKtVR30Rmax4Rmin02TrackJets_BTagging201903",\n')
        Config.write('      "m_minPt"                       : ZHF_config.TrackbJET_MinPt,\n')
        Config.write('      "m_decor"                       : ZHF_config.TrackbJET_decor,\n')
        Config.write('      "m_corrFileName"                : ZHF_config.bJET_corrFileName,\n')
        Config.write('      "m_taggerName"                  : tagger,\n')
        Config.write('      "m_operatingPt"                 : wp,\n')
        Config.write('      "m_coneFlavourLabel"            : ZHF_config.bJET_coneFlavourLabel,\n')
        Config.write('      "m_useDevelopmentFile"          : ZHF_config.bJET_useDevelopmentFile,\n')
        Config.write('      "m_inputAlgo"                   : "Jets_BaseSelected_Algo",\n')
        Config.write('      "m_EfficiencyCalibration"       : ZHF_config.bJET_EfficiencyCalibration,\n')
        Config.write('      "m_setMapIndex"                 : True,\n')
        if 'MC' in sample:
          Config.write('      "m_systName"                    : "All",\n')
        Config.write('      } )\n')
        Config.write('    counter += 1\n')
        Config.write('\n')

        ####################
        #small-R b-jet calib
        ####################
        Config.write('counter = 0\n')
        Config.write('for tagger in ZHF_config.bJET_TaggerNames_boost:\n')
        Config.write('  c.algorithm("BJetEfficiencyCorrector", {\n')
        Config.write('    "m_name"                        : "BsmallJetEffCorrector_continuous_"+tagger,\n')
        Config.write('    "m_msgLevel"                    : ZHF_config.msgLevel,\n')
        Config.write('    "m_inContainerName"             : ZHF_config.bSmallJET_inContainer,\n')
        Config.write('    "m_jetAuthor"                   : "AntiKt4EMPFlowJets"+ZHF_config.bJET_timeStamp,\n')
        Config.write('    "m_minPt"                       : ZHF_config.SmallRJET_MinPt,\n')
        Config.write('    "m_decor"                       : ZHF_config.SmallbJET_decor,\n')
        Config.write('    "m_corrFileName"                : ZHF_config.bJET_corrFileName,\n')
        Config.write('    "m_taggerName"                  : tagger,\n')
        Config.write('    "m_operatingPt"                 : ZHF_config.bJET_OperatingPoint,\n')
        Config.write('    "m_coneFlavourLabel"            : ZHF_config.bJET_coneFlavourLabel,\n')
        Config.write('    "m_useDevelopmentFile"          : ZHF_config.bJET_useDevelopmentFile,\n')
        Config.write('    "m_inputAlgo"                   : "Jets_BaseSelected_Algo",\n')
        Config.write('    "m_EfficiencyCalibration"       : ZHF_config.bJET_EfficiencyCalibration,\n')
        Config.write('    "m_setMapIndex"                 : True,\n')
        Config.write('    } )\n')
        #calibrated WP
        Config.write('  for wp in ZHF_config.bJET_FixedOperatingPoints:\n')
        Config.write('    c.algorithm("BJetEfficiencyCorrector", {\n')
        Config.write('      "m_name"                        : "BsmallJetEffCorrector_calibratedWP_"+tagger+"_"+wp,\n')
        Config.write('      "m_msgLevel"                    : ZHF_config.msgLevel,\n')
        Config.write('      "m_inContainerName"             : ZHF_config.bSmallJET_inContainer,\n')
        Config.write('      "m_jetAuthor"                   : "AntiKt4EMPFlowJets"+ZHF_config.bJET_timeStamp,\n')
        Config.write('      "m_minPt"                       : ZHF_config.SmallRJET_MinPt,\n')
        Config.write('      "m_decor"                       : ZHF_config.SmallbJET_decor,\n')
        Config.write('      "m_corrFileName"                : ZHF_config.bJET_corrFileName,\n')
        Config.write('      "m_taggerName"                  : tagger,\n')
        Config.write('      "m_operatingPt"                 : wp,\n')
        Config.write('      "m_coneFlavourLabel"            : ZHF_config.bJET_coneFlavourLabel,\n')
        Config.write('      "m_useDevelopmentFile"          : ZHF_config.bJET_useDevelopmentFile,\n')
        Config.write('      "m_inputAlgo"                   : "Jets_BaseSelected_Algo",\n')
        Config.write('      "m_EfficiencyCalibration"       : ZHF_config.bJET_EfficiencyCalibration,\n')
        Config.write('      "m_setMapIndex"                 : True,\n')
        if 'MC' in sample:
          Config.write('      "m_systName"                    : "All",\n')
        Config.write('      } )\n')
        Config.write('    counter += 1\n')
        Config.write('\n')

        ##########################
        # TruthMuonSelector
        ##########################
        if 'MC' in sample and inputType != 'AOD':
          Config.write('c.algorithm("TruthSelector", {\n')
          Config.write('  "m_name"                        : "TruthMuonSelector",\n')
          Config.write('  "m_msgLevel"                    : ZHF_config.msgLevel,\n')
          Config.write('  "m_type"                        : ZHF_config.TruthMUSel_type,\n')
          Config.write('  "m_originOptions"               : ZHF_config.TruthMUSel_origins,\n')
          Config.write('  "m_pT_dressed_min"              : ZHF_config.TruthMUSel_pt_dressed_min,\n')
          Config.write('  "m_eta_dressed_min"             : ZHF_config.TruthMUSel_eta_dressed_min,\n')
          Config.write('  "m_eta_dressed_max"             : ZHF_config.TruthMUSel_eta_dressed_max,\n')
          if inputType != 'PHYS':
            Config.write('  "m_inContainerName"             : ZHF_config.TruthMUSel_inContainer,\n')
            Config.write('  "m_outContainerName"            : ZHF_config.TruthMUSel_outContainer,\n')
          else: # DAOD_PHYS
            Config.write('  "m_inContainerName"             : "TruthMuons",\n')
            Config.write('  "m_outContainerName"            : "TruthMuons_selected",\n')
          Config.write('  "m_createSelectedContainer"     : True,\n')
          Config.write('  "m_decorateSelectedObjects"     : False,\n')
          Config.write('  } )\n')
          Config.write('\n')
        ##########################
        # TruthElectronSelector
        ##########################
        if 'MC' in sample and inputType != 'AOD':
          Config.write('c.algorithm("TruthSelector", {\n')
          Config.write('  "m_name"                           : "TruthElectronSelector",\n')
          Config.write('  "m_msgLevel"                       : ZHF_config.msgLevel,\n')
          Config.write('  "m_type"                           : ZHF_config.TruthELSel_type,\n')
          Config.write('  "m_originOptions"                  : ZHF_config.TruthELSel_origins,\n')
          Config.write('  "m_pT_dressed_min"                 : ZHF_config.TruthELSel_pt_dressed_min,\n')
          Config.write('  "m_eta_dressed_min"                : ZHF_config.TruthELSel_eta_dressed_min,\n')
          Config.write('  "m_eta_dressed_max"                : ZHF_config.TruthELSel_eta_dressed_max,\n')
          if inputType != 'PHYS':
            Config.write('  "m_inContainerName"                : ZHF_config.TruthELSel_inContainer,\n')
            Config.write('  "m_outContainerName"               : ZHF_config.TruthELSel_outContainer,\n')
          else: # DAOD_PHYS
            Config.write('  "m_inContainerName"                : "TruthElectrons",\n')
            Config.write('  "m_outContainerName"               : "TruthElectrons_selected",\n')
          Config.write('  "m_createSelectedContainer"        : True,\n')
          Config.write('  "m_decorateSelectedObjects"        : False,\n')
          Config.write('  } )\n')
          Config.write('\n')
        ##########################
        # Large-R TruthJetSelector
        ##########################
        if 'MC' in sample and inputType != 'AOD':
          Config.write('c.algorithm("JetSelector", {\n')
          Config.write('  "m_name"                           : "TruthJetSelector",\n')
          Config.write('  "m_msgLevel"                       : ZHF_config.msgLevel,\n')
          Config.write('  "m_pT_min"                      : ZHF_config.FatTruthJETSel_pT_min,\n')
          if inputType != 'PHYS':
            Config.write('  "m_inContainerName"           : ZHF_config.TruthJETSel_inContainer,\n')
          else: # DAOD_PHYS
            Config.write('  "m_inContainerName"             : "AntiKt4TruthDressedWZJets",\n')
          Config.write('  "m_outContainerName"               : ZHF_config.FatTruthJETSel_outContainer,\n')
          Config.write('  "m_createSelectedContainer"        : True,\n')
          Config.write('  "m_decorateSelectedObjects"        : False,\n')
          Config.write('  } )\n')
          Config.write('\n')

        ###############################
        # VR track-jet TruthJetSelector
        ###############################
        if 'MC' in sample and inputType != 'AOD':
          Config.write('c.algorithm("JetSelector", {\n')
          Config.write('  "m_name"                           : "TruthJetSelector",\n')
          Config.write('  "m_msgLevel"                       : ZHF_config.msgLevel,\n')
          Config.write('  "m_pT_min"                      : ZHF_config.TrackTruthJETSel_pT_min,\n')
          if inputType != 'PHYS':
            Config.write('  "m_inContainerName"           : ZHF_config.TrackTruthJETSel_inContainer,\n')
          Config.write('  "m_outContainerName"               : ZHF_config.TrackTruthJETSel_outContainer,\n')
          Config.write('  "m_createSelectedContainer"        : True,\n')
          Config.write('  "m_decorateSelectedObjects"        : False,\n')
          Config.write('  } )\n')
          Config.write('\n')

        ###############################
        # small-R TruthJetSelector
        ###############################
        if 'MC' in sample and inputType != 'AOD':
          Config.write('c.algorithm("JetSelector", {\n')
          Config.write('  "m_name"                           : "TruthJetSelector",\n')
          Config.write('  "m_msgLevel"                       : ZHF_config.msgLevel,\n')
          Config.write('  "m_pT_min"                         : ZHF_config.TruthJETSel_pT_min,\n')
          if inputType != 'PHYS':
            Config.write('  "m_inContainerName"                : ZHF_config.TruthJETSel_inContainer,\n')
          Config.write('  "m_outContainerName"               : ZHF_config.TruthJETSel_outContainer,\n')
          Config.write('  "m_createSelectedContainer"        : True,\n')
          Config.write('  "m_decorateSelectedObjects"        : False,\n')
          Config.write('  } )\n')
          Config.write('\n')

        ##########################
        # MuonEfficiencyCorrector
        ##########################
        if 'MC' in sample and inputType != 'PHYS': # Temporary
          Config.write('TrigLegs = set()\n')
          if 'MC16a' in sample:
            Config.write('TrigLegs.add("2015:"+"_OR_".join(ZHF_config.MUTriggers_2015))\n')
            Config.write('TrigLegs.add("2016:"+"_OR_".join(ZHF_config.MUTriggers_2016))\n')
          elif 'MC16d' in sample:
            Config.write('TrigLegs.add("2017:"+"_OR_".join(ZHF_config.MUTriggers_2017))\n')
          elif 'MC16e' in sample:
            Config.write('TrigLegs.add("2018:"+"_OR_".join(ZHF_config.MUTriggers_2017))\n')
          Config.write('c.algorithm("MuonEfficiencyCorrector", {\n')
          Config.write('  "m_name"                        : "MuonEfficiencyCorrector",\n')
          Config.write('  "m_msgLevel"                    : ZHF_config.msgLevel,\n')
          Config.write('  "m_inContainerName"             : ZHF_config.MUEff_inContainer,\n')
          Config.write('  "m_MuTrigLegs"                  : ",".join(TrigLegs),\n')
          Config.write('  "m_WorkingPointReco"            : ZHF_config.MUEff_WorkingPointReco,\n')
          Config.write('  "m_WorkingPointIso"             : ZHF_config.MUEff_WorkingPointIso_boost,\n')
          Config.write('  "m_WorkingPointTTVA"            : ZHF_config.MUEff_WorkingPointTTVA,\n')
          Config.write('  "m_inputSystNamesMuons"         : "Muons_Selected_Algo",\n')
          Config.write('  "m_usePerMuonTriggerSFs"        : False,\n') # properly calculate single muon trigger SF (using all muons), same value is saved for every muon
          Config.write('  "m_systNameReco"                : "All",\n')
          Config.write('  "m_systNameIso"                 : "All",\n')
          Config.write('  "m_systNameTrig"                : "All",\n')
          Config.write('  "m_systNameTTVA"                : "All",\n')
          Config.write('  "m_systValReco"                 : 1.0,\n')
          Config.write('  "m_systValIso"                  : 1.0,\n')
          Config.write('  "m_systValTrig"                 : 1.0,\n')
          Config.write('  "m_systValTTVA"                 : 1.0,\n')
          Config.write('  } )\n')
          Config.write('\n')
        ##############################
        # ElectronEfficiencyCorrector
        ##############################
        if 'MC' in sample and inputType != 'PHYS': # Temporary
          Config.write('c.algorithm("ElectronEfficiencyCorrector", {\n')
          Config.write('  "m_name"                        : "ElectronEfficiencyCorrector",\n')
          Config.write('  "m_msgLevel"                    : ZHF_config.msgLevel,\n')
          Config.write('  "m_inContainerName"             : ZHF_config.ELEff_inContainer,\n')
          Config.write('  "m_WorkingPointReco"            : ZHF_config.ELEff_WorkingPointReco,\n')
          Config.write('  "m_WorkingPointPID"             : ZHF_config.ELEff_WorkingPointPID,\n')
          Config.write('  "m_WorkingPointIso"             : ZHF_config.ELEff_WorkingPointIso,\n')
          Config.write('  "m_WorkingPointTrig"            : ZHF_config.ELEff_WorkingPointTrig,\n')
          Config.write('  "m_correlationModel"            : ZHF_config.ELEff_CorrelationModel,\n')
          Config.write('  "m_inputSystNamesElectrons"     : "Electrons_Selected_Algo",\n')
	  Config.write('  "m_usePerElectronTriggerSFs"    : False,\n') # properly calculate single electron trigger SF (using all leptons), same value is saved for every electron
          Config.write('  "m_systNameReco"                : "All",\n')
          Config.write('  "m_systNamePID"                 : "All",\n')
          Config.write('  "m_systNameIso"                 : "All",\n')
          Config.write('  "m_systNameTrig"                : "All",\n')
          Config.write('  "m_systValReco"                 : 1.0,\n')
          Config.write('  "m_systValPID"                  : 1.0,\n')
          Config.write('  "m_systValIso"                  : 1.0,\n')
          Config.write('  "m_systValTrig"                 : 1.0,\n')
          Config.write('  } )\n')
          Config.write('\n')

        ##########################
        # Fix for missing links
        ##########################
        if Fix4MissingLinks:
          Config.write('###########################################################################################\n')
          Config.write('# Temporary (due to missing links to origin containers from the btagging containers)\n')
          Config.write('c.algorithm("JetCalibrator", {\n')
          Config.write('  "m_name"                        : "JetCalibrator_orig",\n')
          if 'MC' in sample:
            Config.write('  "m_calibConfigFullSim"          : ZHF_config.PFcalibConfigFullSim,\n')
          else:
            Config.write('  "m_calibConfigData"             : ZHF_config.PFcalibConfigData,\n')
          if 'MC' in sample:
            Config.write('  "m_calibSequence"               : ZHF_config.calibSequenceMC,\n')
          else: # data
            Config.write('  "m_calibSequence"               : ZHF_config.calibSequenceData,\n')
          Config.write('  "m_sort"                        : True,\n')
          Config.write('  "m_doCleaning"                  : ZHF_config.doCleaning,\n')
          Config.write('  "m_jetCleanCutLevel"            : ZHF_config.jetCleanCutLevel,\n')
          Config.write('  "m_saveAllCleanDecisions"       : False,\n')
          Config.write('  "m_inContainerName"             : "AntiKt4EMPFlowJets",\n')
          Config.write('  "m_outContainerName"            : "JetCalibrator_orig_out",\n')
          Config.write('  "m_jetAlgo"                     : "AntiKt4EMPFlow",\n')
          Config.write('  "m_redoJVT"                     : ZHF_config.redoJVT,\n')
          Config.write('  "m_calculatefJVT"               : ZHF_config.calculatefJVT,\n')
          Config.write('  "m_addGhostMuonsToJets"         : ZHF_config.addGhostMuonsToJets,\n')
          Config.write('  "m_outputAlgo"                  : "JetCalibrator_orig_Algo",\n')
          if sysType == 'Full':
            Config.write('  "m_systName"                    : "All",\n')
            Config.write('  "m_systVal"                     : 1.0,\n')
          Config.write('  } )\n')
          Config.write('###########################################################################################\n')
          Config.write('\n')

        ##########################                                                                     
        # METConstructor                                                                              
        ########################## 
        Config.write('c.algorithm("METConstructor", {\n')
        Config.write('  "m_name"                        : "met",\n')
        Config.write('  "m_msgLevel"                    : ZHF_config.msgLevel,\n')
        Config.write('  "m_referenceMETContainer"       : ZHF_config.referenceMETContainer+"AntiKt4EMPFlow",\n')
        Config.write('  "m_mapName"                     : ZHF_config.MET_mapName+"AntiKt4EMPFlow",\n')
        Config.write('  "m_coreName"                    : ZHF_config.MET_coreName+"AntiKt4EMPFlow",\n')
        Config.write('  "m_outputContainer"             : ZHF_config.MET_outputContainer,\n')
        Config.write('  "m_inputMuons"                  : ZHF_config.MET_inputMuons,\n')
        if Fix4MissingLinks:
          Config.write('  "m_inputJets"                   : "JetCalibrator_orig_out",\n')
        else:
          Config.write('  "m_inputJets"                   : ZHF_config.MET_boostinputJets,\n')
        Config.write('  "m_inputElectrons"              : ZHF_config.MET_inputElectrons,\n')
        Config.write('  "m_doPFlow"                     : True,\n')
        Config.write('  "m_doJVTCut"                    : ZHF_config.MET_doJVTCut,\n')
        Config.write('  "m_dofJVTCut"                   : ZHF_config.MET_dofJVTCut,\n')
        Config.write('  "m_calculateSignificance"       : ZHF_config.MET_calculateSignificance,\n')
        if Fix4MissingLinks:
          Config.write('  "m_jetSystematics"              : "JetCalibrator_orig_Algo",\n')
        else:
          Config.write('  "m_jetSystematics"            : "Jets_SmallRSelected_Algo",\n')
        Config.write('  "m_outputAlgoSystNames"         : "MET_Algo",\n')
        Config.write('  "m_muonSystematics"             : "Muons_Selected_Algo",\n')
        Config.write('  "m_eleSystematics"              : "Electrons_Selected_Algo",\n')
        if sysType == 'Full':
          Config.write('  "m_runNominal"                  : False,\n')
        Config.write('  } )\n')
        Config.write('\n')

        ##########################
        # ZHFSelector
        ##########################
        Config.write('c.algorithm("ZHFSelector", {\n')
        Config.write('  "m_name"                           : "ZHFSelector",\n')
        Config.write('  "m_msgLevel"                       : ZHF_config.msgLevel,\n')
        Config.write('  "m_inContainerName_RecoMuons"      : ZHF_config.MUBaseSel_outContainer,\n')
        Config.write('  "m_inContainerName_RecoElectrons"  : ZHF_config.ELBaseSel_outContainer,\n')
        Config.write('  "m_RecoZmass_min"                  : ZHF_config.ZHFSel_RecoZmass_min,\n')
        Config.write('  "m_RecoZmass_max"                  : ZHF_config.ZHFSel_RecoZmass_max,\n')
        if 'MC' in sample:
          if inputType != 'AOD':
            Config.write('  "m_inContainerName_TruthJets"      : ZHF_config.ZHFSel_inContainer_TruthFatJETS,\n')
            if inputType != 'PHYS':
              Config.write('  "m_inContainerName_TruthElectrons" : ZHF_config.ZHFSel_inContainer_TruthEL,\n')
              Config.write('  "m_inContainerName_TruthMuons"     : ZHF_config.ZHFSel_inContainer_TruthMU,\n')
            else: # DAOD_PHYS
              Config.write('  "m_inContainerName_TruthElectrons" : "TruthElectrons_selected",\n')
              Config.write('  "m_inContainerName_TruthMuons"     : "TruthMuons_selected",\n')
          else: # AOD
            Config.write('  "m_makeTruthSelection"             : False,\n')
        Config.write('  } )\n')
        Config.write('\n')

        ##########################
        # ZHFTreeAlgo
        ##########################
        # Add trigger SFs
        if 'MC' in sample:
          Config.write('muDetailStr = ZHF_config.muDetailStr_boost\n')
          if '2015' in sample or 'MC16a' in sample:
            Config.write('muDetailStr += " TRIG_"\n')
            Config.write('counter = 0\n')
            Config.write('for trigger in ZHF_config.MUTriggers_2015:\n')
            Config.write('  if counter == 0:\n')
            Config.write('    muDetailStr += trigger\n')
            Config.write('  else:\n')
            Config.write('    muDetailStr += "_OR_"+trigger\n')
            Config.write('  counter += 1\n')
          if '2016' in sample or 'MC16a' in sample:
            Config.write('muDetailStr += " TRIG_"\n')
            Config.write('counter = 0\n')
            Config.write('for trigger in ZHF_config.MUTriggers_2016:\n')
            Config.write('  if counter == 0:\n')
            Config.write('    muDetailStr += trigger\n')
            Config.write('  else:\n')
            Config.write('    muDetailStr += "_OR_"+trigger\n')
            Config.write('  counter += 1\n')
          if '2017' in sample or 'MC16d' in sample:
            Config.write('muDetailStr += " TRIG_"\n')
            Config.write('counter = 0\n')
            Config.write('for trigger in ZHF_config.MUTriggers_2017:\n')
            Config.write('  if counter == 0:\n')
            Config.write('    muDetailStr += trigger\n')
            Config.write('  else:\n')
            Config.write('    muDetailStr += "_OR_"+trigger\n')
            Config.write('  counter += 1\n')
          if '2018' in sample or 'MC16e' in sample:
            Config.write('muDetailStr += " TRIG_"\n')
            Config.write('counter = 0\n')
            Config.write('for trigger in ZHF_config.MUTriggers_2018:\n')
            Config.write('  if counter == 0:\n')
            Config.write('    muDetailStr += trigger\n')
            Config.write('  else:\n')
            Config.write('    muDetailStr += "_OR_"+trigger\n')
            Config.write('  counter += 1\n')
	# Add jet details
        Config.write('fatjetDetailStr = ZHF_config.fatjetDetailStr\n')
        Config.write('trackjetDetailStr = ZHF_config.trackjetDetailStr\n')
        Config.write('smallRjetDetailStr = ZHF_config.smallRjetDetailStr\n')
        if 'MC' in sample:
          Config.write('smallRjetDetailStr += " Truth"\n')
        Config.write('for wp in JVTwps:\n')
        Config.write('  smallRjetDetailStr += " sfJVT"+wp\n')
	# Configure ZHFTreeAlgo
        Config.write('c.algorithm("ZHFTreeAlgo", {\n')
        Config.write('  "m_name"                        : "ZHFTreeAlgo",\n')
        Config.write('  "m_msgLevel"                    : ZHF_config.msgLevel,\n')
        Config.write('  "m_fatJetContainerName"         : ZHF_config.FinalFatJetContainerName,\n')
        if 'MC' in sample and sysType == 'Full':
          Config.write('  "m_fatJetSystsVec"                 : "Jets_BaseSelected_Algo",\n')
        Config.write('  "m_jetContainerName"       : ZHF_config.FinalSmallRJetContainerName,\n')
        Config.write('  "m_trackJetContainerName"       : ZHF_config.FinalTrackJetContainerName,\n')
        if 'MC' in sample and sysType == 'Full':
          Config.write('  "m_jetSystsVec"                 : "Jets_SmallRSelected_Algo",\n')
          #          Config.write('  "m_trackjetSystsVec"            : "TrackJets_Selected_Algo",\n')
          Config.write('  "m_muSystsVec"                  : "Muons_Selected_Algo",\n')
          Config.write('  "m_elSystsVec"                  : "Electrons_Selected_Algo",\n')
        Config.write('  "m_METContainerName"            : ZHF_config.FinalMETContainerName,\n')
        if 'MC' in sample:
          Config.write('  "m_evtDetailStr"                : ZHF_config.evtDetailStrMC_NominalSignal,\n')
        else:
          Config.write('  "m_evtDetailStr"                : ZHF_config.evtDetailStrData,\n')
        if 'MC' in sample:
          if inputType != 'AOD': 
            Config.write('  "m_truthJetContainerName"       : ZHF_config.truthSmallRJetContainerName+" "+ZHF_config.truthTrackJetContainerName+" "+ZHF_config.truthFatJetContainerName,\n')
            Config.write('  "m_truthJetBranchName"    : "truthJet truth_trackjet truth_fatjet",\n')
        Config.write('  "m_fatJetDetailStr"             : fatjetDetailStr,\n')
        Config.write('  "m_jetDetailStr"                : smallRjetDetailStr,\n')
        Config.write('  "m_trackJetDetailStr"           : trackjetDetailStr,\n')
        Config.write('  "m_elContainerName"             : ZHF_config.ELBaseSel_outContainer,\n')
        Config.write('  "m_muContainerName"             : ZHF_config.MUBaseSel_outContainer,\n')
        if 'MC' in sample:
          if inputType != 'PHYS' and inputType != 'AOD':
            Config.write('  "m_truthParticlesContainerName" : ZHF_config.truthElectronContainerName+" "+ZHF_config.truthMuonContainerName,\n')
          elif inputType == 'PHYS': # DAOD_PHYS
            Config.write('  "m_truthParticlesContainerName" : "TruthElectrons_selected TruthMuons_selected",\n')
        if 'MC' in sample:
          Config.write('  "m_muDetailStr"                 : muDetailStr,\n')
        else: # data
          Config.write('  "m_muDetailStr"                 : ZHF_config.muDetailStr,\n')
        Config.write('  "m_elDetailStr"                 : ZHF_config.elDetailStr,\n')
        Config.write('  "m_METDetailStr"                : ZHF_config.metDetailStr,\n')
        if 'MC' in sample:
          if inputType != 'AOD':
            Config.write('  "m_truthParticlesDetailStr"     : ZHF_config.truthParticlesDetailStr+"|"+ZHF_config.truthParticlesDetailStr,\n')
            Config.write('  "m_truthParticlesBranchName"    : "truthElectron truthMuon",\n')
            Config.write('  "m_truthJetDetailStr"           : ZHF_config.truthJetDetailStr+"|"+ZHF_config.truthTrackJetDetailStr+"|"+ZHF_config.truthFatJetDetailStr,\n')
          Config.write('  "m_trigDetailStr"               : ZHF_config.trigDetailStrMC,\n')
        else:
          Config.write('  "m_trigDetailStr"               : ZHF_config.trigDetailStrData,\n')
        Config.write('  } )\n')
        Config.write('\n')
        ##########################
        # WeightNamesAlgo
        ##########################
        if 'MC' in sample and inputType != 'AOD':
          Config.write('c.algorithm("WeightNamesAlgo", {\n')
          Config.write('  "m_name"                           : "WeightNamesAlgo",\n')
          Config.write('  "m_msgLevel"                       : ZHF_config.msgLevel,\n')
          Config.write('  } )\n')
          Config.write('\n')
        Config.close()

print '>>> ALL DONE <<<'

